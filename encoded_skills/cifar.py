"""
THE AIM HERE IS TO CHECK THE ACCURACY OF AN ENCODED SKILL FOR MNIST DATASET
"""

from __future__ import print_function
import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torchvision
#from torchvision import datasets, transforms
import torchvision.transforms as transforms
from torch.autograd import Variable
import matplotlib.pyplot as plt
import models as m
import random
import numpy as np

########################################################################
# The output of torchvision datasets are PILImage images of range [0, 1].
# We transform them to Tensors of normalized range [-1, 1].

transform = transforms.Compose(
    [transforms.ToTensor(),
     transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

trainset = torchvision.datasets.CIFAR10(root='./data', train=True,
                                        download=True, transform=transform)
trainloader = torch.utils.data.DataLoader(trainset, batch_size=4,
                                          shuffle=True, num_workers=2)

testset = torchvision.datasets.CIFAR10(root='./data', train=False,
                                       download=True, transform=transform)
testloader = torch.utils.data.DataLoader(testset, batch_size=4,
                                         shuffle=False, num_workers=2)

classes = ('plane', 'car', 'bird', 'cat',
           'deer', 'dog', 'frog', 'horse', 'ship', 'truck')

########################################################################

########################################################################
# 2. Define a Convolution Neural Network
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# Copy the neural network from the Neural Networks section before and modify it to
# take 3-channel images (instead of 1-channel images as it was defined).

from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F


class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1 = nn.Linear(16 * 5 * 5, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = x.view(-1, 16 * 5 * 5)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x


net = Net()

########################################################################



########################################################################
# 3. Define a Loss function and optimizer
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# Let's use a Classification Cross-Entropy loss and SGD with momentum.

import torch.optim as optim

criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)

########################################################################
# 4. Train the network
# ^^^^^^^^^^^^^^^^^^^^
#
# This is when things start to get interesting.
# We simply have to loop over our data iterator, and feed the inputs to the
# network and optimize.

for epoch in range(1):  # loop over the dataset multiple times

    running_loss = 0.0
    for i, data in enumerate(trainloader, 0):
        # get the inputs
        inputs, labels = data

        # wrap them in Variable
        inputs, labels = Variable(inputs), Variable(labels)

        # zero the parameter gradients
        optimizer.zero_grad()

        # forward + backward + optimize
        outputs = net(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()

        # print statistics
        running_loss += loss.data[0]
        if i % 2000 == 1999:    # print every 2000 mini-batches
            print('[%d, %5d] loss: %.3f' %
                  (epoch + 1, i + 1, running_loss / 2000))
            running_loss = 0.0

print('Finished Training')

########################################################################

def testNetwork(net):
    correct = 0
    total = 0
    for data in testloader:
        images, labels = data
        outputs = net(Variable(images))
        _, predicted = torch.max(outputs.data, 1)
        total += labels.size(0)
        correct += (predicted == labels).sum()

    print('Accuracy of the network on the 10000 test images: %d %%' % (
        100 * correct / total))

    ########################################################################
    # That looks waaay better than chance, which is 10% accuracy (randomly picking
    # a class out of 10 classes).
    # Seems like the network learnt something.
    #
    # Hmmm, what are the classes that performed well, and the classes that did
    # not perform well:

    class_correct = list(0. for i in range(10))
    class_total = list(0. for i in range(10))
    for data in testloader:
        images, labels = data
        outputs = net(Variable(images))
        _, predicted = torch.max(outputs.data, 1)
        c = (predicted == labels).squeeze()
        for i in range(4):
            label = labels[i]
            class_correct[label] += c[i]
            class_total[label] += 1


    for i in range(10):
        print('Accuracy of %5s : %2d %%' % (
            classes[i], 100 * class_correct[i] / class_total[i]))

########################################################################
testNetwork(net)



flatModel, shapes = m.flattenNetwork(net)
#print(flatModel.shape)
AE = m.AutoEncoder_cifar()

"""
-------------------------------------------------------------------------------------------
AE TRAINING AND TESTING FUNCTIONS
-------------------------------------------------------------------------------------------
"""
losses = []
def trainAE(weights,auto_encoder, epochs=1, factor=20):
    epochs = epochs
    # Define loss criterion
    criterion = nn.MSELoss()
    learning_rate = .7
    global losses
    for i in range(epochs):
        #learning_rate *= (1.0/(1.0+0.0002*i))
        optimizer = optim.SGD(AE.parameters(), lr=learning_rate, momentum=0.9)
        ##### pytorch syntax ##################################################
        # zero the parameter gradients
        optimizer.zero_grad()
        # forward + backward + optimize
        outputs, encoded_skill = AE(Variable(torch.from_numpy(weights.reshape(1,len(weights))).float()))
        #print(encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1]))
        encoded_skill =  encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1])
        labels = Variable(torch.from_numpy(weights))
        loss = criterion(outputs, labels)
        #print(loss.data.numpy()[0])
        losses.append(loss.data.numpy()[0])
        loss.backward()
        optimizer.step()
        ##### pytorch syntax ##################################################
    approximated_weights, encoded_skill = AE(Variable(torch.from_numpy(weights.reshape(1,len(weights))).float()))
    encoded_skill = encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1])
    #es2 = es2.data.numpy().reshape(500)
    #es3 = es3.data.numpy().reshape(100)
    # fig = plt.figure()
    # plt.plot(losses)
    # plt.show()
    return approximated_weights, encoded_skill, AE


"""
-------------------------------------------------------------------------------------------
END AE TRAINING AND TESTING FUNCTIONS
-------------------------------------------------------------------------------------------
"""




while 1 ==1 :
    for i in range(2000):
        print("training autoencoder: "+str(i))
        #index = random.randint(0,0)
        net_to_encode = flatModel
        approx_weights_flattened, encoded_skill, AE = trainAE(net_to_encode,AE)
    #print(shapes)

    approx_weights_flattened = approx_weights_flattened.data.numpy().reshape(62006)
    approxModel = m.unFlattenNetwork(approx_weights_flattened, shapes)
    newModel = Net()
    #load those weights into the network
    newModel = m.loadWeights_cifar(approxModel, newModel)
    testNetwork(newModel)

    #print(approxModel.shape)
    plt.plot(losses)
    plt.show()
