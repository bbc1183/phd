"""
- GET 6 STATES, 3 EACH FROM 2 CLASSES
    - A CLASS IS THE INITIAL POSITION OF THE BOMB, WALL, TARGET
- TRAIN 6 MODELS
- ENCODE 6 MODELS TO VECTORS LENGTH 4
- EXAMINE PRINCIPLE AND NON-PRINCIPLE COMPONENTS.

HYPOTHESIS:  THE AUTOENCODER WILL USE THE PRINCIPLE COMPONENTS TO SEPARATE THE MODELS,
             BUT I EXPECT TO SEE CLUSTERING AMONG NON-PRINCIPLE COMPONENTS, REPRESENTING
             THE DIFFERENT CLASSES OF TASKS.
"""


import models as m
import gridworld as gw
import qnetwork as q

import torch
import torchvision
import torchvision.transforms as transforms
import torch.optim as optim
import torch.nn as nn
import random
import torch.nn.functional as F
from torch.autograd import Variable, Function
import numpy as np
import matplotlib.pyplot as plt
import sys

"""
INITIALIZE 5 DIFFERENT GAME BOARDS, SAVE THE INITIAL STATES
TRAIN AGENTS ON THESE BOARDS
"""
states = gw.get6GameBoards()
training_states = states[0:6]
"""
TRAIN THE MODELS TO VICTORY
"""

#train the models until they win
winStatus1 = 0
winStatus2 = 0
winStatus3 = 0
winStatus4 = 0
winStatus5 = 0
winStatus6 = 0
while winStatus1 == 0:
    trainedModel_1, paths1 = q.trainNetwork(in_state=training_states[0])
    trainedModel_1, paths1, finalReward1 = q.testNetwork_v2(trainedModel_1,inState=gw.stateToData(training_states[0]))
    if finalReward1 == 20:
        winStatus1 = 1
while winStatus2 == 0:
    trainedModel_2, paths2 = q.trainNetwork(in_state=training_states[1])
    trainedModel_2, paths2, finalReward2 = q.testNetwork_v2(trainedModel_2,inState=gw.stateToData(training_states[1]))
    if finalReward2 == 20:
        winStatus2 = 1
while winStatus3 == 0:
    trainedModel_3, paths3 = q.trainNetwork(in_state=training_states[2])
    trainedModel_3,paths3, finalReward3 = q.testNetwork_v2(trainedModel_3,inState=gw.stateToData(training_states[2]))
    if finalReward3 == 20:
        winStatus3 = 1
while winStatus4 == 0:
    trainedModel_4, paths4 = q.trainNetwork(in_state=training_states[3])
    trainedModel_4,paths4, finalReward4 = q.testNetwork_v2(trainedModel_4,inState=gw.stateToData(training_states[3]))
    if finalReward4 == 20:
        winStatus4 = 1
while winStatus5 == 0:
    trainedModel_5, paths5 = q.trainNetwork(in_state=training_states[4])
    trainedModel_5,paths5, finalReward5 = q.testNetwork_v2(trainedModel_5,inState=gw.stateToData(training_states[4]))
    if finalReward5 == 20:
        winStatus5 = 1
while winStatus6 == 0:
    trainedModel_6, paths6 = q.trainNetwork(in_state=training_states[5])
    trainedModel_6,paths6, finalReward6 = q.testNetwork_v2(trainedModel_6,inState=gw.stateToData(training_states[5]))
    if finalReward6 == 20:
        winStatus6 = 1



"""
COMPRESS THE WEIGHTS TO ENCODED_SKILLS
SAVE THE DECODER (SHUFFLE OVER ALL SKILLS TO GENERALIZE)
SAVE ENCODED_SKILLS IN AN ARRAY
"""

#FLATTEN THE MODELS
trainedModel_1_flat, shapes_1 = m.flattenNetwork(trainedModel_1)
trainedModel_2_flat, shapes_2 = m.flattenNetwork(trainedModel_2)
trainedModel_3_flat, shapes_3 = m.flattenNetwork(trainedModel_3)
trainedModel_4_flat, shapes_4 = m.flattenNetwork(trainedModel_4)
trainedModel_5_flat, shapes_5 = m.flattenNetwork(trainedModel_5)
trainedModel_6_flat, shapes_6 = m.flattenNetwork(trainedModel_6)
trained_models_flat = [trainedModel_1_flat, trainedModel_2_flat, trainedModel_3_flat, trainedModel_4_flat, trainedModel_5_flat, trainedModel_6_flat]
losses = []
def trainAE(weights,auto_encoder, epochs=1, factor=20):
    epochs = epochs
    # Define loss criterion
    criterion = nn.MSELoss()
    learning_rate = .7
    global losses
    for i in range(epochs):
        #learning_rate *= (1.0/(1.0+0.0002*i))
        optimizer = optim.SGD(AE.parameters(), lr=learning_rate, momentum=0.9)
        ##### pytorch syntax ##################################################
        # zero the parameter gradients
        optimizer.zero_grad()
        # forward + backward + optimize
        outputs, encoded_skill = AE(Variable(torch.from_numpy(weights.reshape(1,len(weights))).float()))
        #print(encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1]))
        encoded_skill =  encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1])
        labels = Variable(torch.from_numpy(weights))
        loss = criterion(outputs, labels)
        #print(loss.data.numpy()[0])
        losses.append(loss.data.numpy()[0])
        loss.backward()
        optimizer.step()
        ##### pytorch syntax ##################################################
    approximated_weights, encoded_skill = AE(Variable(torch.from_numpy(weights.reshape(1,len(weights))).float()))
    encoded_skill = encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1])
    # fig = plt.figure()
    # plt.plot(losses)
    # plt.show()
    return approximated_weights, encoded_skill, AE

def test_encoded_skill(skill, grid):
    global AE
    guessed_encoded_skill = Variable(torch.from_numpy(skill.reshape(1,4)).float())
    #pass this skill to the AE to get the needed weights
    guessed_weights_flat = AE.decoder(guessed_encoded_skill)
    #guessed_weights_flat = AE.decoder(Variable(torch.from_numpy(encoded_skill_1.reshape(1,5))).float())
    guessed_weights_flat = guessed_weights_flat.data.numpy().reshape(guessed_weights_flat.shape[1])
    guessed_weights_unflat = m.unFlattenNetwork(guessed_weights_flat, shapes_1)
    net = m.Linear_Net()
    #load those weights into the network
    net = m.loadWeights(guessed_weights_unflat, net)
    #makeMove and get state.
    net,path,reward = q.testNetwork_v2(net, inState=grid)

    if len(path) > 10:
        testStatus = 0
        print("GAME LOST, too many moves")
    if reward == 20:
        testStatus = 0
        print("GAME WON!")
    if reward == -20:
        testStatus = 0
        print("GAME LOST")
    return reward

"""
TRAIN THE AUTOENCODER TO VICTORY
"""
final_state_skill_1 = ()
final_state_skill_2 = ()
final_state_skill_3 = ()
final_state_skill_4 = ()
final_state_skill_5 = ()
final_state_skill_6 = ()
AE_win_status = 0
while AE_win_status == 0:
    AE = m.AutoEncoder(inSize=2224, factor=20)
    losses = []
    for i in range(30000):
        print("training autoencoder: "+str(i))
        index = random.randint(0,len(trained_models_flat)-1)
        net_to_encode = trained_models_flat[index]
        approx_weights_flattened, encoded_skill, AE = trainAE(net_to_encode,AE)
        if index == 0:
            final_state_skill_1 = (training_states[0], encoded_skill)
        if index == 1:
            final_state_skill_2 = (training_states[1], encoded_skill)
        if index == 2:
            final_state_skill_3 = (training_states[2], encoded_skill)
        if index == 3:
            final_state_skill_4 = (training_states[3], encoded_skill)
        if index == 4:
            final_state_skill_5 = (training_states[4], encoded_skill)
        if index == 5:
            final_state_skill_6 = (training_states[5], encoded_skill)
    fig = plt.figure()
    plt.plot(losses)
    plt.show()
    print("first encoded_skill: "+str(final_state_skill_1[1]))
    print("second_encoded_skill: "+str(final_state_skill_2[1]))
    print("third_encoded_skill: "+str(final_state_skill_3[1]))
    print("fourth_encoded_skill: "+str(final_state_skill_4[1]))
    print("fifth_encoded_skill: "+str(final_state_skill_5[1]))
    print("sixth_encoded_skill: "+str(final_state_skill_6[1]))
    status_1 = test_encoded_skill(final_state_skill_1[1], gw.stateToData(final_state_skill_1[0]))
    status_2 = test_encoded_skill(final_state_skill_2[1], gw.stateToData(final_state_skill_2[0]))
    status_3 = test_encoded_skill(final_state_skill_3[1], gw.stateToData(final_state_skill_3[0]))
    status_4 = test_encoded_skill(final_state_skill_4[1], gw.stateToData(final_state_skill_4[0]))
    status_5 = test_encoded_skill(final_state_skill_5[1], gw.stateToData(final_state_skill_5[0]))
    status_6 = test_encoded_skill(final_state_skill_6[1], gw.stateToData(final_state_skill_6[0]))
    if status_1 == 20 and status_2 == 20 and status_3 == 20 and status_4 == 20 and status_5 == 20 and status_6 == 20:

        AE_win_status = 1

final_states_and_skills = [final_state_skill_1,final_state_skill_2, final_state_skill_3, final_state_skill_4, final_state_skill_5, final_state_skill_6]

"""
WINNING ENCODED SKILLS

>>> a = [0.0050832 ,  0.39332399 , 0.99046749 , 0.98313785]
>>> b = [ 0.83903784 , 0.0636243  , 0.98577082 , 0.27874026]
>>> c = [ 0.90995663 , 0.9275229 ,  0.07628328 , 0.99534142]
>>> d = [ 0.35294569 , 0.99041665 , 0.00162931 , 0.00441079]
>>> e =  [ 0.99237329 , 0.00570779 , 0.0117378 ,  0.00543634]
>>> f = [ 0.05174524 , 0.0474399 ,  0.06965593 , 0.27696708]

first encoded_skill: [ 0.00874097 , 0.02057545 , 0.99808526 , 0.98868215]
second_encoded_skill: [ 0.07575157 , 0.98921841, 0.00910866 , 0.00444177]
third_encoded_skill: [ 0.98427922,  0.01108533,  0.97238612 , 0.78898352]
fourth_encoded_skill: [ 0.99841332 , 0.02871653 , 0.00921072,  0.00613912]
fifth_encoded_skill: [ 0.43791285 , 0.98438686 , 0.04386717,  0.99559337]
sixth_encoded_skill: [ 0.37003875 , 0.93361849 , 0.98480761 , 0.35973039]



first encoded_skill: [ 0.97953814 , 0.00353105 , 0.19427104 , 0.07092647]
second_encoded_skill: [ 0.94692534 , 0.99610823 , 0.00193188 , 0.82897633]
third_encoded_skill: [ 0.00678281 , 0.00561532 , 0.98628277 , 0.09245212]
fourth_encoded_skill: [ 0.01581082 , 0.99387383 , 0.68838525 , 0.01975247]
fifth_encoded_skill: [ 0.01625679,  0.29350868 , 0.33281979 , 0.99255693]
sixth_encoded_skill: [ 0.98733574 , 0.89356017 , 0.98691189 , 0.90480459]

"""
