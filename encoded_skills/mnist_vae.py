"""
THIS IS THE SAME AS mnist2.py EXCEPT I WILL USE A VAE HERE


"""

from __future__ import print_function
import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.autograd import Variable
import matplotlib.pyplot as plt
import models as m
import random
import sys
import numpy as np
from sklearn.manifold import TSNE

# Training settings
parser = argparse.ArgumentParser(description='PyTorch MNIST Example')
parser.add_argument('--batch-size', type=int, default=64, metavar='N',
                    help='input batch size for training (default: 64)')
parser.add_argument('--test-batch-size', type=int, default=1000, metavar='N',
                    help='input batch size for testing (default: 1000)')
parser.add_argument('--epochs', type=int, default=10, metavar='N',
                    help='number of epochs to train (default: 10)')
parser.add_argument('--lr', type=float, default=0.01, metavar='LR',
                    help='learning rate (default: 0.01)')
parser.add_argument('--momentum', type=float, default=0.5, metavar='M',
                    help='SGD momentum (default: 0.5)')
parser.add_argument('--no-cuda', action='store_true', default=False,
                    help='disables CUDA training')
parser.add_argument('--seed', type=int, default=1, metavar='S',
                    help='random seed (default: 1)')
parser.add_argument('--log-interval', type=int, default=10, metavar='N',
                    help='how many batches to wait before logging training status')
args = parser.parse_args()
args.cuda = not args.no_cuda and torch.cuda.is_available()

torch.manual_seed(args.seed)
if args.cuda:
    torch.cuda.manual_seed(args.seed)


kwargs = {'num_workers': 1, 'pin_memory': True} if args.cuda else {}
train_loader = torch.utils.data.DataLoader(
    datasets.MNIST('../data', train=True, download=True,
                   transform=transforms.Compose([
                       transforms.ToTensor(),
                       transforms.Normalize((0.1307,), (0.3081,))
                   ])),
    batch_size=args.batch_size, shuffle=True, **kwargs)
test_loader = torch.utils.data.DataLoader(
    datasets.MNIST('../data', train=False, transform=transforms.Compose([
                       transforms.ToTensor(),
                       transforms.Normalize((0.1307,), (0.3081,))
                   ])),
    batch_size=args.test_batch_size, shuffle=True, **kwargs)


class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(1, 10, kernel_size=5)
        self.conv2 = nn.Conv2d(10, 20, kernel_size=5)
        self.conv2_drop = nn.Dropout2d()
        self.fc1 = nn.Linear(320, 50)
        self.fc2 = nn.Linear(50, 10)

    def forward(self, x):
        x = F.relu(F.max_pool2d(self.conv1(x), 2))
        x = F.relu(F.max_pool2d(self.conv2_drop(self.conv2(x)), 2))
        x = x.view(-1, 320)
        x = F.relu(self.fc1(x))
        x = F.dropout(x, training=self.training)
        x = self.fc2(x)
        return F.log_softmax(x)

model = Net()
model2 = Net()
model3 = Net()
if args.cuda:
    model.cuda()

optimizer = optim.SGD(model.parameters(), lr=.01, momentum=.9)
optimizer2 = optim.SGD(model2.parameters(), lr=.01, momentum=.9)
optimizer3 = optim.SGD(model3.parameters(), lr=.01, momentum=.9)
losses = []
def train(epoch):
    model.train()
    for batch_idx, (data, target) in enumerate(train_loader):
        if args.cuda:
            data, target = data.cuda(), target.cuda()
        data, target = Variable(data), Variable(target)

        optimizer.zero_grad()
        optimizer2.zero_grad()
        optimizer3.zero_grad()

        output = model(data)
        output2 = model2(data)
        output3 = model3(data)

        loss = F.nll_loss(output, target)
        loss2 = F.nll_loss(output2, target)
        loss3 = F.nll_loss(output3, target)

        loss.backward()
        loss2.backward()
        loss3.backward()

        optimizer.step()
        optimizer2.step()
        optimizer3.step()

        if batch_idx % args.log_interval == 0:
            #losses.append(loss.data[0])
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), loss.data[0]))

def test(testModel):
    testModel.eval()
    test_loss = 0
    correct = 0
    for data, target in test_loader:
        if args.cuda:
            data, target = data.cuda(), target.cuda()
        data, target = Variable(data, volatile=True), Variable(target)
        output = testModel(data)
        test_loss += F.nll_loss(output, target, size_average=False).data[0] # sum up batch loss
        pred = output.data.max(1, keepdim=True)[1] # get the index of the max log-probability
        correct += pred.eq(target.data.view_as(pred)).cpu().sum()

    test_loss /= len(test_loader.dataset)
    accuracy = 100. * correct / len(test_loader.dataset)
    print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
        test_loss, correct, len(test_loader.dataset),
        100. * correct / len(test_loader.dataset)))
    return accuracy


for epoch in range(1, 4):
    train(epoch)
#test(model)

flatModel, shapes = m.flattenNetwork(model)
flatModel2, shapes2 = m.flattenNetwork(model2)
flatModel3, shapes3 = m.flattenNetwork(model3)

flatModels = [flatModel, flatModel2, flatModel3]

VAE = m.VAE_v3()


"""
-------------------------------------------------------------------------------------------
AE TRAINING AND TESTING FUNCTIONS
-------------------------------------------------------------------------------------------
"""
losses = []
def trainVAE(weights, epochs=1, factor=20):
    epochs = epochs
    # Define loss criterion
    learning_rate = .0001
    criterion = nn.MSELoss()
    vae_optimizer = optim.Adam(VAE.parameters(), lr=learning_rate)
    global losses
    for i in range(epochs):
        #learning_rate *= (1.0/(1.0+0.0002*i))
        ##### pytorch syntax ##################################################
        # zero the parameter gradients
        vae_optimizer.zero_grad()
        # forward + backward + optimize
        inputs = Variable(torch.from_numpy(weights.reshape(1,len(weights))).float())
        outputs, mu, logvar = VAE(inputs)
        #print(encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1]))

        loss = loss_function(outputs, inputs, mu, logvar)
        loss.backward()
        losses.append(loss.data[0])
        print("loss: "+str(loss.data[0]))
        vae_optimizer.step()
        ##### pytorch syntax ##################################################
    approximated_weights, mu, logvar = VAE(inputs)
    #encoded_skill = encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1])

    # fig = plt.figure()
    # plt.plot(losses)
    # plt.show()
    return approximated_weights, mu, logvar, VAE

def trainVAE_dual(weights, epochs=1, factor=20):
    epochs = epochs
    # Define loss criterion
    learning_rate = .001
    criterion = nn.MSELoss()
    vae_optimizer = optim.SGD(VAE.parameters(), lr=learning_rate, momentum=0.9)
    global losses
    for i in range(epochs):
        #learning_rate *= (1.0/(1.0+0.0002*i))
        ##### pytorch syntax ##################################################
        # zero the parameter gradients
        vae_optimizer.zero_grad()
        # forward + backward + optimize
        inputs = Variable(torch.from_numpy(weights.reshape(1,len(weights))).float())
        outputs, mu, logvar = VAE(inputs)
        #print(encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1]))

        loss = loss_function(outputs, inputs, mu, logvar)
        loss.backward()
        losses.append(loss.data[0])
        print("loss: "+str(loss.data[0]))
        vae_optimizer.step()
        ##### pytorch syntax ##################################################
    approximated_weights, mu, logvar = VAE(inputs)
    #encoded_skill = encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1])

    # fig = plt.figure()
    # plt.plot(losses)
    # plt.show()
    return approximated_weights, mu, logvar, VAE

# Reconstruction + KL divergence losses summed over all elements and batch
def loss_function(recon_x, x, mu, logvar):
    MSE = F.mse_loss(recon_x, x.view(-1, 21840), size_average=False)

    # see Appendix B from VAE paper:
    # Kingma and Welling. Auto-Encoding Variational Bayes. ICLR, 2014
    # https://arxiv.org/abs/1312.6114
    # 0.5 * sum(1 + log(sigma^2) - mu^2 - sigma^2)
    KLD = (-0.5 * torch.sum(1 + logvar - mu.pow(2) - logvar.exp()))/21840.0

    return pow(MSE + KLD,1/2)


def trainAE_dual(weights,weights2, auto_encoder, losses,losses_model1, losses_model2, epochs=1, lr=.9):
    epochs = epochs
    local_AE = auto_encoder
    local_losses_model1 = losses_model1
    local_losses_model2 = losses_model2
    local_losses = losses
    # Define loss criterion
    criterion = nn.MSELoss()
    learning_rate = lr

    #learning_rate *= (1.0/(1.0+0.0002*i))
    optimizer = optim.SGD(local_AE.parameters(), lr=learning_rate, momentum=0.9)
    ##### pytorch syntax ##################################################
    # zero the parameter gradients
    optimizer.zero_grad()


    #for weights1 and weights 2
    outputs, encoded_skill = local_AE(Variable(torch.from_numpy(weights.reshape(1,len(weights))).float()))
    outputs2, encoded_skill2 = local_AE(Variable(torch.from_numpy(weights2.reshape(1,len(weights))).float()))

    encoded_skill =  encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1])
    encoded_skill2 =  encoded_skill2.data.numpy().reshape(encoded_skill2.data.numpy().shape[1])

    labels = Variable(torch.from_numpy(weights))
    labels2 = Variable(torch.from_numpy(weights2))


    loss = criterion(outputs, labels)
    loss2 = criterion(outputs2, labels2)

    total_loss = pow(loss + loss2,1/2)

    #print(loss.data.numpy()[0])
    local_losses.append(total_loss.data.numpy()[0])
    local_losses_model1.append(loss.data.numpy()[0])
    local_losses_model2.append(loss2.data.numpy()[0])
    total_loss.backward()
    optimizer.step()
    ##### pytorch syntax ##################################################
    approximated_weights, encoded_skill = local_AE(Variable(torch.from_numpy(weights.reshape(1,len(weights))).float()))
    approximated_weights2, encoded_skill2 = local_AE(Variable(torch.from_numpy(weights2.reshape(1,len(weights2))).float()))

    encoded_skill = encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1])
    encoded_skill2 = encoded_skill2.data.numpy().reshape(encoded_skill2.data.numpy().shape[1])
    #es2 = es2.data.numpy().reshape(500)
    #es3 = es3.data.numpy().reshape(100)
    # fig = plt.figure()
    # plt.plot(losses)
    # plt.show()
    return approximated_weights,approximated_weights2, encoded_skill, encoded_skill2, local_AE, local_losses, local_losses_model1, local_losses_model2

def test_encoded_skill(skill, grid, AE):
    AE
    guessed_encoded_skill = Variable(torch.from_numpy(skill.reshape(1,4)).float())
    #pass this skill to the AE to get the needed weights
    guessed_weights_flat, encoded = AE.decoder(guessed_encoded_skill)
    #guessed_weights_flat = AE.decoder(Variable(torch.from_numpy(encoded_skill_1.reshape(1,5))).float())
    guessed_weights_flat = guessed_weights_flat.data.numpy().reshape(2224)
    guessed_weights_unflat = m.unFlattenNetwork(guessed_weights_flat, shapes)
    net = m.Linear_Net()
    #load those weights into the network
    net = m.loadWeights(guessed_weights_unflat, net)
    #makeMove and get state.
    net,path,reward = q.testNetwork_v2(net, inState=grid)

    if len(path) > 10:
        testStatus = 0
        print("GAME LOST, too many moves")
    if reward == 20:
        testStatus = 0
        print("GAME WON!")
    if reward == -20:
        testStatus = 0
        print("GAME LOST")
    return reward
"""
-------------------------------------------------------------------------------------------
END AE TRAINING AND TESTING FUNCTIONS
-------------------------------------------------------------------------------------------
"""


def loadWeights_mnist(weights_to_load, net):
    net.conv1.weight.data = torch.from_numpy(weights_to_load[0])
    net.conv1.bias.data =   torch.from_numpy(weights_to_load[1])
    net.conv2.weight.data = torch.from_numpy(weights_to_load[2])
    net.conv2.bias.data =   torch.from_numpy(weights_to_load[3])
    net.fc1.weight.data =   torch.from_numpy(weights_to_load[4])
    net.fc1.bias.data =     torch.from_numpy(weights_to_load[5])
    net.fc2.weight.data =   torch.from_numpy(weights_to_load[6])
    net.fc2.bias.data =     torch.from_numpy(weights_to_load[7])
    return net


"""
-------------------------------------------------------------------------------------------
AUTO-ENCODE THE FIRST MODEL AND CHECK THE PERFORMANCE
-------------------------------------------------------------------------------------------
"""


"""
model1_performances = []
model2_performances = []
losses_test_1 = []

for i in range(10000):
    print("training autoencoder: "+str(i))
    index = random.randint(0,0)
    net_to_encode = flatModels[index]
    approx_weights_flattened, encoded_skill, AE, losses_test_1  = trainAE(net_to_encode,AE, losses_test_1)
#print(shapes)

model1_encoded_skill = encoded_skill
approx_weights_flattened1 = approx_weights_flattened.data.numpy().reshape(21840)
approxModel1 = m.unFlattenNetwork(approx_weights_flattened1, shapes)
newModel = Net()
#load those weights into the network
newModel1 = m.loadWeights_mnist(approxModel1, newModel)
accuracy1 = test(newModel1)
model1_performances.append(accuracy1)

#print(approxModel.shape)
plt.plot(losses_test_1)
plt.show()


flatModel1 = approx_weights_flattened1
x = 0

"""




##############################################################################################

VAE_win_status = 0

encoded_skills = []
losses = []
Accuracies_array = []
encoded_skills = []
curr_round = 1
accuracy_ranges = []
while VAE_win_status == 0:


    for i in range(100):
        print("training autoencoder: "+str(i))
        index = random.randint(0,0)
        net_to_encode = flatModels[index]#trained_models_flat[index]
        #approx_weights_flattened, mu, logvar, VAE =
        trainVAE(net_to_encode)
    total_accuracies = 0

    temp_accuracy_range = []
    for j in range(0,100):
        sample =Variable(torch.randn(1, 20))
        print("sample before decode is",sample)
        sample, latent_vars = VAE.decode(sample)
        print("sample after decode is",sample)
        sample=sample.data.numpy().reshape(21840)
        latent_vars = latent_vars.data.numpy().reshape(20)
        final_weights=m.unFlattenNetwork(sample, shapes)
        net = loadWeights_mnist(final_weights,model)
        accuracy = test(net)
        temp_accuracy_range.append(accuracy)
        total_accuracies += accuracy
        encoded_skills.append(latent_vars)
    avg_accuracy = total_accuracies/100.0
    Accuracies_array.append(avg_accuracy)
    accuracy_ranges.append(temp_accuracy_range)
    print("AVERAGE ACCURACY FOR ROUND: "+str(avg_accuracy))
    # status_1 = test_encoded_skill(sample, gw.stateToData(state1), VAE)
    #
    #
    # if status_1 == 20:
    #     #encoded_skills.append(encoded_skill)
    #     print("VICTORY FOR ALL ENCODED SKILLS")
    #     VAE_win_status = 1
    fig = plt.figure()
    plt.plot(losses)
    plt.show()

    fig = plt.figure()
    for idx,acc_range in enumerate(accuracy_ranges):
        x = np.empty(100)
        x.fill(idx+1)
        plt.scatter(x,np.array(acc_range).reshape(1,100))
    plt.show()

    fig = plt.figure()
    plt.plot(Accuracies_array)
    plt.show()
    print()




    # if curr_round == 20:
    #     skills = np.array(encoded_skills)
    #     skill_batch1 = skills
    #     skill_batch1 = TSNE(n_components=2).fit_transform(skill_batch1)
    #     skills = np.array(skill_batch1)
    #     plt.scatter(skills[:,0], skills[:,1])
    #     plt.show()
    #     VAE_win_status = 1

    curr_round += 1
