"""
THIS IS AN EXTENSION OF MNIST.PY

THE AIM IS TO MEASURE THE PERFORMANCE DEGRADATION OVER TIME, AS SKILLS ARE RECALLED MULTIPLE TIMES
"""

from __future__ import print_function
import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.autograd import Variable
import matplotlib.pyplot as plt
import models as m
import random
import numpy as np
import sys


# Training settings
parser = argparse.ArgumentParser(description='PyTorch MNIST Example')
parser.add_argument('--batch-size', type=int, default=64, metavar='N',
                    help='input batch size for training (default: 64)')
parser.add_argument('--test-batch-size', type=int, default=1000, metavar='N',
                    help='input batch size for testing (default: 1000)')
parser.add_argument('--epochs', type=int, default=10, metavar='N',
                    help='number of epochs to train (default: 10)')
parser.add_argument('--lr', type=float, default=0.01, metavar='LR',
                    help='learning rate (default: 0.01)')
parser.add_argument('--momentum', type=float, default=0.5, metavar='M',
                    help='SGD momentum (default: 0.5)')
parser.add_argument('--no-cuda', action='store_true', default=False,
                    help='disables CUDA training')
parser.add_argument('--seed', type=int, default=1, metavar='S',
                    help='random seed (default: 1)')
parser.add_argument('--log-interval', type=int, default=10, metavar='N',
                    help='how many batches to wait before logging training status')
args = parser.parse_args()
args.cuda = not args.no_cuda and torch.cuda.is_available()

torch.manual_seed(args.seed)
if args.cuda:
    torch.cuda.manual_seed(args.seed)


kwargs = {'num_workers': 1, 'pin_memory': True} if args.cuda else {}
train_loader = torch.utils.data.DataLoader(
    datasets.MNIST('../data', train=True, download=True,
                   transform=transforms.Compose([
                       transforms.ToTensor(),
                       transforms.Normalize((0.1307,), (0.3081,))
                   ])),
    batch_size=args.batch_size, shuffle=True, **kwargs)
test_loader = torch.utils.data.DataLoader(
    datasets.MNIST('../data', train=False, transform=transforms.Compose([
                       transforms.ToTensor(),
                       transforms.Normalize((0.1307,), (0.3081,))
                   ])),
    batch_size=args.test_batch_size, shuffle=True, **kwargs)


class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(1, 10, kernel_size=5)
        self.conv2 = nn.Conv2d(10, 20, kernel_size=5)
        self.conv2_drop = nn.Dropout2d()
        self.fc1 = nn.Linear(320, 50)
        self.fc2 = nn.Linear(50, 10)

    def forward(self, x):
        x = F.relu(F.max_pool2d(self.conv1(x), 2))
        x = F.relu(F.max_pool2d(self.conv2_drop(self.conv2(x)), 2))
        x = x.view(-1, 320)
        x = F.relu(self.fc1(x))
        x = F.dropout(x, training=self.training)
        x = self.fc2(x)
        return F.log_softmax(x)

model = Net()
model2 = Net()
model3 = Net()
if args.cuda:
    model.cuda()

optimizer = optim.SGD(model.parameters(), lr=.01, momentum=.9)
optimizer2 = optim.SGD(model2.parameters(), lr=.01, momentum=.9)
optimizer3 = optim.SGD(model3.parameters(), lr=.01, momentum=.9)
losses = []
def train(epoch):
    model.train()
    for batch_idx, (data, target) in enumerate(train_loader):
        if args.cuda:
            data, target = data.cuda(), target.cuda()
        data, target = Variable(data), Variable(target)

        optimizer.zero_grad()
        optimizer2.zero_grad()
        optimizer3.zero_grad()

        output = model(data)
        output2 = model2(data)
        output3 = model3(data)

        loss = F.nll_loss(output, target)
        loss2 = F.nll_loss(output2, target)
        loss3 = F.nll_loss(output3, target)

        loss.backward()
        loss2.backward()
        loss3.backward()

        optimizer.step()
        optimizer2.step()
        optimizer3.step()

        if batch_idx % args.log_interval == 0:
            #losses.append(loss.data[0])
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), loss.data[0]))

def test(testModel):
    testModel.eval()
    test_loss = 0
    correct = 0
    for data, target in test_loader:
        if args.cuda:
            data, target = data.cuda(), target.cuda()
        data, target = Variable(data, volatile=True), Variable(target)
        output = testModel(data)
        test_loss += F.nll_loss(output, target, size_average=False).data[0] # sum up batch loss
        pred = output.data.max(1, keepdim=True)[1] # get the index of the max log-probability
        correct += pred.eq(target.data.view_as(pred)).cpu().sum()

    test_loss /= len(test_loader.dataset)
    accuracy = 100. * correct / len(test_loader.dataset)
    print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
        test_loss, correct, len(test_loader.dataset),
        100. * correct / len(test_loader.dataset)))
    return accuracy


# for epoch in range(1, 4):
#     train(epoch)
# #test(model)
# torch.save(model, "mnist2_vae_model1.pt")
# torch.save(model2, "mnist2_vae_model2.pt")
# torch.save(model3, "mnist2_vae_model3.pt")

model = torch.load("mnist2_vae_model1.pt")
model2 = torch.load("mnist2_vae_model2.pt")
model3 = torch.load("mnist2_vae_model3.pt")



flatModel, shapes = m.flattenNetwork(model)
flatModel2, shapes2 = m.flattenNetwork(model2)
flatModel3, shapes3 = m.flattenNetwork(model3)

flatModels = [flatModel, flatModel2, flatModel3]

VAE = m.VAE_v1()


"""
-------------------------------------------------------------------------------------------
AE TRAINING AND TESTING FUNCTIONS
-------------------------------------------------------------------------------------------
"""
losses = []
def trainVAE(weights,auto_encoder, epochs=1, factor=20):
    epochs = epochs
    # Define loss criterion
    criterion = nn.MSELoss()
    #BEST COMBINATION I HAVE FOUND IS POW(LOSS, 1/2) AND LR=.01 WITH VAE ARCHITECTURE V1
    #OR POW(LOSS, 0.3) AND LR=.05 WITH VAE ARCHITECTURE V1
    learning_rate = .01
    global losses

    for i in range(epochs):
        #learning_rate *= (1.0/(1.0+0.0002*i))
        optimizer = optim.SGD(VAE.parameters(), lr=learning_rate, momentum=0.9)
        ##### pytorch syntax ##################################################
        # zero the parameter gradients
        optimizer.zero_grad()
        # forward + backward + optimize
        inputs = Variable(torch.from_numpy(weights.reshape(1,len(weights))).float())
        outputs, mu, logvar  = VAE(inputs)
        #print(encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1]))

        loss = loss_function(outputs, inputs, mu, logvar)
        loss.backward()
        losses.append(loss.data[0])
        print("loss: "+str(loss.data[0]))
        optimizer.step()
        ##### pytorch syntax ##################################################
    approximated_weights, mu, logvar = VAE(inputs)
    #encoded_skill = encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1])

    # fig = plt.figure()
    # plt.plot(losses)
    # plt.show()
    return approximated_weights, mu, logvar,  VAE

def trainVAE_dual(weights,weights2,vae,  lr = .05):
    epochs = 1
    # Define loss criterion
    criterion = nn.MSELoss()
    learning_rate = lr
    global losses

    for i in range(epochs):
        #learning_rate *= (1.0/(1.0+0.0002*i))
        optimizer = optim.SGD(vae.parameters(), lr=learning_rate, momentum=0.9)
        ##### pytorch syntax ##################################################
        # zero the parameter gradients
        optimizer.zero_grad()
        # forward + backward + optimize
        inputs = Variable(torch.from_numpy(weights.reshape(1,len(weights))).float())
        inputs2 = Variable(torch.from_numpy(weights2.reshape(1,len(weights2))).float())
        outputs, mu, logvar  = vae(inputs)
        outputs2, mu2, logvar2  = vae(inputs2)
        #print(encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1]))

        loss = loss_function_v2(outputs, inputs, mu, logvar)
        loss2 = loss_function_v2(outputs2, inputs2, mu2, logvar2)
        total_loss = (loss + loss2)/2.0
        total_loss.backward()
        losses.append(total_loss.data[0])
        #print("loss: "+str(total_loss.data[0]))
        optimizer.step()
        ##### pytorch syntax ##################################################
    approximated_weights, mu, logvar = vae(inputs)
    approximated_weights2, mu2, logvar2 = vae(inputs2)
    #encoded_skill = encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1])

    # fig = plt.figure()
    # plt.plot(losses)
    # plt.show()
    return approximated_weights,approximated_weights2, mu, logvar, mu2, logvar2,  vae

# Reconstruction + KL divergence losses summed over all elements and batch
def loss_function(recon_x, x, mu, logvar):
    MSE = F.mse_loss(recon_x, x.view(-1, 21840), size_average=False)

    # see Appendix B from VAE paper:
    # Kingma and Welling. Auto-Encoding Variational Bayes. ICLR, 2014
    # https://arxiv.org/abs/1312.6114
    # 0.5 * sum(1 + log(sigma^2) - mu^2 - sigma^2)
    KLD = -0.5 * torch.sum(1 + logvar - mu.pow(2) - logvar.exp())

    #BEST COMBINATION I HAVE FOUND IS POW(LOSS, 1/2) AND LR=.01
    return pow(MSE + KLD, 1/2)

# Reconstruction + KL divergence losses summed over all elements and batch
def loss_function_v2(recon_x, x, mu, logvar):
    MSE = F.mse_loss(recon_x, x.view(-1, 21840), size_average=False)

    # see Appendix B from VAE paper:
    # Kingma and Welling. Auto-Encoding Variational Bayes. ICLR, 2014
    # https://arxiv.org/abs/1312.6114
    # 0.5 * sum(1 + log(sigma^2) - mu^2 - sigma^2)
    KLD = -0.5 * torch.sum(1 + logvar - mu.pow(2) - logvar.exp())

    return pow(MSE + KLD, 1/2)

def test_encoded_skill(skill, grid, AE):
    AE
    guessed_encoded_skill = Variable(torch.from_numpy(skill.reshape(1,4)).float())
    #pass this skill to the AE to get the needed weights
    guessed_weights_flat, encoded = AE.decoder(guessed_encoded_skill)
    #guessed_weights_flat = AE.decoder(Variable(torch.from_numpy(encoded_skill_1.reshape(1,5))).float())
    guessed_weights_flat = guessed_weights_flat.data.numpy().reshape(2224)
    guessed_weights_unflat = m.unFlattenNetwork(guessed_weights_flat, shapes_1)
    net = m.Linear_Net()
    #load those weights into the network
    net = m.loadWeights(guessed_weights_unflat, net)
    #makeMove and get state.
    net,path,reward = q.testNetwork_v2(net, inState=grid)

    if len(path) > 10:
        testStatus = 0
        print("GAME LOST, too many moves")
    if reward == 20:
        testStatus = 0
        print("GAME WON!")
    if reward == -20:
        testStatus = 0
        print("GAME LOST")
    return reward
"""
-------------------------------------------------------------------------------------------
END AE TRAINING AND TESTING FUNCTIONS
-------------------------------------------------------------------------------------------
"""




"""
-------------------------------------------------------------------------------------------
AUTO-ENCODE THE FIRST MODEL AND CHECK THE PERFORMANCE
-------------------------------------------------------------------------------------------
"""
model1_performances = []
model2_performances = []



"""
#THIS IS WORKING AT THE MOMENT
for i in range(1200):
    print("training autoencoder: "+str(i))
    index = random.randint(0,0)
    net_to_encode = flatModels[index]
    approx_weights_flattened, mu, logvar, VAE  = trainVAE(net_to_encode, VAE)
#print(shapes)


sample =Variable(torch.randn(1, 10))
apprx, latent_vars = VAE.decode(sample)
apprx=apprx.data.numpy().reshape(21840)
approxModel1 = m.unFlattenNetwork(apprx, shapes)
newModel = Net()

#load those weights into the network and test
newModel1 = m.loadWeights_mnist(approxModel1, newModel)
accuracy1 = test(newModel1)
model1_performances.append(accuracy1)

#print(approxModel.shape)
plt.plot(losses)
plt.show()

"""

x = 0




VAE = m.VAE_v1()
losses = []
losses_test_2 = []
losses_test_2_model_1 = []
losses_test_2_model_2 = []
currRound = 0
while 1==1:

    print("training autoencoder, Round: "+str(x))

    for i in range(500):
        print("training autoencoder: "+str(i))
        # index = random.randint(0,1)
        # if index == 0:
        #     net_to_encode = flatModel1
        # elif index == 1:
        #     net_to_encode = flatModels[1]
        net_to_encode1 = flatModels[0]
        net_to_encode2 = flatModels[1]

        approx_weights_flattened1,approx_weights_flattened2, mu1, logvar1, mu2, logvar2, VAE  = trainVAE_dual(net_to_encode1,net_to_encode2, VAE)


        approx_weights_flattened1 = approx_weights_flattened1.data.numpy().reshape(21840)
        approxModel1 = m.unFlattenNetwork(approx_weights_flattened1, shapes)
        newModel1 = Net()


        approx_weights_flattened2 = approx_weights_flattened2.data.numpy().reshape(21840)
        approxModel2 = m.unFlattenNetwork(approx_weights_flattened2, shapes)
        newModel2 = Net()

    #MODEL 1
    sample =Variable(torch.randn(1, 10))
    apprx, latent_vars = VAE.decode(sample)
    apprx=apprx.data.numpy().reshape(21840)
    approxModel1 = m.unFlattenNetwork(apprx, shapes)
    newModel = Net()

    #load those weights into the network and test
    newModel1 = m.loadWeights_mnist(approxModel1, newModel)
    accuracy1 = test(newModel1)
    model1_performances.append(accuracy1)


    #MODEL 2
    sample =Variable(torch.randn(1, 10))
    apprx, latent_vars = VAE.decode(sample)
    apprx=apprx.data.numpy().reshape(21840)
    approxModel2 = m.unFlattenNetwork(apprx, shapes)
    newModel = Net()

    #load those weights into the network and test
    newModel2 = m.loadWeights_mnist(approxModel2, newModel)
    accuracy2 = test(newModel2)
    model2_performances.append(accuracy2)


    print(model1_performances)
    print(model2_performances)

    print(mu1)
    print(mu2)
    print(logvar1)
    print(logvar2)

    plt.plot(losses)
    plt.show()

    x+=1

#print(shapes)
