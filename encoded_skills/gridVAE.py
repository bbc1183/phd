"""
IDENTICAL TO EXP15 BUT I WILL USE A VAE INSTEAD OF AE.

TRAIN MODEL ON STATE 1, FOR TASK 1, TO LEARN SKILL 1
TRAIN AUTOENCODER ON SKILL 1

"""
import models as m
import gridworld as gw
import qnetwork as q

import torch
import torchvision
import torchvision.transforms as transforms
import torch.optim as optim
import torch.nn as nn
import random
import torch.nn.functional as F
from torch.autograd import Variable, Function
import numpy as np
import matplotlib.pyplot as plt
import sys
from sklearn.manifold import TSNE
state1 = gw.initGrid()
state2 = gw.initGrid_v2()

#train the models until they win
winStatus1 = 0
winStatus2 = 0
winStatus3 = 0
winStatus4 = 0

# while winStatus1 == 0:
#     model1_class1, paths1 = q.trainNetwork(in_state=state1)
#     model1_class1, paths1, finalReward1 = q.testNetwork_v2(model1_class1,inState=gw.stateToData(state1))
#     if finalReward1 == 20:
#         torch.save(model1_class1, "gridVAE_model1.pt")
#         winStatus1 = 1
#
# while winStatus2 == 0:
#     model2_class1, paths2 = q.trainNetwork(in_state=state2)
#     model2_class1, paths2, finalReward2 = q.testNetwork_v2(model2_class1,inState=gw.stateToData(state2))
#     if finalReward2 == 20:
#         torch.save(model2_class1, "gridVAE_model2.pt")
#         winStatus2 = 1


model1_class1 = torch.load("gridVAE_model1.pt")
model2_class1 = torch.load("gridVAE_model2.pt")


"""
EXTRACT THE WEIGHTS OF THE MODELS, FLATTEN THEM
"""



#FLATTEN THE MODELS
trainedModel_1_flat, shapes_1 = m.flattenNetwork(model1_class1)
trainedModel_2_flat, shapes_2 = m.flattenNetwork(model2_class1)

trained_models_flat = [trainedModel_1_flat, trainedModel_2_flat]


# model = m.Jaya_CIFAR()
# model_weights = torch.load("jay_blake_vae_weights.pt")
# model.load_state_dict(model_weights)
# trainedModel_1_flat, shapes_1 = m.flattenNetwork(model)




"""
-------------------------------------------------------------------------------------------
AE TRAINING AND TESTING FUNCTIONS
-------------------------------------------------------------------------------------------
"""
losses = []
def trainVAE(weights,VAE, epochs=1, factor=20):
    epochs = epochs
    VAE.train()
    # Define loss criterion
    criterion = nn.MSELoss()
    global losses

    for i in range(epochs):
        #learning_rate *= (1.0/(1.0+0.0002*i))

        ##### pytorch syntax ##################################################
        # zero the parameter gradients
        optimizer.zero_grad()
        # forward + backward + optimize
        inputs = Variable(torch.from_numpy(weights.reshape(1,len(weights))).float())
        outputs, mu, logvar  = VAE(inputs)
        #print(encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1]))

        loss = loss_function(outputs, inputs, mu, logvar)
        loss.backward()
        losses.append(loss.data[0])
        print("loss: "+str(loss.data[0]))
        optimizer.step()
        ##### pytorch syntax ##################################################
    approximated_weights, mu, logvar = VAE(inputs)
    #encoded_skill = encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1])

    # fig = plt.figure()
    # plt.plot(losses)
    # plt.show()
    return approximated_weights, mu, logvar,  VAE


# Reconstruction + KL divergence losses summed over all elements and batch
def loss_function(recon_x, x, mu, logvar):
    MSE = F.mse_loss(recon_x, x.view(-1, 2224), size_average=False)
    #BCE = F.binary_cross_entropy(recon_x, x.view(-1, 2224), size_average=False)
    # see Appendix B from VAE paper:
    # Kingma and Welling. Auto-Encoding Variational Bayes. ICLR, 2014
    # https://arxiv.org/abs/1312.6114
    # 0.5 * sum(1 + log(sigma^2) - mu^2 - sigma^2)
    KLD = -0.5 * torch.sum(1 + logvar - mu.pow(2) - logvar.exp())

    return MSE + KLD

def test_encoded_skill(sample, grid, VAE):
    VAE.eval()
    guessed_weights_flat, latent_vars = VAE.decode(sample)
    #guessed_weights_flat = AE.decoder(Variable(torch.from_numpy(encoded_skill_1.reshape(1,5))).float())
    guessed_weights_flat = guessed_weights_flat.data.numpy().reshape(2224)
    guessed_weights_unflat = m.unFlattenNetwork(guessed_weights_flat, shapes_1)
    net = m.Linear_Net()
    #load those weights into the network
    net = m.loadWeights(guessed_weights_unflat, net)
    #makeMove and get state.
    net,path,reward = q.testNetwork_v2(net, inState=grid)

    if len(path) > 10:
        testStatus = 0
        print("GAME LOST, too many moves")
    if reward == 20:
        testStatus = 0
        print("GAME WON!")
    if reward == -20:
        testStatus = 0
        print("GAME LOST")
    return reward, latent_vars


def test_encoded_skill_v2(apprxWeights_flat, grid, VAE, skill_dist):

    sample = Variable(torch.randn(1, 10))
    #guessed_weights_flat = AE.decoder(Variable(torch.from_numpy(encoded_skill_1.reshape(1,5))).float())
    guessed_weights_flat = apprxWeights_flat.data.numpy().reshape(2224)
    guessed_weights_unflat = m.unFlattenNetwork(guessed_weights_flat, shapes_1)
    net = m.Linear_Net()
    #load those weights into the network
    net = m.loadWeights(guessed_weights_unflat, net)
    #makeMove and get state.
    net,path,reward = q.testNetwork_v2(net, inState=grid)

    if len(path) > 10:
        testStatus = 0
        print("GAME LOST, too many moves")
    if reward == 20:
        testStatus = 0
        print("GAME WON!")
    if reward == -20:
        testStatus = 0
        print("GAME LOST")
    return reward, sample
"""
-------------------------------------------------------------------------------------------
END AE TRAINING AND TESTING FUNCTIONS
-------------------------------------------------------------------------------------------
"""




def GENERATE_SAMPLE_FROM_MU_STD(mu,sigma):
    z=torch.distributions.Normal(mu,torch.FloatTensor(np.sqrt(np.exp(sigma))))
    return z.sample()



VAE_win_status = 0
VAE = m.VAE_v2()
optimizer = optim.SGD(VAE.parameters(), lr=.001)
encoded_skills = []
losses = []
skill_dists = []
while VAE_win_status == 0:


    """
    trained models flat, they are clearly different
    [ 0.14263062 -0.17733839 -0.14443073 ..., -0.41046125  0.80579084
     -0.69798607]
    [-0.00612559  0.06590846 -0.0275375  ...,  1.11827636 -0.61436725
     -0.24041881]

    """

    for i in range(5000):
        print("training autoencoder: "+str(i))
        index = random.randint(0,1)
        # print(trained_models_flat[0])
        # print(trained_models_flat[1])

        net_to_encode = trained_models_flat[index]
        #approx_weights_flattened, mu, logvar, VAE = trainVAE(net_to_encode,VAE)

        optimizer.zero_grad()
        # forward + backward + optimize
        inputs = Variable(torch.from_numpy(net_to_encode.reshape(1,len(net_to_encode))).float())
        outputs, mu, logvar  = VAE(inputs)
        #print(encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1]))

        loss = loss_function(outputs, inputs, mu, logvar)
        loss.backward()
        losses.append(loss.data[0])
        print("loss: "+str(loss.data[0]))
        optimizer.step()


        if index == 0:
            skill1_dist = (mu,logvar)
            skill1_weights = outputs
        if index == 1:
            skill2_dist = (mu, logvar)
            skill2_weights = outputs





    #THIS IS THE MU AND LOGVAR FOR SKILL1
    #print(skill1_dist)
    mu = skill1_dist[0].data.numpy()
    logvar = skill1_dist[1].data.numpy()

    #mu2 = skill2_dist[0].data.numpy()
    #logvar2 = skill2_dist[1].data.numpy()
    print(skill1_weights)
    #print(skill2_weights)
    print("////////////////////////////////////////////")
    print("MU: ")
    print(mu)
    #print(mu2)
    print("////////////////////////////////////////////")
    print("LOGVAR: ")
    print(logvar)
    #print(logvar2)
    print("////////////////////////////////////////////")






    winCount_1 = 0
    winCount_2 = 0
    model1_status = 0
    model2_status = 0
    skill1_vars = []
    skill2_vars = []
    neither = []
    allSamples = []
    for i in range(50):
        #sample = Variable(torch.randn(1, 10))

        if(random.randint(0,1) == 0):
            sample = GENERATE_SAMPLE_FROM_MU_STD(skill1_dist[0], skill1_dist[1])
        else:
            sample = GENERATE_SAMPLE_FROM_MU_STD(skill2_dist[0], skill2_dist[1])
        # sample2 = GENERATE_SAMPLE_FROM_MU_STD(skill2_dist[0], skill2_dist[1])
        #try to generate a sameple from skill1 mu and logvar

        # print("here")
        # sys.exit()

        status_1, latent_vars = test_encoded_skill(sample, gw.stateToData(state1), VAE)
        status_2, latent_vars2 = test_encoded_skill(sample, gw.stateToData(state2), VAE)

        # status_1, latent_vars = test_encoded_skill_v2(approx_weights_flattened, gw.stateToData(state1), VAE, skill1_dist)
        # status_2, latent_vars2 = test_encoded_skill_v2(approx_weights_flattened, gw.stateToData(state2), VAE, skill2_dist)


        #THESE ARE ALL THE SAME ###################################################
        # print(sample)
        # print(latent_vars)
        # print(latent_vars2)
        # sys.exit()
        #######################################################################
        allSamples.append(sample.data.numpy()[0])
        print("attempt 1")
        if status_1 == 20:
            print("win for state 1")
            # print(skill1_dist)
            # print(latent_vars.data.numpy()[0])
            # sys.exit()
            winCount_1 += 1
            skill1_vars.append(sample.data.numpy()[0])
            if winCount_1 == 50:
                #encoded_skills.append(encoded_skill)
                print("MODEL_1:  VICTORY FOR ALL LATENT_SAMPLES")
                model1_status = 1


        elif status_2 == 20:
            winCount_2 += 1
            skill2_vars.append(sample.data.numpy()[0])
            if winCount_2 == 50:
                #encoded_skills.append(encoded_skill)
                print("MODEL_2:  VICTORY FOR ALL LATENT_SAMPLES")
                model2_status = 1
        elif status_1 != 20 and status_2 != 20:
            neither.append(sample.data.numpy()[0])

    if model1_status == 1 and model2_status == 1:
        print("VICTORY FOR BOTH MODELS")
        VAE_win_status = 1
    else:
        print("Model 1 Win Count: "+str(winCount_1))
        print("Model 2 Win Count: "+str(winCount_2))


    fig = plt.figure()
    plt.plot(losses)
    plt.show()

    skill1_vars = np.array(skill1_vars)
    skill2_vars = np.array(skill2_vars)
    neither_vars = np.array(neither)
    # print(skill1_vars)
    # sys.exit()
    #skill_batch = TSNE(n_components=2).fit_transform(all_vars)
    if len(skill1_vars) > 1:
        skill_batch1 = TSNE(n_components=2).fit_transform(skill1_vars)
        skills1 = np.array(skill_batch1)
        plt.scatter(skills1[:,0], skills1[:,1], c="green")
    if len(skill2_vars) > 1:
        skill_batch2 = TSNE(n_components=2).fit_transform(skill2_vars)
        skills2 = np.array(skill_batch2)
        plt.scatter(skills2[:,0], skills2[:,1], c="blue")
    if len(neither_vars) > 1:
        neither_batch = TSNE(n_components=2).fit_transform(neither_vars)
        neither_batch = np.array(neither_batch)
        plt.scatter(neither_batch[:,0], neither_batch[:,1], c="red")

    plt.show()
