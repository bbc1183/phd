import torch
import torchvision
import torchvision.transforms as transforms
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable, Function

# Other imports
import sys
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pickle
import os

class Linear(nn.Module):
    # Constructor
    def __init__(self, input_features, output_features, bias=True):
        super(Linear, self).__init__()
        self.input_features = input_features
        self.output_features = output_features
        minInitVal = -0.1
        maxInitVal = 0.1

        self.weight = nn.Parameter(torch.FloatTensor(self.output_features,
            self.input_features).uniform_(minInitVal,maxInitVal))
        if bias:
            self.bias = nn.Parameter(torch.FloatTensor(output_features
                ).uniform_(minInitVal,maxInitVal))
        else:
            self.register_parameter('bias', None)

    # Forward method
    def forward(self, input):
        output = input.mm(self.weight.t())
        if self.bias is not None:
            output += self.bias.unsqueeze(0).expand_as(output)
        return output


class Jaya_CIFAR(nn.Module):
    def __init__(self):
        super(Jaya_CIFAR, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1 = nn.Linear(16 * 5 * 5, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = x.view(-1, 16 * 5 * 5)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x


######################################################################
#     MODELS
######################################################################
class Linear_Net(nn.Module):
    def __init__(self):
        super(Linear_Net, self).__init__()
        self.fc1 = Linear(32, 48)
        self.fc2 = Linear(48, 12)
        #self.fc3 = Linear(48, 20)
        self.fc3 = Linear(12, 4)
        #self.sm = nn.LogSoftmax()

    def forward(self, x):
        x = F.leaky_relu(self.fc1(x))
        x = F.leaky_relu(self.fc2(x))
        #x = F.leaky_relu(self.fc3(x))
        x = self.fc3(x)
        #x = self.sm(x)
        return x


class Linear_Discriminator(nn.Module):
    def __init__(self):
        super(Linear_Discriminator, self).__init__()
        self.fc1 = Linear(2224, 200)
        self.fc2 = Linear(200, 25)
        self.fc3 = Linear(25, 1)


    def forward(self, x):
        x = F.leaky_relu(self.fc1(x))
        x = F.leaky_relu(self.fc2(x))
        x = F.sigmoid(self.fc3(x))
        return x


class VAE_v1(nn.Module):
    def __init__(self):
        super(VAE_v1, self).__init__()

        #mnist2_vae works best (for 1 skill) when hidden layer is 20-100
        self.fc1 = nn.Linear(21840, 50)
        self.fc21 = nn.Linear(50, 10)
        self.fc22 = nn.Linear(50, 10)
        self.fc3 = nn.Linear(10, 50)
        self.fc4 = nn.Linear(50, 21840)

        self.relu = nn.ReLU()
        self.sigmoid = nn.Sigmoid()

    def encode(self, x):
        h1 = F.tanh(self.fc1(x))
        return self.fc21(h1), self.fc22(h1)

    def reparameterize(self, mu, logvar):
        if self.training:
            std = logvar.mul(0.5).exp_()
            eps = Variable(std.data.new(std.size()).normal_())
            return eps.mul(std).add_(mu)
        else:
            return mu

    def decode(self, z):
        h3 = F.tanh(self.fc3(z))
        return self.fc4(h3),z

    def forward(self, x):
        mu, logvar = self.encode(x.view(-1, 21840))
        z = self.reparameterize(mu, logvar)
        return self.decode(z)[0], mu, logvar

class VAE_v2(nn.Module):
    def __init__(self):
        super(VAE_v2, self).__init__()

        self.fc1 = nn.Linear(2224, 20)
        self.fc21 = nn.Linear(20, 10)
        self.fc22 = nn.Linear(20, 10)
        self.fc3 = nn.Linear(10, 20)
        self.fc4 = nn.Linear(20, 2224)

        self.relu = nn.ReLU()
        self.sigmoid = nn.Sigmoid()

    def encode(self, x):
        h1 = F.tanh(self.fc1(x))
        return self.fc21(h1), self.fc22(h1)

    def reparameterize(self, mu, logvar):
        if self.training:
            std = logvar.mul(0.5).exp_()
            eps = Variable(std.data.new(std.size()).normal_())
            return eps.mul(std).add_(mu)
        else:
            return mu

    def decode(self, z):
        h3 = F.tanh(self.fc3(z))
        return self.fc4(h3),z

    def forward(self, x):
        mu, logvar = self.encode(x.view(-1, 2224))
        z = self.reparameterize(mu, logvar)
        return self.decode(z)[0], mu, logvar

class VAE_v3(nn.Module):
    def __init__(self):
        super(VAE_v3, self).__init__()

        self.fc1 = nn.Linear(21840, 800)
        self.fc21 = nn.Linear(800, 20)
        self.fc22 = nn.Linear(800, 20)
        self.fc3 = nn.Linear(20, 800)
        self.fc4 = nn.Linear(800, 21840)

        self.relu = nn.ReLU()
        self.sigmoid = nn.Sigmoid()

    def encode(self, x):
        h1 = F.tanh(self.fc1(x))
        return self.fc21(h1), self.fc22(h1)

    def reparameterize(self, mu, logvar):
        if self.training:
            std = logvar.mul(0.5).exp_()
            eps = Variable(std.data.new(std.size()).normal_())
            return eps.mul(std).add_(mu)
        else:
            return mu

    def decode(self, z):
        h3 = F.tanh(self.fc3(z))
        return self.fc4(h3),z

    def forward(self, x):
        mu, logvar = self.encode(x.view(-1, 21840))
        z = self.reparameterize(mu, logvar)
        return self.decode(z)[0], mu, logvar






class Encoder(nn.Module):
    def __init__(self, inSize, redux_factor):
        super(Encoder, self).__init__()
        redux_1 = int(inSize/redux_factor)
        redux_2 = int(redux_1/2)
        redux_3 = int(redux_1/redux_factor)

        self.fc1 = Linear(inSize, redux_1)
        #self.fc2 = Linear(redux_1, redux_2)
        self.fc3 = Linear(redux_1, 4)

        #self.sm = nn.LogSoftmax()

    def forward(self, x):
        x1 = F.relu(self.fc1(x))
        #x = F.relu(self.fc2(x))
        x2 = F.sigmoid(self.fc3(x1))
        #x = self.sm(x)
        return x2

class Encoder_v5(nn.Module):
    def __init__(self):
        super(Encoder_v5, self).__init__()
        self.fc1 = Linear(2224, 4)
        self.fc2 = Linear(100, 20)
        self.fc3 = Linear(20, 4)

    def forward(self, x):
        x1 = F.leaky_relu(self.fc1(x))
        x2 = F.leaky_relu(self.fc2(x1))
        x3 = F.leaky_relu(self.fc3(x2))
        return x3, x2, x1

class Encoder_v6(nn.Module):
    def __init__(self):
        super(Encoder_v6, self).__init__()
        self.fc1 = Linear(21840, 5)
        #self.fc2 = Linear(500, 100)

    def forward(self, x):
        x1 = F.tanh(self.fc1(x))
        #x2 = F.leaky_relu(self.fc2(x1))
        return  x1

class Encoder_cifar(nn.Module):
    def __init__(self):
        super(Encoder_cifar, self).__init__()
        self.fc1 = Linear(62006, 2)
        #self.fc2 = Linear(500, 100)

    def forward(self, x):
        x1 = F.leaky_relu(self.fc1(x))
        #x2 = F.leaky_relu(self.fc2(x1))
        return  x1


class Encoder_v7(nn.Module):
    def __init__(self):
        super(Encoder_v7, self).__init__()
        self.fc1 = Linear(2224, 4)
        #self.fc2 = Linear(500, 100)

    def forward(self, x):
        x1 = F.leaky_relu(self.fc1(x))
        #x2 = F.leaky_relu(self.fc2(x1))
        return  x1

class Decoder_v6(nn.Module):
    def __init__(self):
        super(Decoder_v6, self).__init__()
        self.fc1 = Linear(5, 21840)
        #self.fc2 = Linear(500, 21840)

    def forward(self, x):
        #x = F.leaky_relu(self.fc1(x))
        x = F.tanh(self.fc1(x))
        return x

class Decoder_cifar(nn.Module):
    def __init__(self):
        super(Decoder_cifar, self).__init__()
        self.fc1 = Linear(2, 62006)
        #self.fc2 = Linear(500, 21840)

    def forward(self, x):
        #x = F.leaky_relu(self.fc1(x))
        x = self.fc1(x)
        return x

class Decoder_v7(nn.Module):
    def __init__(self):
        super(Decoder_v7, self).__init__()
        self.fc1 = Linear(4, 2224)
        #self.fc2 = Linear(500, 21840)

    def forward(self, x):
        #x = F.leaky_relu(self.fc1(x))
        x = self.fc1(x)
        return x


class Decoder_v5(nn.Module):
    def __init__(self):
        super(Decoder_v5, self).__init__()
        self.fc1 = Linear(4, 20)
        self.fc2 = Linear(20, 100)
        self.fc3 = Linear(100, 2224)

    def forward(self, x):
        x = F.leaky_relu(self.fc1(x))
        x = F.leaky_relu(self.fc2(x))
        x = self.fc3(x)

        return x

class Encoder_v2(nn.Module):
    def __init__(self, inSize, redux_factor):
        super(Encoder_v2, self).__init__()
        redux_1 = int(inSize/redux_factor)
        redux_2 = int(redux_1/2)
        redux_3 = int(redux_1/redux_factor)

        self.fc1 = Linear(inSize, redux_1)
        #self.fc2 = Linear(redux_1, redux_2)
        self.fc3 = Linear(redux_1, 2)

        #self.sm = nn.LogSoftmax()

    def forward(self, x):
        x1 = F.relu(self.fc1(x))
        #x = F.relu(self.fc2(x))
        x2 = F.sigmoid(self.fc3(x1))
        #x = self.sm(x)
        return x2

class Encoder_v3(nn.Module):
    def __init__(self):
        super(Encoder_v3, self).__init__()


        self.fc1 = Linear(2224, 400)
        self.fc2 = Linear(400, 300)
        self.fc3 = Linear(300, 100)
        self.fc4 = Linear(100, 20)
        self.fc5 = Linear(20, 4)

        #self.sm = nn.LogSoftmax()

    def forward(self, x):
        x1 = F.sigmoid(self.fc1(x))
        #x1 = F.dropout(x1)
        x2 = F.sigmoid(self.fc2(x1))
        #x2 = F.dropout(x2)
        x3 = F.sigmoid(self.fc3(x2))
        #x3 = F.dropout(x3)
        x4 = F.sigmoid(self.fc4(x3))
        #x4 = F.dropout(x4)
        x5 = F.sigmoid(self.fc5(x4))
        #x = self.sm(x)
        return x5

class Encoder_v4(nn.Module):
    def __init__(self):
        super(Encoder_v4, self).__init__()


        self.fc1 = Linear(2224, 1000)
        self.fc2 = Linear(1000, 800)
        self.fc3 = Linear(800, 500)
        self.fc4 = Linear(500, 300)
        self.fc5 = Linear(300, 100)

        #self.sm = nn.LogSoftmax()

    def forward(self, x):
        x1 = F.sigmoid(self.fc1(x))
        #x1 = F.dropout(x1)
        x2 = F.sigmoid(self.fc2(x1))
        #x2 = F.dropout(x2)
        x3 = F.sigmoid(self.fc3(x2))
        #x3 = F.dropout(x3)
        x4 = F.sigmoid(self.fc4(x3))
        #x4 = F.dropout(x4)
        x5 = F.sigmoid(self.fc5(x4))
        #x = self.sm(x)
        return x5

class State_Encoder(nn.Module):
    def __init__(self):
        super(State_Encoder, self).__init__()
        self.fc1 = Linear(32, 10)

    def forward(self, x):
        x1 = F.leaky_relu(self.fc1(x))
        return x1

class Decoder(nn.Module):
    def __init__(self, outSize, redux_factor):
        super(Decoder, self).__init__()
        redux_1 = int(outSize/redux_factor)
        redux_2 = int(redux_1/2)
        redux_3 = int(redux_1/redux_factor)

        self.fc1 = Linear(4, redux_1)
        #self.fc2 = Linear(redux_2, redux_1)
        self.fc3 = Linear(redux_1, outSize)
        #self.sm = nn.LogSoftmax()

    def forward(self, x):
        x = F.leaky_relu(self.fc1(x))
        #x = F.relu(self.fc2(x))
        x = self.fc3(x)
        #x = self.fc2(x)
        #x = self.sm(x)
        return x



class Decoder_v2(nn.Module):
    def __init__(self, outSize, redux_factor):
        super(Decoder_v2, self).__init__()
        redux_1 = int(outSize/redux_factor)
        redux_2 = int(redux_1/2)
        redux_3 = int(redux_1/redux_factor)

        self.fc1 = Linear(2, redux_1)
        #self.fc2 = Linear(redux_2, redux_1)
        self.fc3 = Linear(redux_1, outSize)
        #self.sm = nn.LogSoftmax()

    def forward(self, x):
        x = F.relu(self.fc1(x))
        #x = F.relu(self.fc2(x))
        x = self.fc3(x)
        #x = self.fc2(x)
        #x = self.sm(x)
        return x

class Decoder_v3(nn.Module):
    def __init__(self):
        super(Decoder_v3, self).__init__()


        self.fc1 = Linear(4, 20)
        self.fc2 = Linear(20, 100)
        self.fc3 = Linear(100, 300)
        self.fc4 = Linear(300, 400)
        self.fc5 = Linear(400, 2224)
        #self.sm = nn.LogSoftmax()

    def forward(self, x):
        x = F.sigmoid(self.fc1(x))
        x = F.sigmoid(self.fc2(x))
        #x = F.dropout(x)
        x = F.sigmoid(self.fc3(x))
        #x = F.dropout(x)
        x = F.sigmoid(self.fc4(x))
        #x = F.relu(self.fc2(x))
        x = self.fc5(x)
        #x = self.fc2(x)
        #x = self.sm(x)
        return x

class Decoder_v4(nn.Module):
    def __init__(self):
        super(Decoder_v4, self).__init__()


        self.fc1 = Linear(100, 300)
        self.fc2 = Linear(300, 500)
        self.fc3 = Linear(500,800)
        self.fc4 = Linear(800, 1000)
        self.fc5 = Linear(1000, 2224)
        #self.sm = nn.LogSoftmax()

    def forward(self, x):
        x = F.sigmoid(self.fc1(x))
        x = F.sigmoid(self.fc2(x))
        #x = F.dropout(x)
        x = F.sigmoid(self.fc3(x))
        #x = F.dropout(x)
        x = F.sigmoid(self.fc4(x))
        #x = F.relu(self.fc2(x))
        x = self.fc5(x)
        #x = self.fc2(x)
        #x = self.sm(x)
        return x

class State_Decoder(nn.Module):
    def __init__(self):
        super(State_Decoder, self).__init__()
        self.fc1 = Linear(10, 32)

    def forward(self, x):

        x3 = F.hardtanh(self.fc1(x),-1,1)
        return x3

class AutoEncoder(nn.Module):
    def __init__(self, inSize, factor):
        super(AutoEncoder, self).__init__()
        self.encoder = Encoder(inSize, factor)
        self.decoder = Decoder(inSize, factor)

    def forward(self, x):
        return self.decoder(self.encoder(x)), self.encoder(x)

class AutoEncoder_v5(nn.Module):
    def __init__(self):
        super(AutoEncoder_v5, self).__init__()
        self.encoder = Encoder_v5()
        self.decoder = Decoder_v5()


    def forward(self, x):
        return self.decoder(self.encoder(x)[0]), self.encoder(x)[0], self.encoder(x)[1], self.encoder(x)[2]

class AutoEncoder_v6(nn.Module):
    def __init__(self):
        super(AutoEncoder_v6, self).__init__()
        self.encoder = Encoder_v6()
        self.decoder = Decoder_v6()


    def forward(self, x):
        return self.decoder(self.encoder(x)), self.encoder(x)

class AutoEncoder_cifar(nn.Module):
    def __init__(self):
        super(AutoEncoder_cifar, self).__init__()
        self.encoder = Encoder_cifar()
        self.decoder = Decoder_cifar()


    def forward(self, x):
        return self.decoder(self.encoder(x)), self.encoder(x)

class AutoEncoder_v7(nn.Module):
    def __init__(self):
        super(AutoEncoder_v7, self).__init__()
        self.encoder = Encoder_v7()
        self.decoder = Decoder_v7()


    def forward(self, x):
        return self.decoder(self.encoder(x)), self.encoder(x)

class AutoEncoder_v2(nn.Module):
    def __init__(self, inSize, factor):
        super(AutoEncoder_v2, self).__init__()
        self.encoder = Encoder_v2(inSize, factor)
        self.decoder = Decoder_v2(inSize, factor)

    def forward(self, x):
        return self.decoder(self.encoder(x)), self.encoder(x)

class AutoEncoder_v3(nn.Module):
    def __init__(self):
        super(AutoEncoder_v3, self).__init__()
        self.encoder = Encoder_v3()
        self.decoder = Decoder_v3()

    def forward(self, x):
        return self.decoder(self.encoder(x)), self.encoder(x)

class AutoEncoder_v4(nn.Module):
    def __init__(self):
        super(AutoEncoder_v4, self).__init__()
        self.encoder = Encoder_v3()
        self.decoder = Decoder_v3()

    def forward(self, x):
        return self.decoder(self.encoder(x)), self.encoder(x)

class State_AutoEncoder(nn.Module):
    def __init__(self):
        super(State_AutoEncoder, self).__init__()
        self.encoder = State_Encoder()
        self.decoder = State_Decoder()

    def forward(self, x):
        return self.decoder(self.encoder(x)), self.encoder(x)


class State_To_Skill_Mapper(nn.Module):
    def __init__(self):
        super(State_To_Skill_Mapper, self).__init__()
        self.fc1 = Linear(32, 64)
        self.fc2 = Linear(64, 48)
        self.fc3 = Linear(48, 30)
        self.fc4 = Linear(30, 20)
        self.fc5 = Linear(20, 12)
        self.fc6 = Linear(12, 5)


    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = F.relu(self.fc3(x))
        x = F.relu(self.fc4(x))
        x = F.relu(self.fc5(x))
        x = self.fc6(x)

        return x


"""
THIS WILL FLATTEN AN ENTIRE NETWORK AND PREPARE IT TO BE FED AS INPUT TO AN
AUTO-ENCODER

IT RETURNS A 1D NUMPY ARRAY
"""
def flattenNetwork(net):
    flatNet = []
    shapes = []
    for param in net.parameters():
        #if its WEIGHTS
        curr_shape = param.data.numpy().shape
        shapes.append(curr_shape)
        if len(curr_shape) == 2:
            param = param.data.numpy().reshape(curr_shape[0]*curr_shape[1])
            flatNet.append(param)
        elif len(curr_shape) == 4:
            param = param.data.numpy().reshape(curr_shape[0]*curr_shape[1]*curr_shape[2]*curr_shape[3])
            flatNet.append(param)
        else:
            param = param.data.numpy().reshape(curr_shape[0])
            flatNet.append(param)
    finalNet = []
    for obj in flatNet:
        for x in obj:
            finalNet.append(x)
    finalNet = np.array(finalNet)
    return finalNet,shapes

"""
THIS WILL UN-FLATTEN APPROXIMATE WEIGHTS SO THAT THEY CAN BE LOADED BACK INTO
THE ORIGINAL MODEL
"""
def unFlattenNetwork(weights, shapes):
    #this is how we know how to slice weights

    begin_slice = 0
    end_slice = 0
    finalParams = []
    #print(len(weights))
    for idx,shape in enumerate(shapes):
        if len(shape) == 2:
            end_slice = end_slice+(shape[0]*shape[1])
            curr_slice = weights[begin_slice:end_slice]
            param = np.array(curr_slice).reshape(shape[0], shape[1])
            finalParams.append(param)
            begin_slice = end_slice
        elif len(shape) == 4:
            end_slice = end_slice+(shape[0]*shape[1]*shape[2]*shape[3])
            curr_slice = weights[begin_slice:end_slice]
            #print("shape: "+str(shape))
            #print("curr_slice: "+str(curr_slice.shape))
            param = np.array(curr_slice).reshape(shape[0], shape[1], shape[2], shape[3])
            finalParams.append(param)
            begin_slice = end_slice
        else:
            end_slice = end_slice+shape[0]
            curr_slice = weights[begin_slice:end_slice]
            param = np.array(curr_slice).reshape(shape[0],)
            finalParams.append(param)
            begin_slice = end_slice
    finalArr = np.array(finalParams)
    return np.array(finalArr)



"""
HELPER FUNCTION TO LOAD WEIGHTS INTO MODEL
"""
def loadWeights(weights_to_load, net):
    net.fc1.weight.data = torch.from_numpy(weights_to_load[0])
    net.fc1.bias.data = torch.from_numpy(weights_to_load[1])
    net.fc2.weight.data = torch.from_numpy(weights_to_load[2])
    net.fc2.bias.data = torch.from_numpy(weights_to_load[3])
    net.fc3.weight.data = torch.from_numpy(weights_to_load[4])
    net.fc3.bias.data = torch.from_numpy(weights_to_load[5])
    return net

def loadWeights_mnist(weights_to_load, net):
    net.conv1.weight.data = torch.from_numpy(weights_to_load[0])
    net.conv1.bias.data =   torch.from_numpy(weights_to_load[1])
    net.conv2.weight.data = torch.from_numpy(weights_to_load[2])
    net.conv2.bias.data =   torch.from_numpy(weights_to_load[3])
    net.fc1.weight.data =   torch.from_numpy(weights_to_load[4])
    net.fc1.bias.data =     torch.from_numpy(weights_to_load[5])
    net.fc2.weight.data =   torch.from_numpy(weights_to_load[6])
    net.fc2.bias.data =     torch.from_numpy(weights_to_load[7])
    return net

def loadWeights_cifar(weights_to_load, net):
    net.conv1.weight.data = torch.from_numpy(weights_to_load[0])
    net.conv1.bias.data =   torch.from_numpy(weights_to_load[1])
    net.conv2.weight.data = torch.from_numpy(weights_to_load[2])
    net.conv2.bias.data =   torch.from_numpy(weights_to_load[3])
    net.fc1.weight.data =   torch.from_numpy(weights_to_load[4])
    net.fc1.bias.data =     torch.from_numpy(weights_to_load[5])
    net.fc2.weight.data =   torch.from_numpy(weights_to_load[6])
    net.fc2.bias.data =     torch.from_numpy(weights_to_load[7])
    net.fc3.weight.data =   torch.from_numpy(weights_to_load[8])
    net.fc3.bias.data =     torch.from_numpy(weights_to_load[9])
    return net
