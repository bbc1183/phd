"""
THIS IS AN EXTENSION OF EXPERIMENT 7.  BUT I WILL TRAIN MAPPER ON ALL SUBSEQUENT STATES AS WELL.
FINDINGS:
"""


import models as m
import gridworld as gw
import qnetwork as q

import torch
import torchvision
import torchvision.transforms as transforms
import torch.optim as optim
import torch.nn as nn
import random
import torch.nn.functional as F
from torch.autograd import Variable, Function
import numpy as np
import matplotlib.pyplot as plt
import sys
import pandas as pd
from sklearn import linear_model

"""
INITIALIZE 5 DIFFERENT GAME BOARDS, SAVE THE INITIAL STATES
TRAIN AGENTS ON THESE BOARDS
"""
states = gw.get6GameBoards()
training_states = states[0:5]
test_state = states[5]

#train the models until they win
winStatus1 = 0
winStatus2 = 0
winStatus3 = 0
winStatus4 = 0
winStatus5 = 0
while winStatus1 == 0:
    trainedModel_1, paths1 = q.trainNetwork(in_state=training_states[0])
    trainedModel_1, paths1, finalReward1 = q.testNetwork_v2(trainedModel_1,inState=gw.stateToData(training_states[0]))
    if finalReward1 == 20:
        winStatus1 = 1
while winStatus2 == 0:
    trainedModel_2, paths2 = q.trainNetwork(in_state=training_states[1])
    trainedModel_2, paths2, finalReward2 = q.testNetwork_v2(trainedModel_2,inState=gw.stateToData(training_states[1]))
    if finalReward2 == 20:
        winStatus2 = 1
while winStatus3 == 0:
    trainedModel_3, paths3 = q.trainNetwork(in_state=training_states[2])
    trainedModel_3, paths3, finalReward3 = q.testNetwork_v2(trainedModel_3,inState=gw.stateToData(training_states[2]))
    if finalReward3 == 20:
        winStatus3 = 1
while winStatus4 == 0:
    trainedModel_4, paths4 = q.trainNetwork(in_state=training_states[3])
    trainedModel_4, paths4, finalReward4 = q.testNetwork_v2(trainedModel_4,inState=gw.stateToData(training_states[3]))
    if finalReward4 == 20:
        winStatus4 = 1
while winStatus5 == 0:
    trainedModel_5, paths5 = q.trainNetwork(in_state=training_states[4])
    trainedModel_5, paths5, finalReward5 = q.testNetwork_v2(trainedModel_5,inState=gw.stateToData(training_states[4]))
    if finalReward5 == 20:
        winStatus5 = 1


"""
COMPRESS THE WEIGHTS TO ENCODED_SKILLS
SAVE THE DECODER (SHUFFLE OVER ALL SKILLS TO GENERALIZE)
SAVE ENCODED_SKILLS IN AN ARRAY
"""

#FLATTEN THE MODELS
trainedModel_1_flat, shapes_1 = m.flattenNetwork(trainedModel_1)
trainedModel_2_flat, shapes_2 = m.flattenNetwork(trainedModel_2)
trainedModel_3_flat, shapes_3 = m.flattenNetwork(trainedModel_3)
trainedModel_4_flat, shapes_4 = m.flattenNetwork(trainedModel_4)
trainedModel_5_flat, shapes_5 = m.flattenNetwork(trainedModel_5)
trained_models_flat = [trainedModel_1_flat, trainedModel_2_flat, trainedModel_3_flat, trainedModel_4_flat, trainedModel_5_flat]
losses = []
def trainAE(weights,auto_encoder, epochs=1, factor=20):
    epochs = epochs
    # Define loss criterion
    criterion = nn.MSELoss()
    learning_rate = .7
    global losses
    for i in range(epochs):
        #learning_rate *= (1.0/(1.0+0.0002*i))
        optimizer = optim.SGD(AE.parameters(), lr=learning_rate, momentum=0.9)
        ##### pytorch syntax ##################################################
        # zero the parameter gradients
        optimizer.zero_grad()
        # forward + backward + optimize
        outputs, encoded_skill = AE(Variable(torch.from_numpy(weights.reshape(1,len(weights))).float()))
        #print(encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1]))
        encoded_skill =  encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1])
        labels = Variable(torch.from_numpy(weights))
        loss = criterion(outputs, labels)
        #print(loss.data.numpy()[0])
        losses.append(loss.data.numpy()[0])
        loss.backward()
        optimizer.step()
        ##### pytorch syntax ##################################################
    approximated_weights, encoded_skill = AE(Variable(torch.from_numpy(weights.reshape(1,len(weights))).float()))
    encoded_skill = encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1])
    # fig = plt.figure()
    # plt.plot(losses)
    # plt.show()
    return approximated_weights, encoded_skill, AE

AE = m.AutoEncoder(inSize=2224, factor=20)
final_state_skill_1 = ()
final_state_skill_2 = ()
final_state_skill_3 = ()
final_state_skill_4 = ()
final_state_skill_5 = ()
for i in range(30000):
    print("training autoencoder: "+str(i))
    index = random.randint(0,len(trained_models_flat)-1)
    net_to_encode = trained_models_flat[index]
    approx_weights_flattened, encoded_skill, AE = trainAE(net_to_encode,AE)
    if index == 0:
        final_state_skill_1 = (training_states[0], encoded_skill)
    if index == 1:
        final_state_skill_2 = (training_states[1], encoded_skill)
    if index == 2:
        final_state_skill_3 = (training_states[2], encoded_skill)
    if index == 3:
        final_state_skill_4 = (training_states[3], encoded_skill)
    if index == 4:
        final_state_skill_5 = (training_states[4], encoded_skill)



final_states_and_skills = [final_state_skill_1,final_state_skill_2, final_state_skill_3, final_state_skill_4, final_state_skill_5]


print("########################################################")
print("Testing Trained Models")
print("########################################################")

model_1, path1, reward1 = q.testNetwork_v2(trainedModel_1,inState=gw.stateToData(training_states[0]))
model_2, path2, reward2 = q.testNetwork_v2(trainedModel_2,inState=gw.stateToData(training_states[1]))
model_3, path3, reward3 = q.testNetwork_v2(trainedModel_3,inState=gw.stateToData(training_states[2]))
model_4, path4, reward4 = q.testNetwork_v2(trainedModel_4,inState=gw.stateToData(training_states[3]))
model_5, path5, reward5 = q.testNetwork_v2(trainedModel_5,inState=gw.stateToData(training_states[4]))


#WE NEED ALL SUBSEQUENT STATES FOR A PARTICULAR SKILL
allStates_allSkills = []
for idx,state_skill in enumerate(final_states_and_skills):

    state = state_skill[0]
    skill = state_skill[1]
    train_state = gw.stateToData(state)
    curr_state = state
    curr_state_train = train_state
    allStates_in_path = []
    allStates_in_path.append(curr_state_train)


    if idx == 0:
        for move in path1:
            newState = gw.makeMove(curr_state, move)
            newState_train = gw.stateToData(newState)
            curr_state = newState
            curr_state_train = newState_train
            allStates_in_path.append(curr_state_train)
        #allStates now contains all states/grids for the given path/skill
        for state in allStates_in_path:
            allStates_allSkills.append((state, skill))
    if idx == 1:
        for move in path2:
            newState = gw.makeMove(curr_state, move)
            newState_train = gw.stateToData(newState)
            curr_state = newState
            curr_state_train = newState_train
            allStates_in_path.append(curr_state_train)
        #allStates now contains all states/grids for the given path/skill
        for state in allStates_in_path:
            allStates_allSkills.append((state, skill))
    if idx == 2:
        for move in path3:
            newState = gw.makeMove(curr_state, move)
            newState_train = gw.stateToData(newState)
            curr_state = newState
            curr_state_train = newState_train
            allStates_in_path.append(curr_state_train)
        #allStates now contains all states/grids for the given path/skill
        for state in allStates_in_path:
            allStates_allSkills.append((state, skill))
    if idx == 3:
        for move in path4:
            newState = gw.makeMove(curr_state, move)
            newState_train = gw.stateToData(newState)
            curr_state = newState
            curr_state_train = newState_train
            allStates_in_path.append(curr_state_train)
        #allStates now contains all states/grids for the given path/skill
        for state in allStates_in_path:
            allStates_allSkills.append((state, skill))
    if idx == 4:
        for move in path5:
            newState = gw.makeMove(curr_state, move)
            newState_train = gw.stateToData(newState)
            curr_state = newState
            curr_state_train = newState_train
            allStates_in_path.append(curr_state_train)
        #allStates now contains all states/grids for the given path/skill
        for state in allStates_in_path:
            allStates_allSkills.append((state, skill))
"""
allStates_allSkills NOW CONTAINS ALL STATE, SKILL PAIRS WE NEED TO TRAIN THE MAPPER
"""
print(allStates_allSkills)
print("######################################################")
print("VIEWING LOSSES FOR AE TRAINING")
print("######################################################")
fig = plt.figure()
plt.plot(losses)
plt.show()

"""
TRAIN THE MAPPER ON THE INITIAL STATES AND ENCODED SKILLS
VIEW THE LOSS
"""


"""
NEED TO WRAP TRAINING OF THE MAPPER IN ITS OWN FUNCTION SO I CAN CALL IT FROM THE TERMINAL
WITH DIFFERENT PARAMETERS
"""
def trainMapper(states_and_skills, epochs, learning_rate):
    mapper = m.State_To_Skill_Mapper()
    # Define loss criterion
    criterion = nn.MSELoss()
    learning_rate = learning_rate
    losses = []
    sample = []
    mapper_losses = []
    for i in range(epochs):
        print("Mapper Training: "+str(i))
        idx = random.randint(0, len(states_and_skills)-1)

        sample = states_and_skills[idx]

        state = sample[0][0]
        skill = sample[1]



        #learning_rate *= (1.0/(1.0+0.0002*i))
        optimizer = optim.SGD(mapper.parameters(), lr=learning_rate, momentum=0.9)
        ##### pytorch syntax ##################################################
        # zero the parameter gradients
        optimizer.zero_grad()
        # forward + backward + optimize

        outputs = mapper(Variable(torch.from_numpy(state.reshape(1,32)).float()))

        labels = Variable(torch.from_numpy(skill))
        loss = criterion(outputs, labels)
        #print(loss.data.numpy()[0])
        mapper_losses.append(loss.data.numpy()[0])
        loss.backward()
        optimizer.step()
        ##### pytorch syntax ##################################################
    print("######################################################")
    print("VIEWING LOSSES FOR MAPPER")
    print("######################################################")
    fig = plt.figure()
    plt.plot(mapper_losses)
    plt.show()
    return mapper
mapper = trainMapper(allStates_allSkills, 10000, .001)

def trainRegressor():
    states_and_skills = pd.read_hdf("states_and_skills.hdf")
    states = list(states_and_skills["states"])
    skills = list(states_and_skills["skills"])

    for idx, s in enumerate(states):
        states[idx] = list(s)

    for idx, s in enumerate(skills):
        skills[idx] = list(s)

    x_train = np.array(states)
    y_train = np.array(skills)

    # print(x_train.shape)
    # print(y_train.shape)

    regression_model = linear_model.LinearRegression()
    regression_model.fit(x_train, y_train)
    return regression_model

#regression_model = trainRegressor()


"""
GET NEW GAME BOARD
FEED INITIAL STATE TO MAPPER TO GENERATE A SKILL
GIVE SKILL TO DECODER TO GET APPROXIMATE WEIGHTS
LOAD APPROXIMATE WEIGHTS INTO MODEL
"""
def testMapper(mapper, AE, test_state):
    global gw
    global m
    global q
    print("######################################################################")
    print("TESTING MAPPER!!!!")
    print("######################################################################")
    new_game_board = test_state
    print(gw.dispGrid(new_game_board))
    testStatus = 1
    moveCount = 0
    #for each new state after a move is made, use the mapper to generate the required skill
    #use the skill to generate the weights
    #load the weights, and use them to navigate the grid
    while testStatus == 1:
        new_game_board_train = gw.stateToData(new_game_board)
        #guessed_skill given a particular state
        guessed_encoded_skill = mapper(Variable(torch.from_numpy(new_game_board_train.reshape(1,32)).float()))
        """
        GIVEN A NEW STATE, CAN WE FEED THAT TO THE TRAINED MAPPER TO GENERATE A NEW ENCODED SKILL,
        PASS THAT SKILL TO THE TRAINED DECODER,
        AND USE THOSE WEIGHTS TO ACCOMPLISH THE NEW TASK/STATE
        """
        #pass this skill to the AE to get the needed weights
        guessed_weights_flat = AE.decoder(guessed_encoded_skill)
        #guessed_weights_flat = AE.decoder(Variable(torch.from_numpy(encoded_skill_1.reshape(1,5))).float())
        guessed_weights_flat = guessed_weights_flat.data.numpy().reshape(guessed_weights_flat.shape[1])
        guessed_weights_unflat = m.unFlattenNetwork(guessed_weights_flat, shapes_1)
        net = m.Linear_Net()
        #load those weights into the network
        net = m.loadWeights(guessed_weights_unflat, net)
        #makeMove and get state.
        new_game_board, reward = q.testNetwork_v3(net, inState=new_game_board_train)
        moveCount += 1
        if moveCount > 10:
            testStatus = 0
            print("GAME LOST, too many moves")
        if reward == 20:
            testStatus = 0
            print("GAME WON!")
        if reward == -20:
            testStatus = 0
            print("GAME LOST")

testMapper(mapper, AE, test_state)

def testRegressor(regr_model, AE, test_state):
    global gw
    global m
    global q
    print("######################################################################")
    print("TESTING REGRESSOR!!!!")
    print("######################################################################")
    new_game_board = test_state
    print(gw.dispGrid(new_game_board))
    testStatus = 1
    moveCount = 0
    #for each new state after a move is made, use the mapper to generate the required skill
    #use the skill to generate the weights
    #load the weights, and use them to navigate the grid
    while testStatus == 1:
        new_game_board_train = gw.stateToData(new_game_board)
        #guessed_skill given a particular state
        guessed_encoded_skill = regr_model.predict(new_game_board_train.reshape(32,).reshape(1,-1))
        guessed_encoded_skill = np.array(guessed_encoded_skill[0])
        """
        GIVEN A NEW STATE, CAN WE FEED THAT TO THE TRAINED MAPPER TO GENERATE A NEW ENCODED SKILL,
        PASS THAT SKILL TO THE TRAINED DECODER,
        AND USE THOSE WEIGHTS TO ACCOMPLISH THE NEW TASK/STATE
        """
        #pass this skill to the AE to get the needed weights
        guessed_weights_flat = AE.decoder(Variable(torch.from_numpy(guessed_encoded_skill.reshape(1,5)).float()))
        #guessed_weights_flat = AE.decoder(Variable(torch.from_numpy(encoded_skill_1.reshape(1,5))).float())
        guessed_weights_flat = guessed_weights_flat.data.numpy().reshape(guessed_weights_flat.shape[1])
        guessed_weights_unflat = m.unFlattenNetwork(guessed_weights_flat, shapes_1)
        net = m.Linear_Net()
        #load those weights into the network
        net = m.loadWeights(guessed_weights_unflat, net)
        #makeMove and get state.
        new_game_board, reward = q.testNetwork_v3(net, inState=new_game_board_train)
        moveCount += 1
        if moveCount > 10:
            testStatus = 0
            print("GAME LOST, too many moves")
        if reward == 20:
            testStatus = 0
            print("GAME WON!")
        if reward == -20:
            testStatus = 0
            print("GAME LOST")
#testRegressor(regression_model, AE, test_state)
