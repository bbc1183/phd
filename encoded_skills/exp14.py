"""
I AM GOING TO TRY TO EXTRACT FEATURES OF SKILLS WITH A SKILL AE

EXPERIMENT:  TRAIN 2 CLASSES OF MODELS, 2 EACH, ON 2 DIFFERENT CONFIGS
             THAT IS: 2 MODELS TRAINED ON CONFIG1 AND 2 MODELS TRAINED ON CONFIG2
             FLATTEN THOSE MODELS AND TRAIN SKILL AE TO EXTRACT SKILL_FEATURES
             COMPARE SKILL_FEATURES USING COSINE-SIMILARITY OR OTHER MEASURE

HYPOTHESIS:  I WOULD EXPECT THE SIMILARITY BETWEEN SKILLS OF THE SAME CLASS TO BE
                CLOSER AS COMPARED TO SKILLS FROM THE OTHER CLASS

FINDINGS:
"""
import models as m
import gridworld as gw
import qnetwork as q

import torch
import torchvision
import torchvision.transforms as transforms
import torch.optim as optim
import torch.nn as nn
import random
import torch.nn.functional as F
from torch.autograd import Variable, Function
import numpy as np
import matplotlib.pyplot as plt
import sys
state1 = gw.initGrid()
state2 = gw.initGrid_v2()

"""
TRAIN 4 MODELS TO VICTORY
"""
#train the models until they win
winStatus1 = 0
winStatus2 = 0
winStatus3 = 0
winStatus4 = 0

while winStatus1 == 0:
    model1_class1, paths1 = q.trainNetwork(in_state=state1)
    model1_class1, paths1, finalReward1 = q.testNetwork_v2(model1_class1,inState=gw.stateToData(state1))
    if finalReward1 == 20:
        winStatus1 = 1
while winStatus2 == 0:
    model2_class1, paths2 = q.trainNetwork(in_state=state1)
    model2_class1, paths2, finalReward2 = q.testNetwork_v2(model2_class1,inState=gw.stateToData(state1))
    if finalReward2 == 20:
        winStatus2 = 1
while winStatus3 == 0:
    model3_class2, paths3 = q.trainNetwork(in_state=state2)
    model3_class2,paths3, finalReward3 = q.testNetwork_v2(model3_class2,inState=gw.stateToData(state2))
    if finalReward3 == 20:
        winStatus3 = 1
while winStatus4 == 0:
    model4_class2, paths4 = q.trainNetwork(in_state=state2)
    model4_class2,paths4, finalReward4 = q.testNetwork_v2(model4_class2,inState=gw.stateToData(state2))
    if finalReward4 == 20:
        winStatus4 = 1

"""
EXTRACT THE WEIGHTS OF THE MODELS, FLATTEN THEM
"""
#FLATTEN THE MODELS
trainedModel_1_flat, shapes_1 = m.flattenNetwork(model1_class1)
trainedModel_2_flat, shapes_2 = m.flattenNetwork(model2_class1)
trainedModel_3_flat, shapes_3 = m.flattenNetwork(model3_class2)
trainedModel_4_flat, shapes_4 = m.flattenNetwork(model4_class2)

trained_models_flat = [trainedModel_1_flat, trainedModel_2_flat, trainedModel_3_flat, trainedModel_4_flat]
"""
-------------------------------------------------------------------------------------------
AE TRAINING AND TESTING FUNCTIONS
-------------------------------------------------------------------------------------------
"""
losses = []
def trainAE(weights,auto_encoder, epochs=1, factor=20):
    epochs = epochs
    # Define loss criterion
    criterion = nn.MSELoss()
    learning_rate = .7
    global losses
    for i in range(epochs):
        #learning_rate *= (1.0/(1.0+0.0002*i))
        optimizer = optim.SGD(AE.parameters(), lr=learning_rate, momentum=0.9)
        ##### pytorch syntax ##################################################
        # zero the parameter gradients
        optimizer.zero_grad()
        # forward + backward + optimize
        outputs, encoded_skill, es2, es3 = AE(Variable(torch.from_numpy(weights.reshape(1,len(weights))).float()))
        #print(encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1]))
        encoded_skill =  encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1])
        labels = Variable(torch.from_numpy(weights))
        loss = criterion(outputs, labels)
        #print(loss.data.numpy()[0])
        losses.append(loss.data.numpy()[0])
        loss.backward()
        optimizer.step()
        ##### pytorch syntax ##################################################
    approximated_weights, encoded_skill,es2, es3 = AE(Variable(torch.from_numpy(weights.reshape(1,len(weights))).float()))
    encoded_skill = encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1])
    es2 = es2.data.numpy().reshape(20)
    es3 = es3.data.numpy().reshape(100)
    # fig = plt.figure()
    # plt.plot(losses)
    # plt.show()
    return approximated_weights, encoded_skill,es2, es3, AE

def test_encoded_skill(skill, grid):
    global AE
    guessed_encoded_skill = Variable(torch.from_numpy(skill.reshape(1,4)).float())
    #pass this skill to the AE to get the needed weights
    guessed_weights_flat = AE.decoder(guessed_encoded_skill)
    #guessed_weights_flat = AE.decoder(Variable(torch.from_numpy(encoded_skill_1.reshape(1,5))).float())
    guessed_weights_flat = guessed_weights_flat.data.numpy().reshape(2224)
    guessed_weights_unflat = m.unFlattenNetwork(guessed_weights_flat, shapes_1)
    net = m.Linear_Net()
    #load those weights into the network
    net = m.loadWeights(guessed_weights_unflat, net)
    #makeMove and get state.
    net,path,reward = q.testNetwork_v2(net, inState=grid)

    if len(path) > 10:
        testStatus = 0
        print("GAME LOST, too many moves")
    if reward == 20:
        testStatus = 0
        print("GAME WON!")
    if reward == -20:
        testStatus = 0
        print("GAME LOST")
    return reward
"""
-------------------------------------------------------------------------------------------
END AE TRAINING AND TESTING FUNCTIONS
-------------------------------------------------------------------------------------------
"""

"""
TRAIN A SKILL AE TO VICTORY USING THE FLATNETS
WHEN NET CONVERGES, MAKE FINAL PASS AND RECORD THE OUTPUTS OF HIDDEN LAYERS,
    THESE WILL BE THE SKILL FEATURES
"""
final_state_skill_1 = ()
final_state_skill_2 = ()
final_state_skill_3 = ()
final_state_skill_4 = ()

AE_win_status = 0
AE = m.AutoEncoder_v5()
while AE_win_status == 0:

    losses = []
    for i in range(30000):
        print("training autoencoder: "+str(i))
        index = random.randint(0,3)
        net_to_encode = trained_models_flat[index]
        approx_weights_flattened, encoded_skill,es2, es3, AE = trainAE(net_to_encode,AE)
        if index == 0:
            final_state_skill_1 = (state1, encoded_skill, es2, es3)
        if index == 1:
            final_state_skill_2 = (state1, encoded_skill, es2, es3)
        if index == 2:
            final_state_skill_3 = (state2, encoded_skill, es2, es3)
        if index == 3:
            final_state_skill_4 = (state2, encoded_skill, es2, es3)

    fig = plt.figure()
    plt.plot(losses)
    plt.show()
    print("first encoded_skill: "+str(final_state_skill_1[1]))
    print("second_encoded_skill: "+str(final_state_skill_2[1]))
    print("third_encoded_skill: "+str(final_state_skill_3[1]))
    print("fourth_encoded_skill: "+str(final_state_skill_4[1]))
    status_1 = test_encoded_skill(final_state_skill_1[1], gw.stateToData(state1))
    status_2 = test_encoded_skill(final_state_skill_2[1], gw.stateToData(state1))
    status_3 = test_encoded_skill(final_state_skill_3[1], gw.stateToData(state2))
    status_4 = test_encoded_skill(final_state_skill_4[1], gw.stateToData(state2))
    if status_1 == 20 and status_2 == 20 and status_3 == 20 and status_4 == 20:
        print("VICTORY FOR ALL ENCODED SKILLS")
        AE_win_status = 1

final_states_and_skills = [final_state_skill_1,final_state_skill_2, final_state_skill_3, final_state_skill_4]


"""
DO A COMPARISON OF SKILL FEATURES BETWEEN THE DIFFERENT CLASSES USING COSINE-SIMILARITY
"""
