"""
THE AIM HERE IS TO CHECK THE ACCURACY OF AN ENCODED SKILL FOR MNIST DATASET
"""

from __future__ import print_function
import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.autograd import Variable
import matplotlib.pyplot as plt
import models as m
import random
import numpy as np


# Training settings
parser = argparse.ArgumentParser(description='PyTorch MNIST Example')
parser.add_argument('--batch-size', type=int, default=64, metavar='N',
                    help='input batch size for training (default: 64)')
parser.add_argument('--test-batch-size', type=int, default=1000, metavar='N',
                    help='input batch size for testing (default: 1000)')
parser.add_argument('--epochs', type=int, default=10, metavar='N',
                    help='number of epochs to train (default: 10)')
parser.add_argument('--lr', type=float, default=0.01, metavar='LR',
                    help='learning rate (default: 0.01)')
parser.add_argument('--momentum', type=float, default=0.5, metavar='M',
                    help='SGD momentum (default: 0.5)')
parser.add_argument('--no-cuda', action='store_true', default=False,
                    help='disables CUDA training')
parser.add_argument('--seed', type=int, default=1, metavar='S',
                    help='random seed (default: 1)')
parser.add_argument('--log-interval', type=int, default=10, metavar='N',
                    help='how many batches to wait before logging training status')
args = parser.parse_args()
args.cuda = not args.no_cuda and torch.cuda.is_available()

torch.manual_seed(args.seed)
if args.cuda:
    torch.cuda.manual_seed(args.seed)


kwargs = {'num_workers': 1, 'pin_memory': True} if args.cuda else {}
train_loader = torch.utils.data.DataLoader(
    datasets.MNIST('../data', train=True, download=True,
                   transform=transforms.Compose([
                       transforms.ToTensor(),
                       transforms.Normalize((0.1307,), (0.3081,))
                   ])),
    batch_size=args.batch_size, shuffle=True, **kwargs)
test_loader = torch.utils.data.DataLoader(
    datasets.MNIST('../data', train=False, transform=transforms.Compose([
                       transforms.ToTensor(),
                       transforms.Normalize((0.1307,), (0.3081,))
                   ])),
    batch_size=args.test_batch_size, shuffle=True, **kwargs)


class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(1, 10, kernel_size=5)
        self.conv2 = nn.Conv2d(10, 20, kernel_size=5)
        self.conv2_drop = nn.Dropout2d()
        self.fc1 = nn.Linear(320, 50)
        self.fc2 = nn.Linear(50, 10)

    def forward(self, x):
        x = F.relu(F.max_pool2d(self.conv1(x), 2))
        x = F.relu(F.max_pool2d(self.conv2_drop(self.conv2(x)), 2))
        x = x.view(-1, 320)
        x = F.relu(self.fc1(x))
        x = F.dropout(x, training=self.training)
        x = self.fc2(x)
        return F.log_softmax(x)

model = Net()
if args.cuda:
    model.cuda()

optimizer = optim.SGD(model.parameters(), lr=.01, momentum=.9)
losses = []
def train(epoch):
    model.train()
    for batch_idx, (data, target) in enumerate(train_loader):
        if args.cuda:
            data, target = data.cuda(), target.cuda()
        data, target = Variable(data), Variable(target)
        optimizer.zero_grad()
        output = model(data)
        loss = F.nll_loss(output, target)
        loss.backward()
        optimizer.step()
        if batch_idx % args.log_interval == 0:
            #losses.append(loss.data[0])
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), loss.data[0]))

def test(testModel):
    testModel.eval()
    test_loss = 0
    correct = 0
    for data, target in test_loader:
        if args.cuda:
            data, target = data.cuda(), target.cuda()
        data, target = Variable(data, volatile=True), Variable(target)
        output = testModel(data)
        test_loss += F.nll_loss(output, target, size_average=False).data[0] # sum up batch loss
        pred = output.data.max(1, keepdim=True)[1] # get the index of the max log-probability
        correct += pred.eq(target.data.view_as(pred)).cpu().sum()

    test_loss /= len(test_loader.dataset)
    print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
        test_loss, correct, len(test_loader.dataset),
        100. * correct / len(test_loader.dataset)))


for epoch in range(1, 4):
    train(epoch)
test(model)

flatModel, shapes = m.flattenNetwork(model)
AE = m.AutoEncoder_v6()
"""
-------------------------------------------------------------------------------------------
AE TRAINING AND TESTING FUNCTIONS
-------------------------------------------------------------------------------------------
"""
losses = []
def trainAE(weights,auto_encoder, epochs=1, factor=20):
    epochs = epochs
    # Define loss criterion
    criterion = nn.MSELoss()
    learning_rate = .9
    global losses
    for i in range(epochs):
        #learning_rate *= (1.0/(1.0+0.0002*i))
        optimizer = optim.SGD(AE.parameters(), lr=learning_rate, momentum=0.9)
        ##### pytorch syntax ##################################################
        # zero the parameter gradients
        optimizer.zero_grad()
        # forward + backward + optimize
        outputs, encoded_skill = AE(Variable(torch.from_numpy(weights.reshape(1,len(weights))).float()))
        #print(encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1]))
        encoded_skill =  encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1])
        labels = Variable(torch.from_numpy(weights))
        loss = criterion(outputs, labels)
        #print(loss.data.numpy()[0])
        losses.append(loss.data.numpy()[0])
        loss.backward()
        optimizer.step()
        ##### pytorch syntax ##################################################
    approximated_weights, encoded_skill = AE(Variable(torch.from_numpy(weights.reshape(1,len(weights))).float()))
    encoded_skill = encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1])
    #es2 = es2.data.numpy().reshape(500)
    #es3 = es3.data.numpy().reshape(100)
    # fig = plt.figure()
    # plt.plot(losses)
    # plt.show()
    return approximated_weights, encoded_skill, AE

def test_encoded_skill(skill, grid, AE):
    AE
    guessed_encoded_skill = Variable(torch.from_numpy(skill.reshape(1,4)).float())
    #pass this skill to the AE to get the needed weights
    guessed_weights_flat = AE.decoder(guessed_encoded_skill)
    #guessed_weights_flat = AE.decoder(Variable(torch.from_numpy(encoded_skill_1.reshape(1,5))).float())
    guessed_weights_flat = guessed_weights_flat.data.numpy().reshape(2224)
    guessed_weights_unflat = m.unFlattenNetwork(guessed_weights_flat, shapes_1)
    net = m.Linear_Net()
    #load those weights into the network
    net = m.loadWeights(guessed_weights_unflat, net)
    #makeMove and get state.
    net,path,reward = q.testNetwork_v2(net, inState=grid)

    if len(path) > 10:
        testStatus = 0
        print("GAME LOST, too many moves")
    if reward == 20:
        testStatus = 0
        print("GAME WON!")
    if reward == -20:
        testStatus = 0
        print("GAME LOST")
    return reward
"""
-------------------------------------------------------------------------------------------
END AE TRAINING AND TESTING FUNCTIONS
-------------------------------------------------------------------------------------------
"""

while 1 ==1 :
    for i in range(500):
        print("training autoencoder: "+str(i))
        index = random.randint(0,0)
        net_to_encode = flatModel
        approx_weights_flattened, encoded_skill, AE = trainAE(net_to_encode,AE)
    #print(shapes)

    approx_weights_flattened = approx_weights_flattened.data.numpy().reshape(21840)
    approxModel = m.unFlattenNetwork(approx_weights_flattened, shapes)
    newModel = Net()
    #load those weights into the network
    newModel = m.loadWeights_mnist(approxModel, newModel)
    test(newModel)

    #print(approxModel.shape)
    plt.plot(losses)
    plt.show()
