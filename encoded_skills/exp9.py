"""
need to successfully model states and skills
"""

import pandas as pd
import numpy as np
from sklearn import linear_model

states_and_skills = pd.read_hdf("states_and_skills.hdf")
states = list(states_and_skills["states"])
skills = list(states_and_skills["skills"])

for idx, s in enumerate(states):
    states[idx] = list(s)

for idx, s in enumerate(skills):
    skills[idx] = list(s)

x_train = np.array(states)
y_train = np.array(skills)

# print(x_train.shape)
# print(y_train.shape)

regression_model = linear_model.LinearRegression()
regression_model.fit(x_train, y_train)

# print(regression_model.score(x_train, y_train))
y_predict = regression_model.predict(x_train[0].reshape(1,-1))
# print(y_predict[0])
