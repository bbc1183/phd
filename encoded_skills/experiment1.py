"""
THE AIM HERE IS TO PULL TRAINED WEIGHTS AND COMPRESS THEM INTO AN ENCODED SKILL

FINDING: WE CAN SUCCESSFULLY COMPRESS A SKILL, GIVEN THE SAME INITIAL STATE, WE CAN
         USE THE COMPRESSED SKILL TO APPROXIMATE THE WEIGHTS WE NEED TO ACCOMPLISH
         THAT STATE/GRID.  THIS CAN BE DONE WITH HIGH ACCURACY.

NOTE: WHEN COMPRESSING A SINGLE SKILL, IT IS IMPORTANT TO KEEP THE LEARNING RATE HIGH, LIKE .99,
      AND THE NUMBER OF EPOCHS RELATIVELY HIGH.

"""


import gridworld as gw
import qnetwork as q
import models as m

import torch
import torchvision
import torchvision.transforms as transforms
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable, Function
import numpy as np
import matplotlib.pyplot as plt
import sys

def run():
    net, paths = q.trainNetwork()
    print("#################################")
    print("Testing Original Network")
    print("#################################")
    q.testNetwork(net)
    flatNet, shapes = m.flattenNetwork(net)
    encoded_skill = []
    def trainAE(weights, epochs=1300, factor=20):
        epochs = epochs
        AE = m.AutoEncoder(len(weights), factor)
        # Define loss criterion
        criterion = nn.MSELoss()
        learning_rate = .99
        losses = []
        for i in range(epochs):
            #learning_rate *= (1.0/(1.0+0.0002*i))
            optimizer = optim.SGD(AE.parameters(), lr=learning_rate, momentum=0.9)
            ##### pytorch syntax ##################################################
            # zero the parameter gradients
            optimizer.zero_grad()
            # forward + backward + optimize
            outputs, encoded_skill = AE(Variable(torch.from_numpy(weights.reshape(1,len(weights))).float()))
            #print(encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1]))
            encoded_skill =  encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1])
            labels = Variable(torch.from_numpy(weights))
            loss = criterion(outputs, labels)
            #print(loss.data.numpy()[0])
            losses.append(loss.data.numpy()[0])
            loss.backward()
            optimizer.step()
            ##### pytorch syntax ##################################################
        approximated_weights, encoded_skill = AE(Variable(torch.from_numpy(weights.reshape(1,len(weights))).float()))
        encoded_skill = encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1])
        fig = plt.figure()
        plt.plot(losses)
        plt.show()
        return approximated_weights, encoded_skill, AE, shapes


    print("#################################################################")
    print("#################################################################")

    approx_weights_flattened, encoded_skill, AE, shapes = trainAE(flatNet)
    approx_weights_flattened = approx_weights_flattened.data.numpy()

    approx_weights_flattened = approx_weights_flattened.reshape(approx_weights_flattened.shape[1],)
    approx_weights_unflattened = m.unFlattenNetwork(approx_weights_flattened, shapes)
    approx_model = m.loadWeights(approx_weights_unflattened, net)
    print("#################################")
    print("Testing Approximate Network")
    print("#################################")
    q.testNetwork(approx_model)
    return encoded_skill, AE, shapes
