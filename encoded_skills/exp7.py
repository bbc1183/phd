"""
THIS IS AN EXTENSION OF EXPERIMENT 3.

INSTEAD OF ASKING THE MAPPER TO GENERATE THE NEEDED SKILL FOR THE COMPLETE TASK GIVEN ONLY
THE INITIAL STATE, WE WILL USE THE MAPPER TO GENERATE THE SKILL NEEDED FOR EACH STATE IN
THE SEQUENCE OF MOVES NEEDED TO GET TO THE GOAL.

FINDINGS: THIS DID NOT WORK, THE MAPPER MUST REQUIRE MORE DATA.
"""


import models as m
import gridworld as gw
import qnetwork as q

import torch
import torchvision
import torchvision.transforms as transforms
import torch.optim as optim
import torch.nn as nn
import random
import torch.nn.functional as F
from torch.autograd import Variable, Function
import numpy as np
import matplotlib.pyplot as plt
import sys

"""
INITIALIZE 5 DIFFERENT GAME BOARDS, SAVE THE INITIAL STATES
TRAIN AGENTS ON THESE BOARDS
"""
states = gw.get6GameBoards()
training_states = states[0:5]
test_state = states[5]


trainedModel_1, paths1 = q.trainNetwork(in_state=training_states[0])
trainedModel_2, paths2 = q.trainNetwork(in_state=training_states[1])
trainedModel_3, paths3 = q.trainNetwork(in_state=training_states[2])
trainedModel_4, paths4 = q.trainNetwork(in_state=training_states[3])
trainedModel_5, paths5 = q.trainNetwork(in_state=training_states[4])


"""
COMPRESS THE WEIGHTS TO ENCODED_SKILLS
SAVE THE DECODER (SHUFFLE OVER ALL SKILLS TO GENERALIZE)
SAVE ENCODED_SKILLS IN AN ARRAY
"""

#FLATTEN THE MODELS
trainedModel_1_flat, shapes_1 = m.flattenNetwork(trainedModel_1)
trainedModel_2_flat, shapes_2 = m.flattenNetwork(trainedModel_2)
trainedModel_3_flat, shapes_3 = m.flattenNetwork(trainedModel_3)
trainedModel_4_flat, shapes_4 = m.flattenNetwork(trainedModel_4)
trainedModel_5_flat, shapes_5 = m.flattenNetwork(trainedModel_5)
trained_models_flat = [trainedModel_1_flat, trainedModel_2_flat, trainedModel_3_flat, trainedModel_4_flat, trainedModel_5_flat]
losses = []
def trainAE(weights,auto_encoder, epochs=1, factor=20):
    epochs = epochs
    # Define loss criterion
    criterion = nn.MSELoss()
    learning_rate = .7
    global losses
    for i in range(epochs):
        #learning_rate *= (1.0/(1.0+0.0002*i))
        optimizer = optim.SGD(AE.parameters(), lr=learning_rate, momentum=0.9)
        ##### pytorch syntax ##################################################
        # zero the parameter gradients
        optimizer.zero_grad()
        # forward + backward + optimize
        outputs, encoded_skill = AE(Variable(torch.from_numpy(weights.reshape(1,len(weights))).float()))
        #print(encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1]))
        encoded_skill =  encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1])
        labels = Variable(torch.from_numpy(weights))
        loss = criterion(outputs, labels)
        #print(loss.data.numpy()[0])
        losses.append(loss.data.numpy()[0])
        loss.backward()
        optimizer.step()
        ##### pytorch syntax ##################################################
    approximated_weights, encoded_skill = AE(Variable(torch.from_numpy(weights.reshape(1,len(weights))).float()))
    encoded_skill = encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1])
    # fig = plt.figure()
    # plt.plot(losses)
    # plt.show()
    return approximated_weights, encoded_skill, AE

AE = m.AutoEncoder(inSize=2224, factor=20)
final_state_skill_1 = ()
final_state_skill_2 = ()
final_state_skill_3 = ()
final_state_skill_4 = ()
final_state_skill_5 = ()
for i in range(30000):
    print("training autoencoder: "+str(i))
    index = random.randint(0,len(trained_models_flat)-1)
    net_to_encode = trained_models_flat[index]
    approx_weights_flattened, encoded_skill, AE = trainAE(net_to_encode,AE)
    if index == 0:
        final_state_skill_1 = (training_states[0], encoded_skill)
    if index == 1:
        final_state_skill_2 = (training_states[1], encoded_skill)
    if index == 2:
        final_state_skill_3 = (training_states[2], encoded_skill)
    if index == 3:
        final_state_skill_4 = (training_states[3], encoded_skill)
    if index == 4:
        final_state_skill_5 = (training_states[4], encoded_skill)


final_states_and_skills = [final_state_skill_1,final_state_skill_2, final_state_skill_3, final_state_skill_4, final_state_skill_5]
# print("########################################################")
# print("Testing Trained Models")
# print("########################################################")
# q.testNetwork(trainedModel_1)
# q.testNetwork(trainedModel_2)
# q.testNetwork(trainedModel_3)
# q.testNetwork(trainedModel_4)
# q.testNetwork(trainedModel_5)

print("######################################################")
print("VIEWING LOSSES FOR AE TRAINING")
print("######################################################")
fig = plt.figure()
plt.plot(losses)
plt.show()

"""
TRAIN THE MAPPER ON THE INITIAL STATES AND ENCODED SKILLS
VIEW THE LOSS
"""
mapper = m.State_To_Skill_Mapper()
# Define loss criterion
criterion = nn.MSELoss()
learning_rate = .05
losses = []
sample = []
mapper_losses = []
for i in range(30000):
    print("Mapper Training: "+str(i))
    idx = random.randint(0, 4)

    sample = final_states_and_skills[idx]

    state = sample[0]
    skill = sample[1]

    train_state = gw.stateToData(state)
    #learning_rate *= (1.0/(1.0+0.0002*i))
    optimizer = optim.SGD(mapper.parameters(), lr=learning_rate, momentum=0.9)
    ##### pytorch syntax ##################################################
    # zero the parameter gradients
    optimizer.zero_grad()
    # forward + backward + optimize

    outputs = mapper(Variable(torch.from_numpy(train_state.reshape(1,32)).float()))

    labels = Variable(torch.from_numpy(skill))
    loss = criterion(outputs, labels)
    #print(loss.data.numpy()[0])
    mapper_losses.append(loss.data.numpy()[0])
    loss.backward()
    optimizer.step()
    ##### pytorch syntax ##################################################
print("######################################################")
print("VIEWING LOSSES FOR MAPPER")
print("######################################################")
fig = plt.figure()
plt.plot(mapper_losses)
plt.show()

"""
GET NEW GAME BOARD
FEED INITIAL STATE TO MAPPER TO GENERATE A SKILL
GIVE SKILL TO DECODER TO GET APPROXIMATE WEIGHTS
LOAD APPROXIMATE WEIGHTS INTO MODEL
"""

print("######################################################################")
print("TESTING MAPPER!!!!")
print("######################################################################")
new_game_board = test_state
print(gw.dispGrid(new_game_board))
testStatus = 1
moveCount = 0
#for each new state after a move is made, use the mapper to generate the required skill
#use the skill to generate the weights
#load the weights, and use them to navigate the grid
while testStatus == 1:
    new_game_board_train = gw.stateToData(new_game_board)
    #guessed_skill given a particular state
    guessed_encoded_skill = mapper(Variable(torch.from_numpy(new_game_board_train.reshape(1,32)).float()))
    """
    GIVEN A NEW STATE, CAN WE FEED THAT TO THE TRAINED MAPPER TO GENERATE A NEW ENCODED SKILL,
    PASS THAT SKILL TO THE TRAINED DECODER,
    AND USE THOSE WEIGHTS TO ACCOMPLISH THE NEW TASK/STATE
    """
    #pass this skill to the AE to get the needed weights
    guessed_weights_flat = AE.decoder(guessed_encoded_skill)
    #guessed_weights_flat = AE.decoder(Variable(torch.from_numpy(encoded_skill_1.reshape(1,5))).float())
    guessed_weights_flat = guessed_weights_flat.data.numpy().reshape(guessed_weights_flat.shape[1])
    guessed_weights_unflat = m.unFlattenNetwork(guessed_weights_flat, shapes_1)
    net = m.Linear_Net()
    #load those weights into the network
    net = m.loadWeights(guessed_weights_unflat, net)
    #makeMove and get state.
    new_game_board, reward = q.testNetwork_v3(net, inState=new_game_board_train)
    moveCount += 1
    if moveCount > 10:
        testStatus = 0
        print("GAME LOST, too many moves")
    if reward == 20:
        testStatus = 0
        print("GAME WON!")
    if reward == -20:
        testStatus = 0
        print("GAME LOST")
