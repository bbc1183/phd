
"""
SAME AS EXPERIMENT 1 BUT WITH FROZEN ENCODER, TRAINABLE DECODER

THIS SHOULD PRODUCE NATURAL ENCODED_SKILLS, INSTEAD OF SYNTHETIC ONES

FINDINGS: I CANNOT YET GET THE DECODER TO CONVERGE
"""
import models as m
import gridworld as gw
import qnetwork as q

import torch
import torchvision
import torchvision.transforms as transforms
import torch.optim as optim
import torch.nn as nn
import random
import torch.nn.functional as F
from torch.autograd import Variable, Function
import numpy as np
import matplotlib.pyplot as plt
import sys
import pandas as pd
from sklearn import linear_model


import models as m
import gridworld as gw
import qnetwork as q

import torch
import torchvision
import torchvision.transforms as transforms
import torch.optim as optim
import torch.nn as nn
import random
import torch.nn.functional as F
from torch.autograd import Variable, Function
import numpy as np
import matplotlib.pyplot as plt
import sys
import pandas as pd
from sklearn import linear_model

"""
INITIALIZE 5 DIFFERENT GAME BOARDS, SAVE THE INITIAL STATES
TRAIN AGENTS ON THESE BOARDS
"""
states = gw.get6GameBoards()
training_states = states[0:5]
test_state = states[5]

#train the models until they win
winStatus1 = 0
winStatus2 = 0
winStatus3 = 0
winStatus4 = 0
winStatus5 = 0
while winStatus1 == 0:
    trainedModel_1, paths1 = q.trainNetwork(in_state=training_states[0])
    trainedModel_1, paths1, finalReward1 = q.testNetwork_v2(trainedModel_1,inState=gw.stateToData(training_states[0]))
    if finalReward1 == 20:
        winStatus1 = 1
while winStatus2 == 0:
    trainedModel_2, paths2 = q.trainNetwork(in_state=training_states[1])
    trainedModel_2, paths2, finalReward2 = q.testNetwork_v2(trainedModel_2,inState=gw.stateToData(training_states[1]))
    if finalReward2 == 20:
        winStatus2 = 1
while winStatus3 == 0:
    trainedModel_3, paths3 = q.trainNetwork(in_state=training_states[2])
    trainedModel_3, paths3, finalReward3 = q.testNetwork_v2(trainedModel_3,inState=gw.stateToData(training_states[2]))
    if finalReward3 == 20:
        winStatus3 = 1
while winStatus4 == 0:
    trainedModel_4, paths4 = q.trainNetwork(in_state=training_states[3])
    trainedModel_4, paths4, finalReward4 = q.testNetwork_v2(trainedModel_4,inState=gw.stateToData(training_states[3]))
    if finalReward4 == 20:
        winStatus4 = 1
while winStatus5 == 0:
    trainedModel_5, paths5 = q.trainNetwork(in_state=training_states[4])
    trainedModel_5, paths5, finalReward5 = q.testNetwork_v2(trainedModel_5,inState=gw.stateToData(training_states[4]))
    if finalReward5 == 20:
        winStatus5 = 1


"""
COMPRESS THE WEIGHTS TO ENCODED_SKILLS
SAVE THE DECODER (SHUFFLE OVER ALL SKILLS TO GENERALIZE)
SAVE ENCODED_SKILLS IN AN ARRAY
"""

#FLATTEN THE MODELS
trainedModel_1_flat, shapes_1 = m.flattenNetwork(trainedModel_1)
trainedModel_2_flat, shapes_2 = m.flattenNetwork(trainedModel_2)
trainedModel_3_flat, shapes_3 = m.flattenNetwork(trainedModel_3)
trainedModel_4_flat, shapes_4 = m.flattenNetwork(trainedModel_4)
trainedModel_5_flat, shapes_5 = m.flattenNetwork(trainedModel_5)
trained_models_flat = [trainedModel_1_flat, trainedModel_2_flat, trainedModel_3_flat, trainedModel_4_flat, trainedModel_5_flat]
# file = open("flat_winning_models.txt", "w")
# for m in trained_models_flat:
#     file.write(str(m))
#     file.write("\n\n\n\n\n\n")
#     file.write("#####################################################")
#     file.write("\n\n\n\n\n\n")
# file.close()
def trainAE(weights,auto_encoder, learning_rate, epochs=1, factor=20):
    epochs = epochs
    # Define loss criterion
    criterion = nn.MSELoss()
    #learning_rate = .99
    global losses
    for i in range(epochs):
        #learning_rate *= (1.0/(1.0+0.0002*i))
        optimizer = optim.SGD(AE.decoder.parameters(), lr=learning_rate, momentum=0.9)
        #optimizer = optim.Adam(AE.decoder.parameters(),lr=1e-3)# lr=learning_rate, weight_decay=1e-5)
        ##### pytorch syntax ##################################################
        # zero the parameter gradients
        optimizer.zero_grad()
        # forward + backward + optimize
        outputs, encoded_skill = AE(Variable(torch.from_numpy(weights.reshape(1,len(weights))).float()))
        #print(encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1]))
        encoded_skill =  encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1])
        labels = Variable(torch.from_numpy(weights))
        loss = criterion(outputs, labels)
        #print(loss.data.numpy()[0])
        losses.append(loss.data.numpy()[0])
        loss.backward()
        optimizer.step()
        ##### pytorch syntax ##################################################
    approximated_weights, encoded_skill = AE(Variable(torch.from_numpy(weights.reshape(1,len(weights))).float()))
    encoded_skill = encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1])
    # fig = plt.figure()
    # plt.plot(losses)
    # plt.show()
    return approximated_weights, encoded_skill, AE


def test_encoded_skill(skill, grid):
    global AE
    guessed_encoded_skill = Variable(torch.from_numpy(skill.reshape(1,2)).float())
    #pass this skill to the AE to get the needed weights
    guessed_weights_flat = AE.decoder(guessed_encoded_skill)
    #guessed_weights_flat = AE.decoder(Variable(torch.from_numpy(encoded_skill_1.reshape(1,5))).float())
    guessed_weights_flat = guessed_weights_flat.data.numpy().reshape(guessed_weights_flat.shape[1])
    guessed_weights_unflat = m.unFlattenNetwork(guessed_weights_flat, shapes_1)
    net = m.Linear_Net()
    #load those weights into the network
    net = m.loadWeights(guessed_weights_unflat, net)
    #makeMove and get state.
    net,path,reward = q.testNetwork_v2(net, inState=grid)

    if len(path) > 10:
        testStatus = 0
        print("GAME LOST, too many moves")
    if reward == 20:
        testStatus = 0
        print("GAME WON!")
    if reward == -20:
        testStatus = 0
        print("GAME LOST")
    return reward


AE = m.AutoEncoder_v2(inSize=2224, factor=20)
for param in AE.encoder.parameters():
    param.requires_grad = False
final_state_skill_1 = ()
final_state_skill_2 = ()
final_state_skill_3 = ()
final_state_skill_4 = ()
final_state_skill_5 = ()
AE_win_status = 0
losses = []
while AE_win_status == 0:

    learning_rate = .9

    losses = []
    for i in range(30000):
        learning_rate *= (1.0/(1.0+0.0002*i))
        print("training autoencoder: "+str(i))
        index = random.randint(0,len(trained_models_flat)-4)
        net_to_encode = trained_models_flat[index]
        approx_weights_flattened, encoded_skill, AE = trainAE(net_to_encode,AE, learning_rate)
        if index == 0:
            final_state_skill_1 = (training_states[0], encoded_skill)
        if index == 1:
            final_state_skill_2 = (training_states[1], encoded_skill)
        if index == 2:
            final_state_skill_3 = (training_states[2], encoded_skill)
        if index == 3:
            final_state_skill_4 = (training_states[3], encoded_skill)
        if index == 4:
            final_state_skill_5 = (training_states[4], encoded_skill)
    fig = plt.figure()
    plt.plot(losses)
    plt.show()
    print("first encoded_skill: "+str(final_state_skill_1[1]))
    print("second_encoded_skill: "+str(final_state_skill_2[1]))
    status_1 = test_encoded_skill(final_state_skill_1[1], gw.stateToData(final_state_skill_1[0]))
    status_2 = test_encoded_skill(final_state_skill_2[1], gw.stateToData(final_state_skill_2[0]))
    # status_3 = test_encoded_skill(final_state_skill_3[1], gw.stateToData(final_state_skill_3[0]))
    # status_4 = test_encoded_skill(final_state_skill_4[1], gw.stateToData(final_state_skill_4[0]))
    # status_5 = test_encoded_skill(final_state_skill_5[1], gw.stateToData(final_state_skill_5[0]))
    if status_1 == 20 and status_2 == 20:
        torch.save(AE, "trained_AE.pt")
        # file = open("encoded_skills.txt", "w")
        # file.write(str(final_state_skill_1[1]))
        # file.write(str(final_state_skill_2[1]))
        # file.write(str(final_state_skill_3[1]))
        # file.write(str(final_state_skill_4[1]))
        # file.write(str(final_state_skill_5[1]))
        # file.close()
        AE_win_status = 1

final_states_and_skills = [final_state_skill_1,final_state_skill_2]


print("########################################################")
print("Testing Trained Models")
print("########################################################")

model_1, path1, reward1 = q.testNetwork_v2(trainedModel_1,inState=gw.stateToData(training_states[0]))
model_2, path2, reward2 = q.testNetwork_v2(trainedModel_2,inState=gw.stateToData(training_states[1]))
# model_3, path3, reward3 = q.testNetwork_v2(trainedModel_3,inState=gw.stateToData(training_states[2]))
# model_4, path4, reward4 = q.testNetwork_v2(trainedModel_4,inState=gw.stateToData(training_states[3]))
# model_5, path5, reward5 = q.testNetwork_v2(trainedModel_5,inState=gw.stateToData(training_states[4]))


#WE NEED ALL SUBSEQUENT STATES FOR A PARTICULAR SKILL
allStates_allSkills = []

for idx,state_skill in enumerate(final_states_and_skills):

    state = state_skill[0]
    skill = state_skill[1]
    train_state = gw.stateToData(state)
    curr_state = state
    curr_state_train = train_state
    allStates_in_path = []
    allStates_in_path.append(curr_state_train)


    if idx == 0:
        for move in path1:
            newState = gw.makeMove(curr_state, move)
            newState_train = gw.stateToData(newState)
            curr_state = newState
            curr_state_train = newState_train
            allStates_in_path.append(curr_state_train)
        #allStates now contains all states/grids for the given path/skill
        for state in allStates_in_path:
            allStates_allSkills.append((state, skill))
    if idx == 1:
        for move in path2:
            newState = gw.makeMove(curr_state, move)
            newState_train = gw.stateToData(newState)
            curr_state = newState
            curr_state_train = newState_train
            allStates_in_path.append(curr_state_train)
        #allStates now contains all states/grids for the given path/skill
        for state in allStates_in_path:
            allStates_allSkills.append((state, skill))
    # if idx == 2:
    #     for move in path3:
    #         newState = gw.makeMove(curr_state, move)
    #         newState_train = gw.stateToData(newState)
    #         curr_state = newState
    #         curr_state_train = newState_train
    #         allStates_in_path.append(curr_state_train)
    #     #allStates now contains all states/grids for the given path/skill
    #     for state in allStates_in_path:
    #         allStates_allSkills.append((state, skill))
    # if idx == 3:
    #     for move in path4:
    #         newState = gw.makeMove(curr_state, move)
    #         newState_train = gw.stateToData(newState)
    #         curr_state = newState
    #         curr_state_train = newState_train
    #         allStates_in_path.append(curr_state_train)
    #     #allStates now contains all states/grids for the given path/skill
    #     for state in allStates_in_path:
    #         allStates_allSkills.append((state, skill))
    # if idx == 4:
    #     for move in path5:
    #         newState = gw.makeMove(curr_state, move)
    #         newState_train = gw.stateToData(newState)
    #         curr_state = newState
    #         curr_state_train = newState_train
    #         allStates_in_path.append(curr_state_train)
    #     #allStates now contains all states/grids for the given path/skill
    #     for state in allStates_in_path:
    #         allStates_allSkills.append((state, skill))
"""
allStates_allSkills NOW CONTAINS ALL STATE, SKILL PAIRS WE NEED TO TRAIN THE MAPPER
"""
print(allStates_allSkills)
print("######################################################")
print("VIEWING LOSSES FOR AE TRAINING")
print("######################################################")
fig = plt.figure()
plt.plot(losses)
plt.show()





"""
AE = m.AutoEncoder_v2(inSize=100, factor=20)
for param in AE.encoder.parameters():
    param.requires_grad = False

x_train = np.random.rand(100)
epochs = 500
# Define loss criterion
criterion = nn.MSELoss()
learning_rate = .9
losses = []
for i in range(epochs):
    #learning_rate *= (1.0/(1.0+0.0002*i))
    optimizer = optim.SGD(AE.decoder.parameters(), lr=learning_rate, momentum=0.9)
    ##### pytorch syntax ##################################################
    # zero the parameter gradients
    optimizer.zero_grad()
    # forward + backward + optimize
    outputs, encoded_skill = AE(Variable(torch.from_numpy(x_train.reshape(1,len(x_train))).float()))
    #print(encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1]))
    encoded_skill =  encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1])
    print("ITERATION: "+str(i)+" / SKILL: "+str(encoded_skill))
    labels = Variable(torch.from_numpy(x_train.reshape(1,len(x_train))).float())
    loss = criterion(outputs, labels)
    #print(loss.data.numpy()[0])
    losses.append(loss.data.numpy()[0])
    loss.backward()
    optimizer.step()
plt.plot(losses)
plt.show()


0.48123735 , 0.51466519
0.46138394  ,0.49248677
"""
