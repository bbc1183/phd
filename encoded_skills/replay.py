import numpy as np
import gridworld as gw
import random
import models
import torch.optim as optim
import torch.nn as nn
import sys
from torch.autograd import Variable, Function
import torch





def trainNetwork():
    net = models.Linear_Net()
    # Define loss criterion
    criterion = nn.MSELoss()
    optimizer = optim.SGD(net.parameters(), lr=0.01, momentum=0.9)
    epochs = 1000
    gamma = 0.9 #since it may take several moves to goal, making gamma high
    epsilon = 1
    numWins = 0
    winning_paths = []
    x = 0
    replay_memory_size = 80
    batchSize = 40
    experiences = []

    for i in range(epochs):

        state = gw.initGrid()

        status = 1
        #while game still in progress

        temp_path = []
        #while game still in progress
        while(status == 1):
            train_state = gw.stateToData(state)
            #We are in state S
            #Let's run our Q function on S to get Q values for all possible actions
            ##### keras syntax ####################################################
            #qval = model.predict(state.reshape(1,64), batch_size=1)
            ##### keras syntax ####################################################

            ##### pytorch syntax ##################################################
            qval = net(Variable(torch.from_numpy(train_state.reshape(1,32)).float()))
            qval = qval.data.numpy()

            if (random.random() < epsilon): #choose random action
                action = np.random.randint(0,4)
            else: #choose best action from Q(s,a) values
                action = (np.argmax(qval))
            temp_path.append(action)
            #Take action, observe new state S'
            new_state = gw.makeMove(state, action)
            new_train_state = gw.stateToData(new_state)
            #Observe reward
            reward = gw.getReward(new_state)

            #Experience replay storage
            if (len(replay) < buffer): #if buffer not filled, add to it
                experiences.append((state, action, reward, new_state))
            #Get max_Q(S',a)
            #newQ = model.predict(new_state.reshape(1,64), batch_size=1)
            ##### pytorch syntax ##################################################
            newQ = net(Variable(torch.from_numpy(new_train_state.reshape(1,32)).float()))
            newQ = newQ.data.numpy()
            ##### pytorch syntax ##################################################
            maxQ = np.max(newQ)
            y = np.zeros((1,4))
            y[:] = qval[:]
            if reward == -1: #non-terminal state
                update = (reward + (gamma * maxQ))
            else: #terminal state
                update = reward
            y[0][action] = update #target output
            print("Game #: "+str(i)+" / num_wins: "+str(numWins)+" / Epsilon: "+str(round(epsilon,3)))
            #model.fit(state.reshape(1,64), y, batch_size=1, nb_epoch=1, verbose=1)
            ##### pytorch syntax ##################################################
            # zero the parameter gradients
            optimizer.zero_grad()
            # forward + backward + optimize
            outputs = net(Variable(torch.from_numpy(train_state.reshape(1,32)).float()))
            labels = Variable(torch.from_numpy(y).float())
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()
            ##### pytorch syntax ##################################################
            old_state = state
            state = new_state


            if reward != -1:
                if reward == 20:
                    numWins+=1
                    winning_paths.append(temp_path)
                status = 0

        if epsilon > 0.1:
            epsilon -= (1/epochs)
    return net, winning_paths

def testNetwork(network, init=0):
    testNet = network
    i = 0
    if init==0:
        state = gw.initGrid()
    elif init==1:
        state = gw.initGridPlayer()
    elif init==2:
        state = gw.initGridRand()

    print("Initial State:")
    print(gw.dispGrid(state))
    status = 1
    #while game still in progress
    path = []
    while(status == 1):
        test_state = gw.stateToData(state)
        ##### keras syntax ####################################################
        #qval = model.predict(state.reshape(1,64), batch_size=1)
        ##### keras syntax ####################################################

        ##### pytorch syntax ##################################################
        qval = testNet(Variable(torch.from_numpy(test_state.reshape(1,32)).float()))
        qval = qval.data.numpy()
        print(qval)
        ##### pytorch syntax ##################################################

        action = (np.argmax(qval)) #take action with highest Q-value
        print('Move #: %s; Taking action: %s' % (i, action))
        state = gw.makeMove(state, action)
        print(gw.dispGrid(state))
        reward = gw.getReward(state)
        path.append(action)
        if reward != -1:
            status = 0
            print("Reward: %s" % (reward,))
            print(path)
        i += 1 #If we're taking more than 10 actions, just stop, we probably can't win this game
        if (i > 10):
            print("Game lost; too many moves.")
            print(path)
            break
    return testNet
