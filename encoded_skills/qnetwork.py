import numpy as np
import gridworld as gw
import random
import models
import torch.optim as optim
import torch.nn as nn
import sys
from torch.autograd import Variable, Function
import torch
import random


def trainNetwork(in_state=None, epoch = 1000):
    net = models.Linear_Net()
    # Define loss criterion
    criterion = nn.MSELoss()
    optimizer = optim.SGD(net.parameters(), lr=0.0001, momentum=0.9)
    epochs = epoch
    gamma = 0.9 #since it may take several moves to goal, making gamma high
    epsilon = 1
    numWins = 0
    winning_path = []
    x = 0

    for i in range(epochs):

        state = gw.initGrid()

        if in_state is not None:
            state = in_state

        status = 1
        #while game still in progress

        temp_path = []
        #while game still in progress
        while(status == 1):
            train_state = gw.stateToData(state)
            #We are in state S
            #Let's run our Q function on S to get Q values for all possible actions
            ##### keras syntax ####################################################
            #qval = model.predict(state.reshape(1,64), batch_size=1)
            ##### keras syntax ####################################################

            ##### pytorch syntax ##################################################
            qval = net(Variable(torch.from_numpy(train_state.reshape(1,32)).float()))
            qval = qval.data.numpy()

            if (random.random() < epsilon): #choose random action
                action = np.random.randint(0,4)
            else: #choose best action from Q(s,a) values
                action = (np.argmax(qval))
            temp_path.append(action)
            #Take action, observe new state S'
            new_state = gw.makeMove(state, action)
            new_train_state = gw.stateToData(new_state)
            #Observe reward
            reward = gw.getReward(new_state)
            #Get max_Q(S',a)
            #newQ = model.predict(new_state.reshape(1,64), batch_size=1)
            ##### pytorch syntax ##################################################
            newQ = net(Variable(torch.from_numpy(new_train_state.reshape(1,32)).float()))
            newQ = newQ.data.numpy()
            ##### pytorch syntax ##################################################
            maxQ = np.max(newQ)
            y = np.zeros((1,4))
            y[:] = qval[:]
            if reward == -1: #non-terminal state
                update = (reward + (gamma * maxQ))
            else: #terminal state
                update = reward
            y[0][action] = update #target output

            print("Game #: "+str(i)+" / num_wins: "+str(numWins)+" / Epsilon: "+str(round(epsilon,3)))
            #model.fit(state.reshape(1,64), y, batch_size=1, nb_epoch=1, verbose=1)
            ##### pytorch syntax ##################################################
            # zero the parameter gradients
            optimizer.zero_grad()
            # forward + backward + optimize
            outputs = net(Variable(torch.from_numpy(train_state.reshape(1,32)).float()))
            labels = Variable(torch.from_numpy(y).float())
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()
            ##### pytorch syntax ##################################################
            old_state = state
            state = new_state


            if reward != -1:
                if reward == 20:
                    numWins+=1
                    winning_path = temp_path
                status = 0

        if epsilon > 0.1:
            epsilon -= (1/epochs)
    return net, winning_path

def trainNetwork_v3(model, in_state=None, epoch = 40 ):
    net = model
    # Define loss criterion
    criterion = nn.MSELoss()
    optimizer = optim.SGD(net.parameters(), lr=0.0001, momentum=0.9)
    epochs = epoch
    gamma = 0.9 #since it may take several moves to goal, making gamma high
    epsilon = 1
    numWins = 0
    winning_path = []
    x = 0

    for i in range(epochs):

        state = gw.initGrid()

        if in_state is not None:
            state = in_state

        status = 1
        #while game still in progress

        temp_path = []
        #while game still in progress
        while(status == 1):
            train_state = gw.stateToData(state)
            #We are in state S
            #Let's run our Q function on S to get Q values for all possible actions
            ##### keras syntax ####################################################
            #qval = model.predict(state.reshape(1,64), batch_size=1)
            ##### keras syntax ####################################################

            ##### pytorch syntax ##################################################
            qval = net(Variable(torch.from_numpy(train_state.reshape(1,32)).float()))
            qval = qval.data.numpy()

            if (random.random() < epsilon): #choose random action
                action = np.random.randint(0,4)
            else: #choose best action from Q(s,a) values
                action = (np.argmax(qval))
            temp_path.append(action)
            #Take action, observe new state S'
            new_state = gw.makeMove(state, action)
            new_train_state = gw.stateToData(new_state)
            #Observe reward
            reward = gw.getReward(new_state)
            #Get max_Q(S',a)
            #newQ = model.predict(new_state.reshape(1,64), batch_size=1)
            ##### pytorch syntax ##################################################
            newQ = net(Variable(torch.from_numpy(new_train_state.reshape(1,32)).float()))
            newQ = newQ.data.numpy()
            ##### pytorch syntax ##################################################
            maxQ = np.max(newQ)
            y = np.zeros((1,4))
            y[:] = qval[:]
            if reward == -1: #non-terminal state
                update = (reward + (gamma * maxQ))
            else: #terminal state
                update = reward
            y[0][action] = update #target output

            print("Game #: "+str(i)+" / num_wins: "+str(numWins)+" / Epsilon: "+str(round(epsilon,3)))
            #model.fit(state.reshape(1,64), y, batch_size=1, nb_epoch=1, verbose=1)
            ##### pytorch syntax ##################################################
            # zero the parameter gradients
            optimizer.zero_grad()
            # forward + backward + optimize
            outputs = net(Variable(torch.from_numpy(train_state.reshape(1,32)).float()))
            labels = Variable(torch.from_numpy(y).float())
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()
            ##### pytorch syntax ##################################################
            old_state = state
            state = new_state


            if reward != -1:
                if reward == 20:
                    numWins+=1
                    winning_path = temp_path
                status = 0
        if epsilon > 0.1:
            epsilon -= (1/epochs)

    return net, winning_path


#this is the same training algorithm as trainNetwork, but it trains to solve two grids, or two tasks
def trainNetwork_v2(in_state_1, in_state_2):
    net = models.Linear_Net()
    # Define loss criterion
    criterion = nn.MSELoss()
    optimizer = optim.SGD(net.parameters(), lr=0.0001, momentum=0.9)
    epochs = 3500
    gamma = 0.9 #since it may take several moves to goal, making gamma high
    epsilon = 1
    numWins = 0
    winning_path = []
    x = 0

    for i in range(epochs):

        #state = gw.initGrid()

        if random.random() > 0.5:
            state = in_state_1
        else:
            state = in_state_2

        status = 1
        #while game still in progress

        temp_path = []
        #while game still in progress
        while(status == 1):
            train_state = gw.stateToData(state)
            #We are in state S
            #Let's run our Q function on S to get Q values for all possible actions
            ##### keras syntax ####################################################
            #qval = model.predict(state.reshape(1,64), batch_size=1)
            ##### keras syntax ####################################################

            ##### pytorch syntax ##################################################
            qval = net(Variable(torch.from_numpy(train_state.reshape(1,32)).float()))
            qval = qval.data.numpy()

            if (random.random() < epsilon): #choose random action
                action = np.random.randint(0,4)
            else: #choose best action from Q(s,a) values
                action = (np.argmax(qval))
            temp_path.append(action)
            #Take action, observe new state S'
            new_state = gw.makeMove(state, action)
            new_train_state = gw.stateToData(new_state)
            #Observe reward
            reward = gw.getReward(new_state)
            #Get max_Q(S',a)
            #newQ = model.predict(new_state.reshape(1,64), batch_size=1)
            ##### pytorch syntax ##################################################
            newQ = net(Variable(torch.from_numpy(new_train_state.reshape(1,32)).float()))
            newQ = newQ.data.numpy()
            ##### pytorch syntax ##################################################
            maxQ = np.max(newQ)
            y = np.zeros((1,4))
            y[:] = qval[:]
            if reward == -1: #non-terminal state
                update = (reward + (gamma * maxQ))
            else: #terminal state
                update = reward
            y[0][action] = update #target output

            print("Game #: "+str(i)+" / num_wins: "+str(numWins)+" / Epsilon: "+str(round(epsilon,3)))
            #model.fit(state.reshape(1,64), y, batch_size=1, nb_epoch=1, verbose=1)
            ##### pytorch syntax ##################################################
            # zero the parameter gradients
            optimizer.zero_grad()
            # forward + backward + optimize
            outputs = net(Variable(torch.from_numpy(train_state.reshape(1,32)).float()))
            labels = Variable(torch.from_numpy(y).float())
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()
            ##### pytorch syntax ##################################################
            old_state = state
            state = new_state


            if reward != -1:
                if reward == 20:
                    numWins+=1
                    winning_path = temp_path
                status = 0

        if epsilon > 0.1:
            epsilon -= (1/epochs)
    return net

def testNetwork(network, init=0, inState=None):
    testNet = network
    i = 0

    if init==0:
        state = gw.initGrid()
    elif init==1:
        state = gw.initGridPlayer()
    elif init==2:
        state = gw.initGridRand()
    if inState is not None:
        state = gw.setGrid(inState)


    print("Initial State:")
    print(gw.dispGrid(state))
    status = 1
    #while game still in progress
    path = []
    while(status == 1):
        test_state = gw.stateToData(state)
        ##### keras syntax ####################################################
        #qval = model.predict(state.reshape(1,64), batch_size=1)
        ##### keras syntax ####################################################

        ##### pytorch syntax ##################################################
        qval = testNet(Variable(torch.from_numpy(test_state.reshape(1,32)).float()))
        qval = qval.data.numpy()
        print(qval)
        ##### pytorch syntax ##################################################

        action = (np.argmax(qval)) #take action with highest Q-value
        print('Move #: %s; Taking action: %s' % (i, action))
        state = gw.makeMove(state, action)
        print(gw.dispGrid(state))
        reward = gw.getReward(state)
        path.append(action)
        if reward != -1:
            status = 0
            print("Reward: %s" % (reward,))
            print(path)
        i += 1 #If we're taking more than 10 actions, just stop, we probably can't win this game
        if (i > 10):
            print("Game lost; too many moves.")
            print(path)
            break
    return testNet

#SAME AS testNetwork but this one returns the path and finalReward as well
def testNetwork_v2(network, inState=None):
    testNet = network
    i = 0

    # if init==0:
    #     state = gw.initGrid()
    # elif init==1:
    #     state = gw.initGridPlayer()
    # elif init==2:
    #     state = gw.initGridRand()
    if inState is not None:
        state = gw.setGrid(inState)


    print("Initial State:")
    print(gw.dispGrid(state))
    status = 1
    #while game still in progress
    path = []
    finalReward = -1
    while(status == 1):
        test_state = gw.stateToData(state)
        ##### keras syntax ####################################################
        #qval = model.predict(state.reshape(1,64), batch_size=1)
        ##### keras syntax ####################################################

        ##### pytorch syntax ##################################################
        qval = testNet(Variable(torch.from_numpy(test_state.reshape(1,32)).float()))
        qval = qval.data.numpy()
        print(qval)
        ##### pytorch syntax ##################################################

        action = (np.argmax(qval)) #take action with highest Q-value
        print('Move #: %s; Taking action: %s' % (i, action))
        state = gw.makeMove(state, action)
        print(gw.dispGrid(state))
        reward = gw.getReward(state)
        path.append(action)
        if reward != -1:
            finalReward = reward
            status = 0
            print("Reward: %s" % (reward,))
            print(path)
        i += 1 #If we're taking more than 10 actions, just stop, we probably can't win this game
        if (i > 10):
            print("Game lost; too many moves.")
            print(path)
            break
    return testNet, path, finalReward


#SAME AS testNetwork_v2 but this one does not print states
def testNetwork_v5(network, init=0, inState=None):
    testNet = network
    i = 0

    if init==0:
        state = gw.initGrid()
    elif init==1:
        state = gw.initGridPlayer()
    elif init==2:
        state = gw.initGridRand()
    if inState is not None:
        state = gw.setGrid(inState)


    #print("Initial State:")
    #print(gw.dispGrid(state))
    status = 1
    #while game still in progress
    path = []
    finalReward = -1
    while(status == 1):
        test_state = gw.stateToData(state)
        ##### keras syntax ####################################################
        #qval = model.predict(state.reshape(1,64), batch_size=1)
        ##### keras syntax ####################################################

        ##### pytorch syntax ##################################################
        qval = testNet(Variable(torch.from_numpy(test_state.reshape(1,32)).float()))
        qval = qval.data.numpy()
        #print(qval)
        ##### pytorch syntax ##################################################

        action = (np.argmax(qval)) #take action with highest Q-value
        #print('Move #: %s; Taking action: %s' % (i, action))
        state = gw.makeMove(state, action)
        #print(gw.dispGrid(state))
        reward = gw.getReward(state)
        path.append(action)
        if reward != -1:
            finalReward = reward
            status = 0
            #print("Reward: %s" % (reward,))
            #print(path)
        i += 1 #If we're taking more than 10 actions, just stop, we probably can't win this game
        if (i > 10):
            #print("Game lost; too many moves.")
            #print(path)
            break
    return testNet, path, finalReward

#This version will only make a single move and return the new state, and reward
def testNetwork_v3(network, init=0, inState=None):
    testNet = network
    i = 0


    state = gw.setGrid(inState)
    test_state = gw.stateToData(state)
    ##### keras syntax ####################################################
    #qval = model.predict(state.reshape(1,64), batch_size=1)
    ##### keras syntax ####################################################

    ##### pytorch syntax ##################################################
    qval = testNet(Variable(torch.from_numpy(test_state.reshape(1,32)).float()))
    qval = qval.data.numpy()
    print(qval)
    ##### pytorch syntax ##################################################

    action = (np.argmax(qval)) #take action with highest Q-value
    print('Move #: %s; Taking action: %s' % (i, action))
    state = gw.makeMove(state, action)
    print(gw.dispGrid(state))
    reward = gw.getReward(state)


    return state, reward


#SAME AS testNetwork_v2 but this one tests ability to solve two grids
def testNetwork_v4(network, inState_1, inState_2):
    testNet = network
    i = 0


    state1 = gw.setGrid(inState_1)
    state2 = gw.setGrid(inState_2)


    print("Initial State_1:")
    print(gw.dispGrid(state1))
    status_1 = 1
    #while game still in progress
    path_1 = []
    finalReward_1 = -1
    while(status_1 == 1):
        test_state = gw.stateToData(state1)
        ##### keras syntax ####################################################
        #qval = model.predict(state.reshape(1,64), batch_size=1)
        ##### keras syntax ####################################################

        ##### pytorch syntax ##################################################
        qval = testNet(Variable(torch.from_numpy(test_state.reshape(1,32)).float()))
        qval = qval.data.numpy()
        print(qval)
        ##### pytorch syntax ##################################################

        action = (np.argmax(qval)) #take action with highest Q-value
        print('Move #: %s; Taking action: %s' % (i, action))
        state1 = gw.makeMove(state1, action)
        print(gw.dispGrid(state1))
        reward = gw.getReward(state1)
        path_1.append(action)
        if reward != -1:
            finalReward_1 = reward
            status_1 = 0
            print("Reward: %s" % (reward,))
            print(path_1)
        i += 1 #If we're taking more than 10 actions, just stop, we probably can't win this game
        if (i > 10):
            print("Game lost; too many moves.")
            print(path_1)
            break

    print("Initial State_2:")
    print(gw.dispGrid(state2))
    status_2 = 1
    #while game still in progress
    path_2 = []
    finalReward_2 = -1
    while(status_2 == 1):
        test_state = gw.stateToData(state2)
        ##### keras syntax ####################################################
        #qval = model.predict(state.reshape(1,64), batch_size=1)
        ##### keras syntax ####################################################

        ##### pytorch syntax ##################################################
        qval = testNet(Variable(torch.from_numpy(test_state.reshape(1,32)).float()))
        qval = qval.data.numpy()
        print(qval)
        ##### pytorch syntax ##################################################

        action = (np.argmax(qval)) #take action with highest Q-value
        print('Move #: %s; Taking action: %s' % (i, action))
        state2 = gw.makeMove(state2, action)
        print(gw.dispGrid(state2))
        reward = gw.getReward(state2)
        path_2.append(action)
        if reward != -1:
            finalReward_2 = reward
            status_2 = 0
            print("Reward: %s" % (reward,))
            print(path_2)
        i += 1 #If we're taking more than 10 actions, just stop, we probably can't win this game
        if (i > 10):
            print("Game lost; too many moves.")
            print(path_2)
            break

    if finalReward_1 == 20 and finalReward_2 == 20:
        finalReward = 20
    else:
        finalReward = -20
    return testNet,  finalReward
