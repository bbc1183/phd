"""

THIS IS THE NEW EXPERIMENT:

    THE IDEA HERE IS TO EXPERIMENT WITH A DISCRIMINATOR WHEN TRAINING THE AE.

    THE OBJECTIVE IS TO FOOL THE DISCRIMINATOR INTO BEING UNABLE TO DISTINGUISH WHICH
    IS MORE EFFECTIVE, THE REAL ORIGINAL NETWORK PARAMETERS, OR THE GENERATED, APPROXIMATE
    PARAMETERS.

    IN THEORY, THIS SHOULD MEAN THAT THE FINAL APPROXIMATE WEIGHTS WILL NOT NECESSARILY BE
    EXACT COPIES/APPROXIMATIONS OF THE ORIGINAL WEIGHTS.  INSTEAD, THE CRITERIA FOR
    ASSESSMENT IS THAT THEY ACCOMPLISH THE OBJECTIVE.


"""



"""
THIS IS THE OLD EXPERIMENT:

    THE AIM OF THIS EXPERIMENT IS TO DEMONSTRATE TASK-FLEXIBLE CONTINUAL LEARNING BY AUTOENCODING
    SKILLS IN AN ITERATIVE FASHION

    GET 2 STATES
        TRAIN MODEL ON STATE 1, FOR TASK 1, TO LEARN SKILL 1
        TRAIN AUTOENCODER ON SKILL 1

        TRAIN NEW MODEL ON STATE 2, FOR TASK 2, TO LEARN SKILL 2
        GENERATE SKILL RECOLLECTION WITH AUTOENCODER
        RETRAIN AUTOENCODER ON SKILL 1, SKILL 2

        TEST SKILLS

"""
import models as m
import gridworld as gw
import qnetwork as q

import torch
import torchvision
import torchvision.transforms as transforms
import torch.optim as optim
import torch.nn as nn
import random
import torch.nn.functional as F
from torch.autograd import Variable, Function
import numpy as np
import matplotlib.pyplot as plt
import sys
state1 = gw.initGrid()
state2 = gw.initGrid_v2()

#train the models until they win
winStatus1 = 0
winStatus2 = 0
winStatus3 = 0
winStatus4 = 0

while winStatus1 == 0:
    model1_class1, paths1 = q.trainNetwork(in_state=state1)
    model1_class1, paths1, finalReward1 = q.testNetwork_v2(model1_class1,inState=gw.stateToData(state1))
    if finalReward1 == 20:
        winStatus1 = 1
while winStatus2 == 0:
    model2_class1, paths2 = q.trainNetwork(in_state=state2)
    model2_class1, paths2, finalReward2 = q.testNetwork_v2(model2_class1,inState=gw.stateToData(state2))
    if finalReward2 == 20:
        winStatus2 = 1


"""
EXTRACT THE WEIGHTS OF THE MODELS, FLATTEN THEM
"""
#FLATTEN THE MODELS
trainedModel_1_flat, shapes_1 = m.flattenNetwork(model1_class1)
trainedModel_2_flat, shapes_2 = m.flattenNetwork(model2_class1)

trained_models_flat = [trainedModel_1_flat, trainedModel_2_flat]
"""
-------------------------------------------------------------------------------------------
AE TRAINING AND TESTING FUNCTIONS
-------------------------------------------------------------------------------------------
"""

def test_encoded_skill(skill, grid, AE):
    AE
    guessed_encoded_skill = Variable(torch.from_numpy(skill.reshape(1,4)).float())
    #pass this skill to the AE to get the needed weights
    guessed_weights_flat = AE.decoder(guessed_encoded_skill)
    #guessed_weights_flat = AE.decoder(Variable(torch.from_numpy(encoded_skill_1.reshape(1,5))).float())
    guessed_weights_flat = guessed_weights_flat.data.numpy().reshape(2224)
    guessed_weights_unflat = m.unFlattenNetwork(guessed_weights_flat, shapes_1)
    net = m.Linear_Net()
    #load those weights into the network
    net = m.loadWeights(guessed_weights_unflat, net)
    #makeMove and get state.
    net,path,reward = q.testNetwork_v5(net, inState=grid)

    if len(path) > 10:
        testStatus = 0
        #print("GAME LOST, too many moves")
    if reward == 20:
        testStatus = 0
        #print("GAME WON!")
    if reward == -20:
        testStatus = 0
        #print("GAME LOST")
    return reward
"""
-------------------------------------------------------------------------------------------
END AE TRAINING AND TESTING FUNCTIONS
-------------------------------------------------------------------------------------------
"""

def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        m.weight.data.normal_(0.0, 0.02)
    elif classname.find('fc') != -1:
        m.weight.data.normal_(1.0, 0.02)
        m.bias.data.fill_(0)

real_label = 1
AE = m.AutoEncoder_v7()
discriminator = m.Linear_Discriminator()
AE.apply(weights_init)
discriminator.apply(weights_init)

for param in AE.parameters():
    param.requires_grad = True
for param in discriminator.parameters():
    param.requires_grad = True
losses_AE = []

criterion = nn.BCELoss()
optimizer_AE = optim.SGD(AE.parameters(), lr=.0003, momentum=0.9)
optimizer_D = optim.SGD(discriminator.parameters(), lr=.0003, momentum=0.9)
encoded_skills = []
losses_D = []

label = torch.FloatTensor(1)
x = 1
while 1==1:
    for i in range(1000):
        print("round: "+str(x)+" / training AUTO_GAN: "+str(i))
        #################################################################
        # (1) update the Discriminator
        #################################################################
        #train with original weights

        discriminator.zero_grad()
        originalWeights = trained_models_flat[0]
        if x == 1 and i == 0:
            fig = plt.figure()
            plt.plot(originalWeights)
            plt.show()

        labelv = Variable(label.fill_(1))
        output  = discriminator(Variable(torch.from_numpy(originalWeights.reshape(1,len(originalWeights))).float()))
        #print("real guess: "+str(output.data.numpy()[0]))
        #print("/")
        # print(output.data)
        # print(real_label.data)
        loss_real = criterion(output, labelv)
        loss_real.backward()
        #print("//")
        #print(originalWeights.shape)





        #train with approximation

        approx_weights_flattened, encoded_skill  = AE(Variable(torch.from_numpy(originalWeights.reshape(1,len(originalWeights))).float()))
        #print(approx_weights_flattened.data.numpy()[0])

        encoded_skill =  encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1])
        #print("///")
        status_1 = test_encoded_skill(encoded_skill, gw.stateToData(state1), AE)
        if status_1 == 20:
            approx_label = 1
        else:
            approx_label = 0
        labelv = Variable(label.fill_(approx_label))
        output  = discriminator(approx_weights_flattened.detach())
        #print("approximation guess: "+str(output.data.numpy()[0]))
        #print("////")
        loss_approx = criterion(output, labelv)
        loss_approx.backward()
        lossD = loss_real + loss_approx
        #print("D loss: "+str(lossD.data.numpy()[0]))
        losses_D.append(lossD.data.numpy()[0])
        optimizer_D.step()
        #print("X")

        #################################################################
        # (2) update the AE
        #################################################################
        AE.zero_grad()
        labelv = Variable(label.fill_(real_label))
        #labelvv = Variable(label.fill_(approx_label))
        output = discriminator(approx_weights_flattened)

        loss_AE = criterion(output, labelv)

        loss_AE.backward()
        #print("AE loss: "+str(loss_AE.data.numpy()[0]))
        losses_AE.append(loss_AE.data.numpy()[0])
        optimizer_AE.step()


    x+=1

    approx_weights_flattened, encoded_skill  = AE(Variable(torch.from_numpy(originalWeights.reshape(1,len(originalWeights))).float()))
    encoded_skill =  encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1])
    #print("///")
    status_1 = test_encoded_skill(encoded_skill, gw.stateToData(state1), AE)
    print("game status: "+str(status_1))

    if status_1 == 20:
        fig = plt.figure()
        plt.plot(approx_weights_flattened.data.numpy()[0])
        plt.show()


    fig = plt.figure()
    plt.plot(losses_D)
    plt.show()

    fig = plt.figure()
    plt.plot(losses_AE)
    plt.show()
