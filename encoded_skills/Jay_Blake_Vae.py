"""
IDENTICAL TO EXP15 BUT I WILL USE A VAE INSTEAD OF AE.

TRAIN MODEL ON STATE 1, FOR TASK 1, TO LEARN SKILL 1
TRAIN AUTOENCODER ON SKILL 1

"""


import torch
import torchvision
import torchvision.transforms as transforms
import torch.optim as optim
import torch.nn as nn
import random
import torch.nn.functional as F
from torch.autograd import Variable, Function
import numpy as np
import matplotlib.pyplot as plt
import sys
from sklearn.manifold import TSNE



transform = transforms.Compose(
    [transforms.ToTensor(),
     transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

trainset = torchvision.datasets.CIFAR10(root='../data', train=True,
                                        download=True, transform=transform)
trainloader = torch.utils.data.DataLoader(trainset, batch_size=4,
                                          shuffle=True, num_workers=2)

testset = torchvision.datasets.CIFAR10(root='../data', train=False,
                                       download=True, transform=transform)
testloader = torch.utils.data.DataLoader(testset, batch_size=4,
                                         shuffle=False, num_workers=2)

classes = ('plane', 'car', 'bird', 'cat',
           'deer', 'dog', 'frog', 'horse', 'ship', 'truck')
#############################################################################
# Flattening the NET
#############################################################################
def flattenNetwork(net):
    flatNet = []
    shapes = []
    for param in net.parameters():

        #if its WEIGHTS
        curr_shape = param.data.numpy().shape
        #print(curr_shape)
        shapes.append(curr_shape)
        if len(curr_shape) == 2:
            param = param.data.numpy().reshape(curr_shape[0]*curr_shape[1])
            flatNet.append(param)
        elif len(curr_shape) == 4:
            param = param.data.numpy().reshape(curr_shape[0]*curr_shape[1]*curr_shape[2]*curr_shape[3])
            flatNet.append(param)
        else:
            param = param.data.numpy().reshape(curr_shape[0])
            flatNet.append(param)
    finalNet = []
    for obj in flatNet:
        for x in obj:
            finalNet.append(x)
    finalNet = np.array(finalNet)
    return finalNet,shapes

#############################################################################
# UN-Flattening the NET
#############################################################################
def unFlattenNetwork(weights, shapes):
    #this is how we know how to slice weights

    begin_slice = 0
    end_slice = 0
    finalParams = []
    #print(len(weights))
    for idx,shape in enumerate(shapes):
        if len(shape) == 2:
            end_slice = end_slice+(shape[0]*shape[1])
            curr_slice = weights[begin_slice:end_slice]
            param = np.array(curr_slice).reshape(shape[0], shape[1])
            finalParams.append(param)
            begin_slice = end_slice
        elif len(shape) == 4:
            end_slice = end_slice+(shape[0]*shape[1]*shape[2]*shape[3])
            curr_slice = weights[begin_slice:end_slice]
            #print("shape: "+str(shape))
            #print("curr_slice: "+str(curr_slice.shape))
            param = np.array(curr_slice).reshape(shape[0], shape[1], shape[2], shape[3])
            finalParams.append(param)
            begin_slice = end_slice
        else:
            end_slice = end_slice+shape[0]
            curr_slice = weights[begin_slice:end_slice]
            param = np.array(curr_slice).reshape(shape[0],)
            finalParams.append(param)
            begin_slice = end_slice
    finalArr = np.array(finalParams)
    return np.array(finalArr)


class VAE_v1(nn.Module):
    def __init__(self):
        super(VAE_v1, self).__init__()

        self.fc1 = nn.Linear(62006, 200)
        self.fc21 = nn.Linear(200, 10)
        self.fc22 = nn.Linear(200, 10)
        self.fc3 = nn.Linear(10, 200)
        self.fc4 = nn.Linear(200, 62006)

        self.relu = nn.ReLU()
        self.sigmoid = nn.Sigmoid()

    def encode(self, x):
        h1 = F.tanh(self.fc1(x))
        return self.fc21(h1), self.fc22(h1)

    def reparameterize(self, mu, logvar):
        if self.training:
            std = logvar.mul(0.5).exp_()
            eps = Variable(std.data.new(std.size()).normal_())
            return eps.mul(std).add_(mu)
        else:
            return mu

    def decode(self, z):
        h3 = F.tanh(self.fc3(z))
        return self.fc4(h3),z

    def forward(self, x):
        mu, logvar = self.encode(x.view(-1, 62006))
        z = self.reparameterize(mu, logvar)
        return self.decode(z)[0], mu, logvar


"""
EXTRACT THE WEIGHTS OF THE MODELS, FLATTEN THEM
"""


"""
#FLATTEN THE MODELS
trainedModel_1_flat, shapes_1 = m.flattenNetwork(model1_class1)
trainedModel_2_flat, shapes_2 = m.flattenNetwork(model2_class1)

trained_models_flat = [trainedModel_1_flat, trainedModel_2_flat]
"""

class Jaya_CIFAR(nn.Module):
    def __init__(self):
        super(Jaya_CIFAR, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1 = nn.Linear(16 * 5 * 5, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = x.view(-1, 16 * 5 * 5)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x
model = Jaya_CIFAR()
criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(model.parameters(), lr=0.001, momentum=0.9)
def train_cifar(epochs):
    for epoch in range(epochs):  # loop over the dataset multiple times
        running_loss = 0.0
        for i, data in enumerate(trainloader, 0):
            # get the inputs
            inputs, labels = data

            # wrap them in Variable
            inputs, labels = Variable(inputs), Variable(labels)

            # zero the parameter gradients
            optimizer.zero_grad()

            # forward + backward + optimize
            outputs = model(inputs)
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()

            # print statistics
            running_loss += loss.data[0]
            if i % 2000 == 1999:    # print every 2000 mini-batches
                print('[%d, %5d] loss: %.3f' %
                    (epoch + 1, i + 1, running_loss / 2000))
                running_loss = 0.0

def test_cifar():
    correct = 0
    total = 0
    for data in testloader:
        images, labels = data
        outputs = model(Variable(images))
        _, predicted = torch.max(outputs.data, 1)
        total += labels.size(0)
        correct += (predicted == labels).sum()
    accuracy = 100 * correct / total
    print('Accuracy of the network on the 10000 test images: %d %%' % (
        100 * correct / total))
    class_correct = list(0. for i in range(10))
    class_total = list(0. for i in range(10))
    for data in testloader:
        images, labels = data
        outputs = model(Variable(images))
        _, predicted = torch.max(outputs.data, 1)
        c = (predicted == labels).squeeze()
        for i in range(4):
            label = labels[i]
            class_correct[label] += c[i]
            class_total[label] += 1
    for i in range(10):
        print('Accuracy of %5s : %2d %%' % (
            classes[i], 100 * class_correct[i] / class_total[i]))

        print('Finished Training')
    return accuracy


"""
-------------------------------------------------------------------------------------------
AE TRAINING AND TESTING FUNCTIONS
-------------------------------------------------------------------------------------------
"""
losses = []
def trainVAE(weights, epochs=1, factor=20):
    epochs = epochs
    # Define loss criterion
    learning_rate = .01
    criterion = nn.MSELoss()
    vae_optimizer = optim.SGD(VAE.parameters(), lr=learning_rate, momentum=0.9)
    global losses
    for i in range(epochs):
        #learning_rate *= (1.0/(1.0+0.0002*i))
        ##### pytorch syntax ##################################################
        # zero the parameter gradients
        vae_optimizer.zero_grad()
        # forward + backward + optimize
        inputs = Variable(torch.from_numpy(weights.reshape(1,len(weights))).float())
        outputs, mu, logvar = VAE(inputs)
        #print(encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1]))

        loss = loss_function(outputs, inputs, mu, logvar)
        loss.backward()
        losses.append(loss.data[0])
        print("loss: "+str(loss.data[0]))
        vae_optimizer.step()
        ##### pytorch syntax ##################################################
    approximated_weights, mu, logvar = VAE(inputs)
    #encoded_skill = encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1])

    # fig = plt.figure()
    # plt.plot(losses)
    # plt.show()
    return approximated_weights, mu, logvar, VAE


# Reconstruction + KL divergence losses summed over all elements and batch
def loss_function(recon_x, x, mu, logvar):
    MSE = F.mse_loss(recon_x, x.view(-1, 62006), size_average=False)

    # see Appendix B from VAE paper:
    # Kingma and Welling. Auto-Encoding Variational Bayes. ICLR, 2014
    # https://arxiv.org/abs/1312.6114
    # 0.5 * sum(1 + log(sigma^2) - mu^2 - sigma^2)
    KLD = (-0.5 * torch.sum(1 + logvar - mu.pow(2) - logvar.exp()))/62006.0

    return pow(MSE + KLD,1/2)

def loadWeights_cifar(weights_to_load, net):
    net.conv1.weight.data = torch.from_numpy(weights_to_load[0])
    net.conv1.bias.data =   torch.from_numpy(weights_to_load[1])
    net.conv2.weight.data = torch.from_numpy(weights_to_load[2])
    net.conv2.bias.data =   torch.from_numpy(weights_to_load[3])
    net.fc1.weight.data =   torch.from_numpy(weights_to_load[4])
    net.fc1.bias.data =     torch.from_numpy(weights_to_load[5])
    net.fc2.weight.data =   torch.from_numpy(weights_to_load[6])
    net.fc2.bias.data =     torch.from_numpy(weights_to_load[7])
    net.fc3.weight.data =   torch.from_numpy(weights_to_load[8])
    net.fc3.bias.data =     torch.from_numpy(weights_to_load[9])
    return net

def test_encoded_skill(sample):
    #pass this skill to the AE to get the needed weights
    guessed_weights_flat, latent_vars = VAE.decode(sample)
    #guessed_weights_flat = AE.decoder(Variable(torch.from_numpy(encoded_skill_1.reshape(1,5))).float())
    guessed_weights_flat = guessed_weights_flat.data.numpy().reshape(2224)
    guessed_weights_unflat = m.unFlattenNetwork(guessed_weights_flat, shapes_1)
    net = m.Linear_Net()
    #load those weights into the network
    net = m.loadWeights(guessed_weights_unflat, net)
    #makeMove and get state.
    net,path,reward = q.testNetwork_v2(net, inState=grid)

    if len(path) > 10:
        testStatus = 0
        print("GAME LOST, too many moves")
    if reward == 20:
        testStatus = 0
        print("GAME WON!")
    if reward == -20:
        testStatus = 0
        print("GAME LOST")
    return reward
"""
-------------------------------------------------------------------------------------------
END AE TRAINING AND TESTING FUNCTIONS
-------------------------------------------------------------------------------------------
"""
# train_cifar(2)
# test_cifar()
model_weights = torch.load("jay_blake_vae_weights.pt")
model.load_state_dict(model_weights)

trainedModel_1_flat, shapes_1 = flattenNetwork(model)
unFlattenNetwork(trainedModel_1_flat, shapes_1)

print("=====================passed the unflatten=============")
#trained_models_flat = [trainedModel_1_flat]
VAE_win_status = 0
VAE = VAE_v1()
encoded_skills = []
losses = []
Accuracies_array = []
encoded_skills = []
curr_round = 1
while VAE_win_status == 0:


    for i in range(1000):
        print("training autoencoder: "+str(i))
        index = random.randint(0,0)
        net_to_encode = trainedModel_1_flat#trained_models_flat[index]
        #approx_weights_flattened, mu, logvar, VAE =
        trainVAE(net_to_encode)
    total_accuracies = 0

    for j in range(0,5):
        sample =Variable(torch.randn(1, 10))
        print("sample before decode is",sample)
        sample, latent_vars = VAE.decode(sample)
        print("sample after decode is",sample)
        sample=sample.data.numpy().reshape(62006)
        latent_vars = latent_vars.data.numpy().reshape(10)
        final_weights=unFlattenNetwork(sample, shapes_1)
        net = loadWeights_cifar(final_weights,model)
        accuracy = test_cifar()
        total_accuracies += accuracy
        encoded_skills.append(latent_vars)
    avg_accuracy = total_accuracies/5.0
    Accuracies_array.append(avg_accuracy)
    print("AVERAGE ACCURACY FOR ROUND: "+str(avg_accuracy))
    # status_1 = test_encoded_skill(sample, gw.stateToData(state1), VAE)
    #
    #
    # if status_1 == 20:
    #     #encoded_skills.append(encoded_skill)
    #     print("VICTORY FOR ALL ENCODED SKILLS")
    #     VAE_win_status = 1
    fig = plt.figure()
    plt.plot(losses)
    plt.show()

    fig = plt.figure()
    plt.plot(Accuracies_array)
    plt.show()




    if curr_round == 11:
        skills = np.array(encoded_skills)
        skill_batch1 = skills
        skill_batch1 = TSNE(n_components=2).fit_transform(skill_batch1)
        skills = np.array(skill_batch1)
        plt.scatter(skills[:,0], skills[:,1])
        plt.show()

    curr_round += 1
