import experiment1 as e1
import models as m
import gridworld as gw
import qnetwork as q

import torch
import torchvision
import torchvision.transforms as transforms
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable, Function
import numpy as np
import matplotlib.pyplot as plt
import sys


encoded_skill_1, AE, shapes = e1.run()
state = gw.initGrid()
train_state = gw.stateToData(state)

mapper = m.State_To_Skill_Mapper()
# Define loss criterion
criterion = nn.MSELoss()
learning_rate = .01
losses = []
for i in range(100):
    #learning_rate *= (1.0/(1.0+0.0002*i))
    optimizer = optim.SGD(mapper.parameters(), lr=learning_rate, momentum=0.9)
    ##### pytorch syntax ##################################################
    # zero the parameter gradients
    optimizer.zero_grad()
    # forward + backward + optimize

    outputs = mapper(Variable(torch.from_numpy(train_state.reshape(1,32)).float()))

    labels = Variable(torch.from_numpy(encoded_skill_1))
    loss = criterion(outputs, labels)
    #print(loss.data.numpy()[0])
    losses.append(loss.data.numpy()[0])
    loss.backward()
    optimizer.step()
    ##### pytorch syntax ##################################################
fig = plt.figure()
plt.plot(losses)
plt.show()


"""
GIVEN A NEW STATE, CAN WE FEED THAT TO THE TRAINED MAPPER TO GENERATE A NEW ENCODED SKILL,
PASS THAT SKILL TO THE TRAINED DECODER,
AND USE THOSE WEIGHTS TO ACCOMPLISH THE NEW TASK/STATE

1/17/18 - SO FAR THE ANSWER IS NO
"""

unseen_state = gw.initGridPlayer()
unseen_state_train = gw.stateToData(unseen_state)
#print(gw.dispGrid(unseen_state))
guessed_encoded_skill = mapper(Variable(torch.from_numpy(unseen_state_train.reshape(1,32)).float()))

#which encoded skill do we want to try, does the same encoded skill result in the same path
guessed_weights_flat = AE.decoder(guessed_encoded_skill)
#guessed_weights_flat = AE.decoder(Variable(torch.from_numpy(encoded_skill_1.reshape(1,5))).float())

guessed_weights_flat = guessed_weights_flat.data.numpy().reshape(guessed_weights_flat.shape[1])
guessed_weights_unflat = m.unFlattenNetwork(guessed_weights_flat, shapes)
net = m.Linear_Net()
net = m.loadWeights(guessed_weights_unflat, net)
q.testNetwork(net, setState=unseen_state_train)
print(encoded_skill_1)
print(guessed_encoded_skill.data.numpy().reshape(guessed_encoded_skill.data.numpy().shape[1]))
