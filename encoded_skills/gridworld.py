import numpy as np
import matplotlib.pyplot as plt
import time
import random
import sys

player = np.float64(0.26)
wall = np.float64(0.13)
bomb = np.float64(0.77)
target = np.float64(0.99)

num_elements = 4

def randPair(s,e):
    return np.random.randint(s,e), np.random.randint(s,e)

#finds an array in the "depth" dimension of the grid
def findLoc(state, obj):
    if obj == player:
        elm = 0
        #print("n here")
    if obj == wall:
        elm = 1
    if obj == bomb:
        elm = 2
    if obj == target:
        elm = 3
    for i in range(0,4):
        for j in range(0,4):
            if (state[elm][i,j] == obj):
                #print("found")
                return i,j

def getAllStates4configuration(state):
    s1 = state
    xy1 = findLoc(state, player)
    wallxy = findLoc(state, wall)
    allStates = []
    allStates.append(xy1)
    allStatesFound = 0
    #while allStatesFound == false
    while allStatesFound == 0:
        #generate random new coordinates for player which != wall
        notWall = 0
        while notWall == 0:
            newCoords = (random.randint(0,3), random.randint(0,3))
            if newCoords != wallxy:
                notWall = 1
        matchFound = 0
        for coords in allStates:
            if newCoords == coords:
                matchFound = 1
        if matchFound == 0:
            allStates.append(newCoords)
        #when the number of unique coordinates == 15, allStatesFound = true
        if len(allStates) == 15:
            allStatesFound = 1
    finalStates = []
    for coord in allStates:
        new_state = np.zeros((num_elements,4,4)) # num_elements   x   x_dim   x   y_dim
        new_state.fill(-0.5)
        new_state[0][coord[0],coord[1]] = player
        new_state[1][2,1] = wall
        new_state[2][1,2] = bomb
        new_state[3][2,2] = target
        finalStates.append(new_state)
    return finalStates



def setGrid(state):
    state = state.reshape(state.shape[1])
    coords = []
    coords.append(list(state[0:4]))
    coords.append(list(state[4:8]))
    coords.append(list(state[8:12]))
    coords.append(list(state[12:16]))
    coords.append(list(state[16:20]))
    coords.append(list(state[20:24]))
    coords.append(list(state[24:28]))
    coords.append(list(state[28:32]))
    new_coords = []
    for ob in coords:
        tmp_coord = 0
        if ob == [1.0,-1.0,-1.0,-1.0]:
            tmp_coord = 0
        if ob == [-1.0,1.0,-1.0,-1.0]:
            tmp_coord = 1
        if ob == [-1.0,-1.0,1.0,-1.0]:
            tmp_coord = 2
        if ob == [-1.0,-1.0,-1.0,1.0]:
            tmp_coord = 3
        new_coords.append(tmp_coord)
    player_coords = (new_coords[0], new_coords[1])
    wall_coords = (new_coords[2], new_coords[3])
    bomb_coords = (new_coords[4], new_coords[5])
    target_coords = (new_coords[6], new_coords[7])
    state = np.zeros((num_elements,4,4))
    state.fill(-0.5)
    state[0][player_coords[0], player_coords[1]] = player
    #place wall
    state[1][wall_coords[0], wall_coords[1]] = wall
    #place bomb
    state[2][bomb_coords[0], bomb_coords[1]] = bomb
    #place target
    state[3][target_coords[0], target_coords[1]] = target

    return state






#Initialize stationary grid, all items are placed deterministically
def initGrid():
    state = np.zeros((num_elements,4,4)) # num_elements   x   x_dim   x   y_dim
    state.fill(-0.5)
    #place player
    state[0][0,0] = player
    #place wall
    state[1][2,1] = wall
    #place bomb
    state[2][1,2] = bomb
    #place target
    state[3][2,2] = target

    return state

#Initialize stationary grid, all items are placed deterministically
def initGrid_v2():
    state = np.zeros((num_elements,4,4)) # num_elements   x   x_dim   x   y_dim
    state.fill(-0.5)
    #place player
    state[0][0,3] = player
    #place wall
    state[1][1,2] = wall
    #place bomb
    state[2][2,1] = bomb
    #place target
    state[3][3,0] = target

    return state

def get6GameBoards():
    allStates = []
    state1 = np.zeros((num_elements,4,4)) # num_elements   x   x_dim   x   y_dim
    state1.fill(-0.5)

    #state 1, class 1
    #place player
    state1[0][0,0] = player
    #place wall
    state1[1][2,1] = wall
    #place bomb
    state1[2][1,2] = bomb
    #place target
    state1[3][2,2] = target

    state2 = np.zeros((num_elements,4,4)) # num_elements   x   x_dim   x   y_dim
    state2.fill(-0.5)

    #state 2, class 1
    #place player
    state2[0][0,3] = player
    #place wall
    state2[1][2,1] = wall
    #place bomb
    state2[2][1,2] = bomb
    #place target
    state2[3][2,2] = target

    state3 = np.zeros((num_elements,4,4)) # num_elements   x   x_dim   x   y_dim
    state3.fill(-0.5)

    #state 3, class 1
    #place player
    state3[0][3,0] = player
    #place wall
    state3[1][2,1] = wall
    #place bomb
    state3[2][1,2] = bomb
    #place target
    state3[3][2,2] = target

    state4 = np.zeros((num_elements,4,4)) # num_elements   x   x_dim   x   y_dim
    state4.fill(-0.5)
    #state 4, class 2
    #place player
    state4[0][0,1] = player
    #place wall
    state4[1][1,0] = wall
    #place bomb
    state4[2][1,1] = bomb
    #place target
    state4[3][2,0] = target

    state5 = np.zeros((num_elements,4,4)) # num_elements   x   x_dim   x   y_dim
    state5.fill(-0.5)
    #state, 5, class 2
    #place player
    state5[0][3,2] = player
    #place wall
    state5[1][1,0] = wall
    #place bomb
    state5[2][1,1] = bomb
    #place target
    state5[3][2,0] = target

    state6 = np.zeros((num_elements,4,4)) # num_elements   x   x_dim   x   y_dim
    state6.fill(-0.5)
    #state 6, class 2
    #place player
    state6[0][0,3] = player
    #place wall
    state6[1][1,0] = wall
    #place bomb
    state6[2][1,1] = bomb
    #place target
    state6[3][2,0] = target

    allStates = [state1, state2, state3, state4, state5, state6]
    return allStates



#Initialize player in random location, but keep wall, goal and pit stationary
def initGridPlayer():
    state = np.zeros((num_elements,4,4))
    state.fill(-0.5)
    #place player
    pairfound = 0
    while pairfound == 0:
        randpair = randPair(0,4)
        if ((randpair) != (2,2)) and ((randpair) != (1,1)) and ((randpair) != (1,2)):
            pairfound = 1

    state[0][randpair] = player
    #place wall
    state[1][2,2] = wall
    #place bomb
    state[2][1,1] = bomb
    #place target
    state[3][1,2] = target

    p = findLoc(state, player) #find grid position of player (agent)
    w = findLoc(state, wall) #find wall
    t = findLoc(state, target) #find goal
    b = findLoc(state, bomb) #find pit
    if (not p or not w or not t or not b):
        #print('Invalid grid. Rebuilding..')
        return initGridPlayer()

    return state

#Initialize grid so that goal, pit, wall, player are all randomly placed
def initGridRand():
    state = np.zeros((num_elements,4,4))
    state.fill(-0.5)

    stateFound = 0
    while stateFound == 0:
        print("start")
        player_coords = randPair(0,4)
        wall_coords = randPair(0,4)
        if (player_coords) != (wall_coords):
            bomb_coords = randPair(0,4)
            if ((bomb_coords) != (player_coords)) and ((bomb_coords) != (wall_coords)):
                target_coords = randPair(0,4)
                if((target_coords) != (player_coords)) and ((target_coords) != (wall_coords)) and ((target_coords) != (bomb_coords)):


                    if (((bomb_coords) == (0,1)) and ((wall_coords) == (1,0))) or (((wall_coords) == (0,1)) and ((bomb_coords) == (1,0))):
                        if ((target_coords) == (0,0)) and ((player_coords) == (0,0)):
                            stateFound = 0
                            #print("invalid state")
                    elif(((bomb_coords) == (0,2)) and ((wall_coords) == (1,3))) or (((wall_coords) == (0,2)) and ((bomb_coords) == (1,3))):
                        if ((target_coords) == (0,3))or ((player_coords) == (0,3)):
                            stateFound = 0
                            #print("invalid state")
                    elif(((bomb_coords) == (2,3)) and ((wall_coords) == (3,2))) or (((wall_coords) == (2,3)) and ((bomb_coords) == (3,2))):
                        if ((target_coords) == (3,3)) or ((player_coords) == (3,3)):
                            stateFound = 0
                            #print("invalid state")
                    elif(((bomb_coords) == (2,0)) and ((wall_coords) == (3,1))) or (((wall_coords) == (2,0)) and ((bomb_coords) == (3,1))):
                        if ((target_coords) == (3,0)) or ((player_coords) == (3,0)):
                            stateFound = 0
                            #print("invalid state")
                    else:
                        #print("state found")
                        stateFound= 1
                #else:
                    #print("invalid state")
            #else:
                #print("invalid state")
        #else:
            #print("invalid state")


    # print(player_coords)
    # print(wall_coords)
    # print(target_coords)
    # print(bomb_coords)

    #place player
    state[0][player_coords] = player
    #place wall
    state[1][wall_coords] = wall
    #place bomb
    state[2][bomb_coords] = bomb
    #place target
    state[3][target_coords] = target

    p = findLoc(state, player)
    w = findLoc(state, wall)
    t = findLoc(state, target)
    b = findLoc(state, bomb)
    #If any of the "objects" are superimposed, just call the function again to re-place
    if (not p or not w or not t or not b):
        #print('Invalid grid. Rebuilding..')
        return initGridRand()

    return state

def makeMove(state, action):
    #need to locate player in grid
    #need to determine what object (if any) is in the new grid spot the player is moving to
    player_loc = findLoc(state, player)
    wall_loc = findLoc(state, wall)
    target_loc = findLoc(state, target)
    bomb_loc = findLoc(state, bomb)

    #print("player_loc: "+str(player_loc))
    # print(wall_loc)
    # print(target_loc)
    # print(bomb_loc)
    state = np.zeros((num_elements,4,4))
    state.fill(-0.5)

    actions = [[0,1],[0,-1],[-1,0],[1,0]]
    #e.g. up => (player row - 1, player column + 0)
    new_loc = (player_loc[0] + actions[action][0], player_loc[1] + actions[action][1])
    #print("new_loc: "+ str(new_loc))
    if (new_loc != wall_loc):
        if ((np.array(new_loc) <= (3,3)).all() and (np.array(new_loc) >= (0,0)).all()):
            state[0][new_loc] = player
            #print(state)
    #print("new loc "+str(tuple(reversed(new_loc))))

    new_player_loc = findLoc(state, player)
    if (not new_player_loc):
        state[0][player_loc] = player
    #re-place bomb
    state[2][bomb_loc] = bomb
    #print("bomb_loc "+str(tuple(reversed(bomb_loc))))
    #re-place wall
    state[1][wall_loc] = wall
    #print("wall_loc "+str(tuple(reversed(wall_loc))))
    #re-place target
    state[3][target_loc] = target
    #print("target_loc "+str(tuple(reversed(target_loc))))
    #print(state)


    return state


def stateToData(state):
    player_loc = findLoc(state,player)
    bomb_loc = findLoc(state, bomb)
    target_loc = findLoc(state, target)
    wall_loc = findLoc(state, wall)

    all_coords = [player_loc,wall_loc, bomb_loc,target_loc]

    data = []
    for coords in all_coords:
        for coord in coords:
            if coord == 0:
                encoded_coord = [1.0,-1.0,-1.0,-1.0]
            elif coord == 1:
                encoded_coord = [-1.0,1.0,-1.0,-1.0]
            elif coord == 2:
                encoded_coord = [-1.0,-1.0,1.0,-1.0]
            elif coord == 3:
                encoded_coord = [-1.0,-1.0,-1.0,1.0]
            data.append(encoded_coord)
    data = np.array(data).reshape((1,32))
    return data

def getReward(state):
    player_loc = findLoc(state, player)
    bomb_loc = findLoc(state, bomb)
    target_loc = findLoc(state, target)
    # print(player_loc)
    # print(bomb_loc)
    # print(target_loc)
    if (player_loc == bomb_loc):
        return -20
    elif (player_loc == target_loc):
        return 20
    else:
        return -1

def dispGrid(state):
    grid = np.zeros((4,4),dtype=object)
    #row_col
    #print(type(player))

    player_loc = tuple(reversed(findLoc(state, player)))
    player_loc_grid = ((num_elements-1) - player_loc[0],  player_loc[1])
    #print(tuple(reversed(player_loc)))

    wall_loc = tuple(reversed(findLoc(state, wall)))
    wall_loc_grid = ((num_elements-1) -wall_loc[0],  wall_loc[1])
    #print(tuple(reversed(wall_loc)))

    target_loc = tuple(reversed(findLoc(state, target)))
    target_loc_grid = ((num_elements-1) -target_loc[0],  target_loc[1])
    #print(tuple(reversed(target_loc)))

    bomb_loc = tuple(reversed(findLoc(state, bomb)))
    bomb_loc_grid = ((num_elements-1) -bomb_loc[0], bomb_loc[1])
    #print(tuple(reversed(bomb_loc)))

    for i in range(0,4):
        for j in range(0,4):
            grid[i,j] = ' '

    if player_loc:
        grid[player_loc_grid] = 'P' #player/
        #print("player_loc: "+str(player_loc))
    if wall_loc:
        grid[wall_loc_grid] = 'W' #wall
        #print("wall_loc: "+str(wall_loc))
    if target_loc:
        grid[target_loc_grid] = '+' #target
        #print("target_loc: "+str(target_loc))
    if bomb_loc:
        grid[bomb_loc_grid] = '-' #bomb
        #print("bomb_loc: "+str(bomb_loc))
    if player_loc == bomb_loc:
        grid[bomb_loc_grid] = 'LOSS'
    if player_loc == target_loc:
        grid[target_loc_grid] = 'WIN'



    return grid
