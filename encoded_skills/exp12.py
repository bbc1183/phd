import models as m
import gridworld as gw
import qnetwork as q

import torch
import torchvision
import torchvision.transforms as transforms
import torch.optim as optim
import torch.nn as nn
import random
import torch.nn.functional as F
from torch.autograd import Variable, Function
import numpy as np
import matplotlib.pyplot as plt
import sys
import pandas as pd
from sklearn import linear_model


import models as m
import gridworld as gw
import qnetwork as q

import torch
import torchvision
import torchvision.transforms as transforms
import torch.optim as optim
import torch.nn as nn
import random
import torch.nn.functional as F
from torch.autograd import Variable, Function
import numpy as np
import matplotlib.pyplot as plt
import sys
import pandas as pd
from sklearn import linear_model
g = gw

net1 = torch.load("t1.pt")
net2 = torch.load("t4_1.pt")
cNet = torch.load("t1_t4.pt")

state1 = g.initGrid()
states = g.get6GameBoards()
state2 = states[3]


flatNet_1, shapes_1 = m.flattenNetwork(net1)
flatNet_2, shapes_2 = m.flattenNetwork(net2)
flatnet_c, shapes_c = m.flattenNetwork(cNet)

trained_models_flat = [flatNet_1, flatNet_2, flatnet_c]
losses = []

def trainAE(weights,auto_encoder, learning_rate, epochs=1, factor=20):
    epochs = epochs
    # Define loss criterion
    criterion = nn.MSELoss()
    #learning_rate = .99
    global losses
    for i in range(epochs):
        #learning_rate *= (1.0/(1.0+0.0002*i))
        optimizer = optim.SGD(AE.decoder.parameters(), lr=learning_rate, momentum=0.9)
        #optimizer = optim.Adam(AE.decoder.parameters(),lr=1e-3)# lr=learning_rate, weight_decay=1e-5)
        ##### pytorch syntax ##################################################
        # zero the parameter gradients
        optimizer.zero_grad()
        # forward + backward + optimize
        outputs, encoded_skill = AE(Variable(torch.from_numpy(weights.reshape(1,len(weights))).float()))
        #print(encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1]))
        encoded_skill =  encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1])
        labels = Variable(torch.from_numpy(weights))
        loss = criterion(outputs, labels)
        #print(loss.data.numpy()[0])
        losses.append(loss.data.numpy()[0])
        loss.backward()
        optimizer.step()
        ##### pytorch syntax ##################################################
    approximated_weights, encoded_skill = AE(Variable(torch.from_numpy(weights.reshape(1,len(weights))).float()))
    encoded_skill = encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1])
    # fig = plt.figure()
    # plt.plot(losses)
    # plt.show()
    return approximated_weights, encoded_skill, AE

def test_encoded_skill(skill, grid):
    global AE
    guessed_encoded_skill = Variable(torch.from_numpy(skill.reshape(1,4)).float())
    #pass this skill to the AE to get the needed weights
    guessed_weights_flat = AE.decoder(guessed_encoded_skill)
    #guessed_weights_flat = AE.decoder(Variable(torch.from_numpy(encoded_skill_1.reshape(1,5))).float())
    guessed_weights_flat = guessed_weights_flat.data.numpy().reshape(guessed_weights_flat.data.numpy().shape[1])
    guessed_weights_unflat = m.unFlattenNetwork(guessed_weights_flat, shapes_1)
    net = m.Linear_Net()
    #load those weights into the network
    net = m.loadWeights(guessed_weights_unflat, net)
    #makeMove and get state.
    net,path,reward = q.testNetwork_v2(net, inState=grid)

    if len(path) > 10:
        testStatus = 0
        print("GAME LOST, too many moves")
    if reward == 20:
        testStatus = 0
        print("GAME WON!")
    if reward == -20:
        testStatus = 0
        print("GAME LOST")
    return reward



"""
TRAIN THE AUTOENCODER TO VICTORY
"""

AE = m.AutoEncoder(2224,20)

final_state_skill_1 = ()
final_state_skill_2 = ()
final_state_skill_3 = ()
final_state_skill_4 = ()
AE_win_status = 0
losses = []
while AE_win_status == 0:


    learning_rate = 9

    losses = []
    for i in range(5000):
        if i %1000 == 0:
            print("training autoencoder: "+str(i))
        index = random.randint(0,2)
        net_to_encode = trained_models_flat[index]
        approx_weights_flattened, encoded_skill, AE = trainAE(net_to_encode,AE, learning_rate)
        if index == 0:
            final_state_skill_1 = (state1, encoded_skill)
        if index == 1:
            final_state_skill_2 = (state2, encoded_skill)
        if index == 2:
            final_state_skill_3 = (state1, encoded_skill)
            final_state_skill_4 = (state2, encoded_skill)

    fig = plt.figure()
    plt.plot(losses)
    plt.show()
    print("first encoded_skill: "+str(final_state_skill_1[1]))
    print("second_encoded_skill: "+str(final_state_skill_2[1]))
    print("Combined_encoded_skill: "+str(final_state_skill_3[1]))
    status_1 = test_encoded_skill(final_state_skill_1[1], gw.stateToData(final_state_skill_1[0]))
    status_2 = test_encoded_skill(final_state_skill_2[1], gw.stateToData(final_state_skill_2[0]))
    status_3 = test_encoded_skill(final_state_skill_3[1], gw.stateToData(final_state_skill_1[0]))
    status_4 = test_encoded_skill(final_state_skill_4[1], gw.stateToData(final_state_skill_2[0]))


    if status_1 == 20 and status_2 == 20 and status_3 == 20 and status_4 == 20:

        AE_win_status = 1

final_states_and_skills = [final_state_skill_1,final_state_skill_2, final_state_skill_3,final_state_skill_4]


"""
FINALLY GOT AUTOENCODER TO CONVERGE FOR ALL THREE NETS: THESE ARE THE WINNING ENCODED SKILLS
x = [ 0.47697815 , 0.41511825,  0.49660966,  0.58290875]
y = [ 0.51906532 , 0.43959469 , 0.5291363 ,  0.50750601]
z  = [ 0.51026922 , 0.47951892 , 0.47688529 , 0.55114627]


x=  [ 0.49754  ,   0.52509469 , 0.52463007 , 0.61164016]
y=  [ 0.45428869,  0.50299597,  0.48766565 , 0.51147664]
z = [ 0.44012356 , 0.4486976 ,  0.46773258  ,0.50460023]

x=[ 0.5263806  , 0.49338797 , 0.46743712,  0.52374387]
y= [ 0.50466192,  0.53558081,  0.48746806 , 0.45981172]
z=  [ 0.5353964  , 0.59569228 , 0.49116862 , 0.4745332 ]

x= [ 0.41569394 , 0.51992077 , 0.41101193 , 0.3944467 ]
y=[ 0.463727 ,   0.51534426, 0.50480789 , 0.37209091]
z= [ 0.40689605 , 0.50364321 , 0.4831726 ,  0.3694424 ]

x= [ 0.48606786 , 0.48231691  ,0.59201127 , 0.47977129]
y= [ 0.50223047 , 0.52636963 , 0.54791796 , 0.52185285]
z= [ 0.34269789 , 0.51900929 , 0.56327236 , 0.47123984]

x= [ 0.56580842 , 0.39654711 , 0.44130653 , 0.50051802]
y= [ 0.46766651 , 0.49804708 , 0.4212667 ,  0.47002366]
z= [ 0.4881407  , 0.42084384 , 0.42362368 , 0.52131808]

x [ 0.55450106,  0.49940729 , 0.49642891 , 0.48414445]
y= [ 0.50313443 , 0.46589431 , 0.4774366 ,  0.52645713]
z= [ 0.54317158 , 0.47492307 , 0.47960237 , 0.30628181]





"""

skill1 = final_state_skill_1[1]
skill2 = final_state_skill_2[1]
skill3 = final_state_skill_3[1]
