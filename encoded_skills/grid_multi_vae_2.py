"""
THE AIM HERE IS TO TRAIN VAE ON TWO SKILLS,

    THEN SEE IF WE CAN GENERATE A SKILL GIVEN THE MU, LOGVAR

"""
import models as m
import gridworld as gw
import qnetwork as q

import torch
import torchvision
import torchvision.transforms as transforms
import torch.optim as optim
import torch.nn as nn
import random
import torch.nn.functional as F
from torch.autograd import Variable, Function
import numpy as np
import matplotlib.pyplot as plt
import sys
from sklearn.manifold import TSNE
state1 = gw.initGrid()
state2 = gw.initGrid_v2()


"""
winStatus1 = 0
while winStatus1 == 0:
    model1_class1, paths1 = q.trainNetwork(in_state=state1)
    model1_class1, paths1, finalReward1 = q.testNetwork_v2(model1_class1,inState=gw.stateToData(state1))
    if finalReward1 == 20:
        torch.save(model1_class1, "grid_multi_vae_model1.pt")
        winStatus1 = 1

numRounds = []

#train the models until they win
winStatus1 = 0
winStatus2 = 0
winStatus3 = 0
winStatus4 = 0

#winStatus1 = 0
model = model1_class1
currRound = 1
while winStatus2 == 0 :
    epsilon = 1

    #model, paths1 = q.trainNetwork_v3(model, in_state=state1, epoch=1, eps=epsilon)
    model, paths2 = q.trainNetwork_v3(model, in_state=state2)


    #model, paths1, finalReward1 = q.testNetwork_v2(model,inState=gw.stateToData(state1))
    model, paths2, finalReward2 = q.testNetwork_v2(model,inState=gw.stateToData(state2))

    if finalReward2 == 20:
        torch.save(model, "grid_multi_vae_model2.pt")
        winStatus2 = 1
        numRounds.append(currRound)
    currRound +=1
#
# print(numRounds)t
# sys.exit()
"""


model1_class1 = torch.load("grid_multi_vae_model1.pt")
model = torch.load("grid_multi_vae_model2.pt")





"""
EXTRACT THE WEIGHTS OF THE MODELS, FLATTEN THEM
"""

#
# model = torch.load("grid_multi_vae_model2.pt")




#FLATTEN THE MODELS
trainedModel_1_flat, shapes_1 = m.flattenNetwork(model1_class1)
trainedModel_2_flat, shapes_2 = m.flattenNetwork(model)

trained_models_flat = [trainedModel_1_flat, trainedModel_2_flat]


# model = m.Jaya_CIFAR()
# model_weights = torch.load("jay_blake_vae_weights.pt")
# model.load_state_dict(model_weights)
# trainedModel_1_flat, shapes_1 = m.flattenNetwork(model)




"""
-------------------------------------------------------------------------------------------
AE TRAINING AND TESTING FUNCTIONS
-------------------------------------------------------------------------------------------
"""
losses = []


def train(weights):


    optimizer.zero_grad()

    inputs = Variable(torch.from_numpy(weights.reshape(1,len(weights))).float())
    outputs, mu, logvar  = VAE(inputs)
    #print(encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1]))

    loss = loss_function(outputs, inputs, mu, logvar)
    loss.backward()
    losses.append(loss.data[0])
    print("loss: "+str(loss.data[0]))
    optimizer.step()
    ##### pytorch syntax ##################################################
    approximated_weights, mu, logvar = VAE(inputs)
    #encoded_skill = encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1])

    # fig = plt.figure()
    # plt.plot(losses)
    # plt.show()
    return approximated_weights, mu, logvar

def trainVAE_dual(weights,weights2,vae,  lr = .001):
    epochs = 1
    # Define loss criterion
    criterion = nn.MSELoss()
    learning_rate = lr
    global losses

    for i in range(epochs):
        #learning_rate *= (1.0/(1.0+0.0002*i))
        optimizer = optim.SGD(vae.parameters(), lr=learning_rate, momentum=0.9)
        ##### pytorch syntax ##################################################
        # zero the parameter gradients
        optimizer.zero_grad()
        # forward + backward + optimize
        inputs = Variable(torch.from_numpy(weights.reshape(1,len(weights))).float())
        inputs2 = Variable(torch.from_numpy(weights2.reshape(1,len(weights2))).float())
        outputs, mu, logvar  = vae(inputs)
        outputs2, mu2, logvar2  = vae(inputs2)
        #print(encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1]))

        loss = loss_function(outputs, inputs, mu, logvar)
        loss2 = loss_function(outputs2, inputs2, mu2, logvar2)
        total_loss = loss + loss2
        total_loss.backward()
        losses.append(total_loss.data[0])
        #print("loss: "+str(total_loss.data[0]))
        optimizer.step()
        ##### pytorch syntax ##################################################
    approximated_weights, mu, logvar = vae(inputs)
    approximated_weights2, mu2, logvar2 = vae(inputs2)
    #encoded_skill = encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1])

    # fig = plt.figure()
    # plt.plot(losses)
    # plt.show()
    return approximated_weights,approximated_weights2, mu, logvar, mu2, logvar2,  vae


# Reconstruction + KL divergence losses summed over all elements and batch
def loss_function(recon_x, x, mu, logvar):
    MSE = F.mse_loss(recon_x, x.view(-1, 2224), size_average=False)

    # see Appendix B from VAE paper:
    # Kingma and Welling. Auto-Encoding Variational Bayes. ICLR, 2014
    # https://arxiv.org/abs/1312.6114
    # 0.5 * sum(1 + log(sigma^2) - mu^2 - sigma^2)
    KLD = -0.5 * torch.sum(1 + logvar - mu.pow(2) - logvar.exp())

    return MSE + KLD

def test_encoded_skill(sample, grid, local_vae):


    #pass this skill to the AE to get the needed weights

    guessed_weights_flat, latent_vars = local_vae.decode(sample)
    #guessed_weights_flat = AE.decoder(Variable(torch.from_numpy(encoded_skill_1.reshape(1,5))).float())
    guessed_weights_flat = guessed_weights_flat.data.numpy().reshape(2224)
    guessed_weights_unflat = m.unFlattenNetwork(guessed_weights_flat, shapes_1)
    net = m.Linear_Net()
    #load those weights into the network
    net = m.loadWeights(guessed_weights_unflat, net)
    #makeMove and get state.
    net,path,reward = q.testNetwork_v2(net, inState=grid)



    if len(path) > 10:
        testStatus = 0
        print("GAME LOST, too many moves")
    if reward == 20:
        testStatus = 0
        print("GAME WON!")
    if reward == -20:
        testStatus = 0
        print("GAME LOST")
    return reward, latent_vars
"""
-------------------------------------------------------------------------------------------
END AE TRAINING AND TESTING FUNCTIONS
-------------------------------------------------------------------------------------------
"""

VAE_win_status = 0
VAE = m.VAE_v2()
optimizer = optim.Adam(VAE.parameters(), lr=1e-3)
encoded_skills = []
losses = []
skill_dists = []

# model1 = m.unFlattenNetwork(trained_models_flat[0], shapes_1)
# model2 = m.unFlattenNetwork(trained_models_flat[1], shapes_1)
#
# net = m.Linear_Net()
# net2 = m.Linear_Net()
# #load those weights into the network
# model1 = m.loadWeights(model1, net)
# model2 = m.loadWeights(model2, net2)
#
# q.testNetwork_v2(model1,inState=gw.stateToData(state1))
# q.testNetwork_v2(model2,inState=gw.stateToData(state2))
# sys.exit()


def GENERATE_SAMPLE_FROM_MU_STD(mu,sigma, vae):
    z=torch.distributions.Normal(mu,np.sqrt(np.exp(sigma)))
    return z.sample()


while VAE_win_status == 0:
    for i in range(4000):
        print("training autoencoder: "+str(i))

        # net_to_encode = trained_models_flat[0]
        #
        # net_to_encode2 = trained_models_flat[1]
        #index = random.randint(0,1)
        if (random.random() < 0.5):
            index = 0
        else:
            index = 1
        net_to_encode = trained_models_flat[index]


        #approx_weights_flattened,approx_weights_flattened2, mu, logvar, mu2, logvar2, VAE = trainVAE_dual(net_to_encode,net_to_encode2, VAE)
        approximated_weights, mu, logvar = train(net_to_encode)
        # if(index == 0):
        #     skill1_dist = (mu,logvar)
        # if(index == 1):
        #     skill2_dist = (mu2, logvar2)




    #THIS IS THE MU AND LOGVAR FOR SKILL1
    #print(skill1_dist)
    # mu = skill1_dist[0].data.numpy()
    # logvar = skill1_dist[1].data.numpy()
    # print("////////////////////////////////////////////")
    # print("MU: ")
    # print(mu)
    # print("////////////////////////////////////////////")
    # print("LOGVAR: ")
    # print(logvar)
    # print("////////////////////////////////////////////")

    winCount_1 = 0
    model1_status = 0
    skill1_vars = []
    sample = Variable(torch.randn(1, 10))

    sample = Variable(torch.from_numpy(np.random.random(10)).float())
    # print("SAMPLE: ")
    # print(sample.data.numpy())
    # print("////////////////////////////////////////////")
    # print("GENERATED SAMPLE")
    # skill1_sample = GENERATE_SAMPLE_FROM_MU_STD((skill1_dist[0]), skill1_dist[1], VAE)
    # skill2_sample = GENERATE_SAMPLE_FROM_MU_STD((skill2_dist[0]), skill2_dist[1], VAE)
    print("////////////////////////////////////////////")
    #sample = Variable(torch.from_numpy(sample.reshape(1,10)).float())
    status_1, latent_vars = test_encoded_skill(sample, gw.stateToData(state1), VAE)
    status_2, latent_vars2 = test_encoded_skill(sample, gw.stateToData(state2), VAE)

    print("////////////////////////////////////////////")
    #print("LAST INDEX: "+ str(index))
    print(str(losses[len(losses)-1]))
    print("////////////////////////////////////////////")

    fig = plt.figure()
    plt.plot(losses)
    plt.show()



    if status_1 == 20 and status_2 == 20:
        print("win for both!!!!!")
        # print(skill1_dist)
        # print(latent_vars.data.numpy()[0])
        # sys.exit()
        winCount_1 += 1
        skill1_vars.append(latent_vars.data.numpy()[0])
        if winCount_1 == 5:
            #encoded_skills.append(encoded_skill)
            print("MODEL_1:  VICTORY FOR LATENT_SAMPLE")
            model1_status = 1
            model2_status = 2

            VAE_win_status = 1


"""

winCount_1 = 0
winCount_2 = 0
model1_status = 0
model2_status = 0
skill1_vars = []
skill2_vars = []
neither = []
allSamples = []
for i in range(10):
    #try to generate a sameple from skill1 mu and logvar
    sample = []
    for idx, x in enumerate(skill1_dist[0].data.numpy()):
        newSample = x*random.random()+pow(10,skill1_dist[1].data.numpy()[idx])
        sample.append(newSample)
    sample = np.array(newSample)
    #print(sample)
    sample = Variable(torch.from_numpy(sample.reshape(1,10)).float())
    # print("here")
    # sys.exit()

    status_1, latent_vars = test_encoded_skill(sample, gw.stateToData(state1), VAE, skill1_dist)
    status_2, latent_vars2 = test_encoded_skill(sample, gw.stateToData(state2), VAE, skill2_dist)
    #THESE ARE ALL THE SAME ###################################################
    # print(sample)
    # print(latent_vars)
    # print(latent_vars2)
    # sys.exit()
    #######################################################################
    allSamples.append(latent_vars.data.numpy()[0])
    print("attempt 1")
    if status_1 == 20:
        print("win")
        # print(skill1_dist)
        # print(latent_vars.data.numpy()[0])
        # sys.exit()
        winCount_1 += 1
        skill1_vars.append(latent_vars.data.numpy()[0])
        if winCount_1 == 5:
            #encoded_skills.append(encoded_skill)
            print("MODEL_1:  VICTORY FOR ALL LATENT_SAMPLES")
            model1_status = 1
    else:
        print("loss")
    sys.exit()
    # elif status_2 == 20:
    #     winCount_2 += 1
    #     skill2_vars.append(latent_vars.data.numpy()[0])
    #     if winCount_2 == 5:
    #         #encoded_skills.append(encoded_skill)
    #         print("MODEL_1:  VICTORY FOR ALL LATENT_SAMPLES")
    #         model2_status = 1
    # elif status_1 != 20 and status_2 != 20:
    #     neither.append(latent_vars.data.numpy()[0])
if model1_status == 1 and model2_status == 1:
    print("VICTORY FOR BOTH MODELS")
    VAE_win_status = 1
else:
    print("Model 1 Win Count: "+str(winCount_1))
    print("Model 2 Win Count: "+str(winCount_2))


fig = plt.figure()
plt.plot(losses)
plt.show()



    skill1_vars = np.array(skill1_vars)
    skill2_vars = np.array(skill2_vars)
    neither_vars = np.array(neither)
    # print(skill1_vars)
    # sys.exit()
    #skill_batch = TSNE(n_components=2).fit_transform(all_vars)
    if len(skill1_vars) > 1:
        skill_batch1 = TSNE(n_components=2).fit_transform(skill1_vars)
        skills1 = np.array(skill_batch1)
        plt.scatter(skills1[:,0], skills1[:,1], c="green")
    if len(skill2_vars) > 1:
        skill_batch2 = TSNE(n_components=2).fit_transform(skill2_vars)
        skills2 = np.array(skill_batch2)
        plt.scatter(skills2[:,0], skills2[:,1], c="blue")
    if len(neither_vars) > 1:
        neither_batch = TSNE(n_components=2).fit_transform(neither_vars)
        neither_batch = np.array(neither_batch)
        plt.scatter(neither_batch[:,0], neither_batch[:,1], c="red")

    plt.show()
"""
