"""
THE AIM OF THIS EXPERIMENT IS TO DEMONSTRATE TASK-FLEXIBLE CONTINUAL LEARNING BY AUTOENCODING
SKILLS IN AN ITERATIVE FASHION

GET 2 STATES
    TRAIN MODEL ON STATE 1, FOR TASK 1, TO LEARN SKILL 1
    TRAIN AUTOENCODER ON SKILL 1

    TRAIN NEW MODEL ON STATE 2, FOR TASK 2, TO LEARN SKILL 2
    GENERATE SKILL RECOLLECTION WITH AUTOENCODER
    RETRAIN AUTOENCODER ON SKILL 1, SKILL 2

    TEST SKILLS

"""
import models as m
import gridworld as gw
import qnetwork as q

import torch
import torchvision
import torchvision.transforms as transforms
import torch.optim as optim
import torch.nn as nn
import random
import torch.nn.functional as F
from torch.autograd import Variable, Function
import numpy as np
import matplotlib.pyplot as plt
import sys
state1 = gw.initGrid()
state2 = gw.initGrid_v2()

#train the models until they win
winStatus1 = 0
winStatus2 = 0
winStatus3 = 0
winStatus4 = 0

while winStatus1 == 0:
    model1_class1, paths1 = q.trainNetwork(in_state=state1)
    model1_class1, paths1, finalReward1 = q.testNetwork_v2(model1_class1,inState=gw.stateToData(state1))
    if finalReward1 == 20:
        winStatus1 = 1
while winStatus2 == 0:
    model2_class1, paths2 = q.trainNetwork(in_state=state2)
    model2_class1, paths2, finalReward2 = q.testNetwork_v2(model2_class1,inState=gw.stateToData(state2))
    if finalReward2 == 20:
        winStatus2 = 1


"""
EXTRACT THE WEIGHTS OF THE MODELS, FLATTEN THEM
"""
#FLATTEN THE MODELS
trainedModel_1_flat, shapes_1 = m.flattenNetwork(model1_class1)
trainedModel_2_flat, shapes_2 = m.flattenNetwork(model2_class1)

trained_models_flat = [trainedModel_1_flat, trainedModel_2_flat]
"""
-------------------------------------------------------------------------------------------
AE TRAINING AND TESTING FUNCTIONS
-------------------------------------------------------------------------------------------
"""
losses = []
def trainAE(weights,auto_encoder, epochs=1, factor=20):
    epochs = epochs
    # Define loss criterion
    criterion = nn.MSELoss()
    learning_rate = .7
    global losses
    for i in range(epochs):
        #learning_rate *= (1.0/(1.0+0.0002*i))
        optimizer = optim.SGD(AE.parameters(), lr=learning_rate, momentum=0.9)
        ##### pytorch syntax ##################################################
        # zero the parameter gradients
        optimizer.zero_grad()
        # forward + backward + optimize
        outputs, encoded_skill  = AE(Variable(torch.from_numpy(weights.reshape(1,len(weights))).float()))
        #print(encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1]))
        encoded_skill =  encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1])
        labels = Variable(torch.from_numpy(weights))
        loss = criterion(outputs, labels)
        #print(loss.data.numpy()[0])
        losses.append(loss.data.numpy()[0])
        loss.backward()
        optimizer.step()
        ##### pytorch syntax ##################################################
    approximated_weights, encoded_skill = AE(Variable(torch.from_numpy(weights.reshape(1,len(weights))).float()))
    encoded_skill = encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1])

    # fig = plt.figure()
    # plt.plot(losses)
    # plt.show()
    return approximated_weights, encoded_skill, AE

def test_encoded_skill(skill, grid, AE):
    AE
    guessed_encoded_skill = Variable(torch.from_numpy(skill.reshape(1,4)).float())
    #pass this skill to the AE to get the needed weights
    guessed_weights_flat = AE.decoder(guessed_encoded_skill)
    #guessed_weights_flat = AE.decoder(Variable(torch.from_numpy(encoded_skill_1.reshape(1,5))).float())
    guessed_weights_flat = guessed_weights_flat.data.numpy().reshape(2224)
    guessed_weights_unflat = m.unFlattenNetwork(guessed_weights_flat, shapes_1)
    net = m.Linear_Net()
    #load those weights into the network
    net = m.loadWeights(guessed_weights_unflat, net)
    #makeMove and get state.
    net,path,reward = q.testNetwork_v2(net, inState=grid)

    if len(path) > 10:
        testStatus = 0
        print("GAME LOST, too many moves")
    if reward == 20:
        testStatus = 0
        print("GAME WON!")
    if reward == -20:
        testStatus = 0
        print("GAME LOST")
    return reward
"""
-------------------------------------------------------------------------------------------
END AE TRAINING AND TESTING FUNCTIONS
-------------------------------------------------------------------------------------------
"""

AE_win_status = 0
AE = m.AutoEncoder_v7()
encoded_skills = []
losses = []
while AE_win_status == 0:


    for i in range(1000):
        print("training autoencoder: "+str(i))
        index = random.randint(0,0)
        net_to_encode = trained_models_flat[index]
        approx_weights_flattened, encoded_skill, AE = trainAE(net_to_encode,AE)
    status_1 = test_encoded_skill(encoded_skill, gw.stateToData(state1), AE)
    if status_1 == 20:
        encoded_skills.append(encoded_skill)
        print("VICTORY FOR ALL ENCODED SKILLS")
        AE_win_status = 1
    fig = plt.figure()
    plt.plot(losses)
    plt.show()

AE_win_status_2 = 0
losses = []
AE2 = m.AutoEncoder_v7()
#recall the needed skill for state1, so that we can train the AE using these
#weights too
encoded_skill1 = Variable(torch.from_numpy(encoded_skills[0].reshape(1,4)).float())
net_to_encode1 = AE.decoder(encoded_skill1)
net_to_encode1 = net_to_encode1.data.numpy().reshape(2224)
while AE_win_status_2 == 0:
    encoded_skill1 = ""
    encoded_skill2 = ""

    for i in range(2000):
        print("training autoencoder: "+str(i))
        index = random.randint(0,1)
        if index == 0:
            approx_weights_flattened, encoded_skill1, AE = trainAE(net_to_encode1,AE)
        elif index == 1:
            net_to_encode = trained_models_flat[index]
            approx_weights_flattened, encoded_skill2, AE = trainAE(net_to_encode,AE)
    status_1 = test_encoded_skill(encoded_skill1, gw.stateToData(state1), AE)
    status_2 = test_encoded_skill(encoded_skill2, gw.stateToData(state2), AE)
    if status_1 == 20 and status_2 == 20:
        encoded_skills = []
        encoded_skills.append(encoded_skill1)
        encoded_skills.append(encoded_skill2)
        print("VICTORY FOR ALL ENCODED SKILLS")
        AE_win_status_2 = 1
    fig = plt.figure()
    plt.plot(losses)
    plt.show()
