"""
THE IDEA HERE IS TO COMPRESS ALL STATES AS WELL AS SKILLS.  THIS WILL MAKE THE SKILL AE
SUBSTANTIALLY SMALLER, AND IT WILL MAKE IT EASIER TO STUDY RELATIONSHIPS BETWEEN THE
PARAMETERS OF THE TRAINED NETS.

- TRAIN NET NORMALLY
    - WITHIN TRAINING ALGORITHM, TRAIN STATE_AE TO COMPRESS STATES, SAVE STATE_AE
-
"""

import gridworld as gw
import models as m

import torch
import torchvision
import torchvision.transforms as transforms
import torch.optim as optim
import torch.nn as nn
import random
import torch.nn.functional as F
from torch.autograd import Variable, Function
import numpy as np
import matplotlib.pyplot as plt

state = gw.initGrid()
allStates = gw.getAllStates4configuration(state)
for idx,state in enumerate(allStates):
    allStates[idx] = gw.stateToData(state)
stateAE = m.State_AutoEncoder()


"""
----------------------------------------------------------------------------------------
"""
losses = []

def train_StateAE(state,auto_encoder, learning_rate, epochs=1, factor=20):
    epochs = epochs
    # Define loss criterion
    criterion = nn.MSELoss()
    #learning_rate = .99
    global losses
    for i in range(epochs):
        #learning_rate *= (1.0/(1.0+0.0002*i))
        optimizer = optim.SGD(auto_encoder.decoder.parameters(), lr=learning_rate, momentum=0.9)
        #optimizer = optim.Adam(AE.decoder.parameters(),lr=1e-3)# lr=learning_rate, weight_decay=1e-5)
        ##### pytorch syntax ##################################################
        # zero the parameter gradients
        optimizer.zero_grad()
        # forward + backward + optimize
        outputs, encoded_state = auto_encoder(Variable(torch.from_numpy(state.reshape(1,32)).float()))
        #print(encoded_skill.data.numpy().reshape(encoded_skill.data.numpy().shape[1]))
        encoded_state =  encoded_state.data.numpy().reshape(encoded_state.data.numpy().shape[1])
        labels = Variable(torch.from_numpy(state.reshape(1,32)).float())
        loss = criterion(outputs, labels)
        #print(loss.data.numpy()[0])
        losses.append(loss.data.numpy()[0])
        loss.backward()
        optimizer.step()
        ##### pytorch syntax ##################################################
    approximated_state, encoded_state = auto_encoder(Variable(torch.from_numpy(state.reshape(1,32)).float()))
    encoded_state = encoded_state.data.numpy().reshape(encoded_state.data.numpy().shape[1])
    approximated_state = approximated_state.data.numpy().reshape(approximated_state.data.numpy().shape[1])
    # fig = plt.figure()
    # plt.plot(losses)
    # plt.show()
    return approximated_state, encoded_state, auto_encoder
"""
----------------------------------------------------------------------------------------
"""
learning_rate = .9

losses = []
correctStatus = 0
while correctStatus == 0:
    for i in range(20000):
        if i %1000 == 0:

            print("training autoencoder: "+str(i))

        index = random.randint(0,14)
        state_to_encode = allStates[index]
        approx_state, encoded_state, stateAE = train_StateAE(state_to_encode,stateAE, learning_rate)
    fig = plt.figure()
    plt.plot(losses)
    plt.show()

    numWins = 0
    encoded_states = []
    for state in allStates:

        print(state)
        approximated_state, encoded_state= stateAE(Variable(torch.from_numpy(state.reshape(1,32)).float()))
        approximated_state = approximated_state.data.numpy().reshape(approximated_state.data.numpy().shape[1])
        encoded_state = list(encoded_state.data.numpy().reshape(10))
        encoded_states.append(encoded_state)
        for idx, apprx in enumerate(approximated_state):
            if apprx != 1.0 and apprx != -1.0:
                if apprx < 0:
                    approximated_state[idx] = -1.0
                else:
                    approximated_state[idx] = 1.0
        print(approximated_state)
        errorFound = 0
        for idx,x1 in enumerate(state[0]):

            if state[0][idx] != approximated_state[idx]:
                errorFound = 1
        if errorFound == 0:
            numWins += 1
    if numWins == 15:
        correctStatus = 1
        print("CORRECT FOR ALL STATES")
    else:
        print(numWins)
